# Internal API with Implementation

The api to code plugins for Craftverse.eu.

## Installation (soon)

I will crate a Nexus Repo soon.

```xml
<dependency>
    <groupId>eu.craftverse</groupId>
    <artifactId>Internal-API</artifactId>
    <version>1.0-SNAPSHOT</version>     
</dependency>
```

## Usage

### Players

```java
ICorePlayer player = VenadeAPI.getPlayer(UUID.randomUUID());
player.addStars(10);
        
System.out.println(player.getGlobalLevel());
System.out.println(player.getStars());

//Update player after editing
VenadeAPI.getPlayerService().updateInRedis(player);
```

#### Rank

```java
NonPlayerCharacterBuilder.builder()
        .display("§aTest")
        .location(event.getPlayer().getLocation().clone())
        .lookAtPlayer(true)
        .event(new EntityLeftClickEvent(clickEvent -> {
        clickEvent.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_CAT_HISS, 1F, 1F);
        }))
        .build(npc -> npc.sendPacket(event.getPlayer()));
```
### Commands
See [COMMANDS.md](COMMANDS.md)

### NPC

```java
NonPlayerCharacterBuilder.builder()
        .display("§aTest")
        .location(event.getPlayer().getLocation().clone())
        .lookAtPlayer(true)
        .event(new EntityLeftClickEvent(clickEvent -> {
        clickEvent.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_CAT_HISS, 1F, 1F);
        }))
        .build(npc -> npc.sendPacket(event.getPlayer()));
```

### Inventory

```java
public class TestInventory implements InventoryProvider {
    
    private SmartInventory inventory;
    
    public TestInventory() {
        this.inventory = SmartInventory.builder()
                .manager(VenadeAPI.getInventoryManager())
                .provider(this)
                .id("test")
                .title("§aA quick test inv")
                .closeable(false)
                .size(3, 9)
                .build();
    }

    public void openInventory(Player player) {
        this.inventory.open(player);
    }

    public void closeInventory(Player player) {
        this.inventory.close(player);
    }
    
    @Override
    public void init(Player player, InventoryContents contents) {
        ItemStack sword = ItemBuilder.builder(Material.DIAMOND_SWORD).unbreakable().display("§4Sword").build();
        ItemStack glass = ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§4").build();
        
        //First row will be filled with class, not clickable
        contents.fillRow(0, ClickableItem.empty(glass));
        
        //Sword itemstack will be at row 2, column 2 (java starts counting at 0)
        contents.set(1, 1, ClickableItem.of(sword, inventoryClickEvent -> {
            player.sendMessage("You clicked on " + inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName());
        }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {
        //Make animations here, if you have to
        //Every x ticks this method will be called
    }
    
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change or
contact CubePixels#3629 on discord.

## License

[MIT](https://choosealicense.com/licenses/mit/)
