# Commands

To create a command, first create a class that extends `VenadeCommand`

```java
public class ExampleCommand extends VenadeCommand {
    public ExampleCommand() {
        super(
                "example", // the command name
                "example.command.example.description", // the language key of the command description,
                Permission.has(CubidRank.DEV), // the permission required to run the command (null if for everyone)
                false // whether the command class is a container (i.e. it just holds sub commands). defaults to false if not provided,
                "examplecommandalias1", "examplecommandalias2" // aliases, optional
        );
    }
}
```

Then create a method for the command itself, like this

```java
public class ExampleCommand extends VenadeCommand {
    
    [constructor...]

    @CommandMethod
    public void onCommand(Sender sender, String name, @ConsumeRest String reason) {
        sender.sendMessage(name);
        sender.sendMessage(reason);
    }
}
```

## Sub Commands

To make a sub command, you make a command the exact same way, so for example

```java
public class ChildCommand extends VenadeCommand {
    public ChildCommand() {
        super(
                "child",
                "example.command.child.description",
                null // will have the same permissions as parent command
        );
    }

    @CommandMethod
    public void onCommand(Sender sender) {
        sender.sendMessage("Child command!");
    }
}
```

To register a sub command, do the following in the constructor of the parent:

```java
public class ParentCommand extends GCommand {
    public ParentCommand() {
        super(
                "parent",
                "example.command.parent.description",
                Permission.hasOrHigher(CubidRank.MOD)
        );
        this.registerSubCommand(new ChildCommand());
    }

    @CommandMethod
    public void onCommand(Sender sender) {
        sender.sendMessage("Parent command!");
    }
}
```

Running `/parent` would return "Parent command!" and running `/parent child` would return "Child command!"

Remember, a sub command can have sub commands of its own

**IMPORTANT**: Providing `null` as the permission of a sub command will make the sub command use the same permissions as
its parent command.

### Permissions

Permissions are simple, add one of the following to the constructor:

* `Permission.has(CubidRank.DEV, CubidRank.SR_DEV)` - checks the user has the ranks specified
* `Permission.hasOrHigher(CubidRank.DEV)` - checks the user has a rank **or higher**.
* You can also define a custom one like this for example:
  ```java
  new Permission() {
        @Override
        public boolean hasPermission(Sender sender) {
            return sender.getAsCommandSender().getName().equals("JakeMT04");
        }
  }
  ```

### Containers

A container is a command that simply holds sub commands and doesn't do anything on its own.

To make a command a container, set `container` to true in the constructor.

All `@CommandMethod`s in container commands are ignored.

## Registering commands

You do not need to add commands to the `plugin.yml`. The system will register them automatically.

To register commands with Bukkit, simply add this code to your onEnable. **Remember to register providers first (see
below)**

```java
import net.venade.internal.api.command.registration.CommandManager;

public void onEnable() {
    CommandManager.registerCommand(new AdminCommand(),this);
    CommandManager.registerCommand(new OtherCommand(),this);
}
```

## Compiling (IMPORTANT)

In order to compile properly, you need to include the following in the maven POM for the plugin you are making:

```xml

<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
            <source>11</source>
            <target>11</target>
            <compilerArgs>
                <arg>-parameters</arg>
            </compilerArgs>
        </configuration>
    </plugin>
</plugins>
```

The  `-parameters` tells the compiler to retain the parameter names. This is important if you are using the parameter
name as the argument names, otherwise the help message will look like this:
`/command <arg1> <arg2> <arg3>`

## The `Sender`

The following methods are available for the `Sender` of a command:

* `sender.sendMessage("cubid.language.key", "arg1", "arg2", "arg3)` - sends a message using the cubid language lib.
    * If sender is console, english will be used
* `sender.getLanguageString("cubid.language.key")` - gets a language string, same as cubid.
    * If sender is console, english will be used
* `sender.sendRawMessage("raw message")` - sends a normal chat message
* `sender.hasRank(CubidRank.DEV)` / `sender.hasRankOrHigher(CubidRank.DEV)` / `sender.hasRankOrLower(CubidRank.DEV)` -
  same as cubid
* `sender.isConsole()` - checks if console is running the command
* `sender.getAsPlayer()` - gets the sender as a bukkit `Player`
* `sender.getAsCorePlayer()` - gets the sender as a Venade `ICorePlayer`
* `sender.getAsCommandSender()` - gets the sender as the raw bukkit `CommandSender`

## Arguments and parameters

The following annotations are available for parameters. **None of these are required**

* `@Name` - the name of the argument. If not provided, it will use the parameter name
* `@Desc` - the description of the argument, shown in the help menu (uses cubid language strings)
* `@Optional` - marks the argument as optional. If it fails to parse, `null` will be provided to the command
* `@Default` - gives a default value to use if no value is provided.
    * **You cannot add `@Optional` and `@Default`, use one or the other**
* `@CustomProvider` - tells the system to use an alternative provider to parse the argument rather than the default
  one (see below).
    * **Do not add this unless you know you need to use a different provider to the default one for the class.**
* `@ConsumeRest` - tells the system to take all extra arguments and provide it to the last argument.
    * **This only works for `String` types and only on the last argument of a command**

Have a look
at [the default providers](Internal-API/src/main/java/net/venade/internal/api/command/provider/DefaultProviders.java)
for examples of providers.

**A provider must be registered for every class that you use in a command argument. The command will fail to register if
a provider cannot be found!**

### Flags

Flags are simple ways of doing a boolean option, for example, adding a "silent" flag to a punishment command.

To define a flag, add `@Flag(flag char)` to the parameter.

Notes:
* Flag characters must be unique in a command, you cannot reuse characters
* Flags only support booleans

This example would create a command with arguments `<target> [-s] <reason>`. The `-s` can be placed anywhere within the
arguments (so `/ban JakeMT04 -s Cheating`, `/ban -s JakeMT04 Cheating` & `/ban JakeMT04 Cheating -s` all work)

```java
@CommandMethod
public void onCommand(Sender sender, Player target, @Flag('s') boolean silent, @ConsumeRest String reason) {
}
```

If a flag is not given in command arguments, `false` is given to the method.

If you don't want a flag to show up in the help message, add `hiddenFromHelp = true` to the flag like
this: `@Flag(value = 'p', hiddenFromHelp = true)`
This is useful for flags that do the opposite of other flags, for example `-p` being the opposite of `-s`