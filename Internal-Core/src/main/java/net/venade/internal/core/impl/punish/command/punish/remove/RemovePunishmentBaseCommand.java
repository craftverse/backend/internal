package net.venade.internal.core.impl.punish.command.punish.remove;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.lang.ref.WeakReference;
import java.util.Collections;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.annotation.Name;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.RemovablePunishment;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;

/**
 * @author JakeMT04
 * @since 21/10/2021
 */
public class RemovePunishmentBaseCommand extends VenadeCommand {

  private final Class<? extends RemovablePunishment> type;

  public RemovePunishmentBaseCommand(Class<? extends RemovablePunishment> type, String... aliases) {
    super("un" + type.getSimpleName().toLowerCase(),
        "core.un" + type.getSimpleName().toLowerCase() + ".desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        false, aliases);
    this.type = type;
    this.setAsync(true);
  }

  @CommandMethod
  public void onCommand(Sender sender,
      @Name("target | id") String targetStr,
      @Name("points | c | /2") String newPointsStr,
      @ConsumeRest String reason) {
    WeakReference<Sender> weakSender = new WeakReference<>(sender);
    RemovablePunishment punishment;
    if (targetStr.startsWith("#")) {
      String id = targetStr.substring(1).toUpperCase();
      RemovablePunishment punish = VenadeAPI.getPunishmentService()
          .getPunishmentById(id, type);
      if (punish == null) {
        sender.sendMessage("core." + this.getName() + ".error.not.found", id);
        return;
      }
      if (!punish.isActive()) {
        sender.sendMessage("core." + this.getName() + ".error.not.active", id);
        return;
      }
      punishment = punish;

    } else {
      IOfflineCubidPlayer target;
      try {
        target = Providers.provider(IOfflineCubidPlayer.class)
            .provide(new Context(this, weakSender, Collections.emptyList(), targetStr));
      } catch (CommandException e) {
        sender.sendMessage(e.getMessage(sender));
        return;
      }

      punishment = VenadeAPI.getPunishmentService()
          .getActivePunishment(this.type, target.getUniqueId());
      if (punishment == null) {
        sender.sendMessage("core." + this.getName() + ".error.not.punished",
            target.getRank().getColor() + target.getName());
        return;
      }
    }
    int newPoints;
    if (newPointsStr.equalsIgnoreCase("c") || newPointsStr.equalsIgnoreCase("~")
        || newPointsStr.equalsIgnoreCase("-")) {
      newPoints = punishment.getPoints();
    } else if (newPointsStr.equalsIgnoreCase("/2")) {
      newPoints = punishment.getPoints() / 2;
    } else {
      try {
        newPoints = Providers.provider(Integer.class).provide(new Context(this, weakSender,
            Collections.emptyList(), newPointsStr));
        if (newPoints < 0) {
          throw new CommandException("core.command.argument.error.int.negative", newPoints);
        }
      } catch (CommandException e) {
        sender.sendMessage(e.getMessage(sender));
        return;
      }
    }
    punishment.setRemoved(sender.getUniqueId(), reason,
        CubidCloud.getBridge().getLocalService().getServiceId().getName(),
        System.currentTimeMillis());
    punishment.setPoints(newPoints);
    VenadeAPI.getPunishmentService().savePunishment(punishment);
    VenadeAPI.getPunishmentService().broadcastRemovedPunishment(punishment);
  }

  public static class UnbanCommand extends RemovePunishmentBaseCommand {

    public UnbanCommand() {
      super(Ban.class, "ub");
    }
  }

  public static class UnmuteCommand extends RemovePunishmentBaseCommand {

    public UnmuteCommand() {
      super(Mute.class, "um");
    }
  }
}
