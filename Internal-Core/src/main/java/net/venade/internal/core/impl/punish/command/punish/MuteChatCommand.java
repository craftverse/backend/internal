package net.venade.internal.core.impl.punish.command.punish;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import java.util.HashMap;
import java.util.Map;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 16/12/2021
 */
public class MuteChatCommand extends VenadeCommand {

  public static boolean IS_MUTED = false;

  public MuteChatCommand() {
    super(
        "mutechat",
        "core.mutechat.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        "mc"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    IS_MUTED = !IS_MUTED;
    for (Player player : Bukkit.getOnlinePlayers()) {
      ICorePlayer corePlayer = VenadeAPI.getPlayer(player.getUniqueId());
      if (corePlayer != null) {
        corePlayer.sendMessage(IS_MUTED ? "core.mutechat.muted" : "core.mutechat.unmuted");
      }
    }
    Map<Language, JsonDocument> thing = new HashMap<>();
    String senderName;
    if (sender.isConsole()) {
      senderName = Sender.CONSOLE_NAME;
    } else {
      senderName =
          sender.getAsCorePlayer().getCubidPlayer().getRank().getColor() + sender.getAsPlayer()
              .getName();
    }
    for (Language l : Language.values()) {
      thing.put(l, new JsonDocument().append("message",
          HexColor.translate(CubidLanguage.getLanguageAPI()
              .getLanguageString("core.mutechat." + (IS_MUTED ? "muted" : "unmuted") + "-staff", l,
                  CubidCloud.getBridge().getLocalService().getServiceId().getName(), senderName))));
    }
    StaffMessagePacket packet = new StaffMessagePacket(thing, "mutechat",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), false);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

}
