package net.venade.internal.core.impl.punish.command.punish.blockedname;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.punish.BlockedName;

/**
 * @author JakeMT04
 * @since 23/12/2021
 */
public class AddBlockedNameCommand extends VenadeCommand {

  public AddBlockedNameCommand() {
    super(
        "add",
        "core.blockedname.command.add.desc",
        null
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, String name, @ConsumeRest String reason) {
    name = name.toUpperCase();
    if (VenadeAPI.getPunishmentService().isBlockedName(name)) {
      sender.sendMessage("core.blockedname.already", name);
      return;
    }
    BlockedName blockedName = new BlockedName(name, sender.getUniqueId(),
        System.currentTimeMillis(), reason);
    VenadeAPI.getPunishmentService().saveBlockedName(blockedName);
    VenadeAPI.getPunishmentService().applyBlockedName(blockedName);
    VenadeAPI.getPunishmentService().broadcastBlockedName(blockedName);
//    sender.sendMessage("core.blockedname.added", name);
  }

}
