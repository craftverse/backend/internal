package net.venade.internal.core.bukkit;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.bukkit.listener.PlayerChatListener;
import net.venade.internal.core.bukkit.listener.PlayerJoinQuitListener;
import net.venade.internal.core.bukkit.listener.PunishmentPreventionsListener;
import net.venade.internal.core.impl.filter.ChatFilterManager;
import net.venade.internal.core.impl.particle.ParticleManager;
import net.venade.internal.core.labymod.LabyModChannelHandler;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
public class BukkitCore extends JavaPlugin {

  private static BukkitCore instance;
  private VenadeCore core;

  public static BukkitCore getInstance() {
    return instance;
  }

  @Override
  public void onEnable() {
    instance = this;
    this.core = new VenadeCore();
    this.core.initializeBukkit(this);

    this.register();
    this.handleLabymod();
    this.setDefaultValues();

    VenadeAPI.setParticleManager(new ParticleManager(this));
  }

  private void setDefaultValues() {
    Bukkit.getWorlds().forEach(world -> world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false));
  }

  private void handleLabymod() {
    new LabyModChannelHandler(this);
  }

  private void register() {
    new PlayerJoinQuitListener(this.core, this);
    this.getServer().getPluginManager()
        .registerEvents(new PunishmentPreventionsListener(this.core), this);
    this.getServer().getPluginManager().registerEvents(new PlayerChatListener(), this);
    this.getServer().getPluginManager()
        .registerEvents(new ChatFilterManager(this.core.getMongoManager()), this);
  }
}
