package net.venade.internal.core.impl.gui.template;

import java.util.ArrayList;
import java.util.List;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.content.SlotIterator;
import net.venade.internal.api.inventory.content.SlotIterator.Type;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.punish.template.PunishmentTemplate;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class TemplateGUI implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final SmartInventory inventory;
  private final String type;

  public TemplateGUI(ICorePlayer corePlayer, String type) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("template-" + type)
        .title(HexColor.translate("Edit templates"))
        .closeable(true)
        .size(4, 9)
        .build();
    this.type = type;
    this.corePlayer = corePlayer;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    List<ClickableItem> items = new ArrayList<>();
    for (PunishmentTemplate template : VenadeAPI.getPunishmentTemplateService()
        .getPunishmentTemplates(this.type)) {
      Material mat = Material.RED_WOOL;
      if (this.type.equals("ban")) {
        mat = Material.CHEST;
      } else if (this.type.equals("mute")) {
        mat = Material.OAK_SIGN;
      }
      ItemStack item = ItemBuilder.builder(mat)
          .amount(1)
          .unbreakable()
          .display("§b§l" + template.getName())
          .lore(
              "§fIdentifier: §b" + template.getIdentifier(),
              "§fReason: §b" + template.getReason(),
              "§fPoints to issue: §b" + template.getPointsToIssue(),
              "",
              "§bLeft click §fto edit reason.",
              "§bRight click §fto edit points.",
              "§bShift + left click §fto edit identifier.",
              "§bShift + right click §fto delete."
          )
          .build();
      items.add(ClickableItem.of(item, (e) -> {
        e.setCancelled(true);
        this.process(e, template);
      }));

    }
    contents.pagination()
        .setItemsPerPage(this.inventory.getColumns() * (this.inventory.getRows() - 1));
    contents.pagination().setItems(items);
    fillPage(0, contents);
  }

  public void fillPage(int page, InventoryContents contents) {
    contents.clear();
    SlotIterator iterator = contents.newIterator(Type.HORIZONTAL, 0, 0);
    contents.pagination().page(page);
    contents.pagination().addToIterator(iterator);
    contents.fillRow(3, ClickableItem.empty(
        ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
    if (!contents.pagination().isFirst()) {
      ItemStack backButton = ItemBuilder.builder(Material.MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.prev"))
          .amount(1)
          .build();
      contents.set(3, 0, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() - 1, contents)));
    }
    if (!contents.pagination().isLast()) {
      ItemStack backButton = ItemBuilder.builder(Material.GLISTERING_MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.next"))
          .amount(1)
          .build();
      contents.set(3, 8, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() + 1, contents)));
    }
    ItemStack newItem = ItemBuilder.builder(Material.CRAFTING_TABLE)
        .display("§bCreate new")
        .build();
    contents.set(3, 4, ClickableItem.of(newItem, (e) -> this.doNew()));
  }

  public void doNew() {
    this.closeInventory();
    new GetChatMessage(corePlayer.getPlayer(),
        corePlayer.getLanguageString("core.punish.template.create.name"), (name) -> {
      if (name.equalsIgnoreCase("cancel")) {
        this.openInventory();
        return;
      }
      new GetChatMessage(corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.template.create.identifier"),
          (gcm, identifierStr) -> {
            if (identifierStr.equalsIgnoreCase("cancel")) {
              this.openInventory();
              return;
            }
            String identifier = identifierStr.toLowerCase();
            if (VenadeAPI.getPunishmentTemplateService().punishmentTemplateExists(type, identifier)) {
              corePlayer.sendMessage("core.punish.template.edit.identifier.inuse", identifier);
              corePlayer.sendMessage("core.punish.template.create.identifier");
              gcm.reissue();
              return;
            }
            new GetChatMessage(corePlayer.getPlayer(),
                corePlayer.getLanguageString("core.punish.template.create.reason"), (reason) -> {
              if (reason.equalsIgnoreCase("cancel")) {
                this.openInventory();
                return;
              }
              new GetChatMessage(corePlayer.getPlayer(),
                  corePlayer.getLanguageString("core.punish.template.create.points"),
                  (gcm2, pointsStr) -> {
                    if (pointsStr.equalsIgnoreCase("cancel")) {
                      this.openInventory();
                      return;
                    }
                    int points;
                    try {
                      points = Integer.parseInt(pointsStr);
                    } catch (NumberFormatException e) {
                      corePlayer.sendMessage("core.command.argument.error.int", pointsStr);
                      corePlayer.sendMessage("core.punish.template.create.points");
                      gcm2.reissue();
                      return;
                    }
                    PunishmentTemplate template = new PunishmentTemplate(null, name, identifier,
                        reason, type, points);
                    VenadeAPI.getPunishmentTemplateService().savePunishmentTemplate(template);
                    corePlayer.sendMessage("core.punish.template.create.done");
                    this.openInventory();
                  });

            });
          });
    });
  }

  public void process(InventoryClickEvent event, PunishmentTemplate template) {
    if (event.getClick() == ClickType.LEFT) {
      // Edit reason
      this.closeInventory();
      new GetChatMessage(corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.template.edit.reason"), (reason) -> {
        if (!reason.equalsIgnoreCase("cancel")) {
          template.setReason(reason);
          VenadeAPI.getPunishmentTemplateService().savePunishmentTemplate(template);
        }
        this.openInventory();
      });
    } else if (event.getClick() == ClickType.RIGHT) {
      this.closeInventory();
      new GetChatMessage(corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.template.edit.points"), (gcm, points) -> {
        if (!points.equalsIgnoreCase("cancel")) {
          int newPoints;
          try {
            newPoints = Integer.parseInt(points);
            template.setPointsToIssue(newPoints);
            VenadeAPI.getPunishmentTemplateService().savePunishmentTemplate(template);
          } catch (NumberFormatException e) {
            corePlayer.sendMessage("core.command.argument.error.int", points);
            corePlayer.sendMessage("core.punish.template.edit.points");
            gcm.reissue();
            return;
          }
        }
        this.openInventory();
      });
    } else if (event.getClick() == ClickType.SHIFT_LEFT) {
      this.closeInventory();
      new GetChatMessage(corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.template.edit.identifier"), (identifier) -> {
        if (!identifier.equalsIgnoreCase("cancel")) {
          template.setIdentifier(identifier.toLowerCase());
          VenadeAPI.getPunishmentTemplateService().savePunishmentTemplate(template);
        }
        this.openInventory();
      });
    } else if (event.getClick() == ClickType.SHIFT_RIGHT) {
      // Delete
      VenadeAPI.getPunishmentTemplateService().deletePunishmentTemplate(template);
      this.openInventory();
    }
  }

  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
