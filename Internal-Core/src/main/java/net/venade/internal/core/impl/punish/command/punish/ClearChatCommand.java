package net.venade.internal.core.impl.punish.command.punish;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.HashMap;
import java.util.Map;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 16/12/2021
 */
public class ClearChatCommand extends VenadeCommand {

  public ClearChatCommand() {
    super(
        "clearchat",
        "core.clearchat.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        "cc"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    for (Player player : Bukkit.getOnlinePlayers()) {
      ICorePlayer corePlayer = VenadeAPI.getPlayer(player.getUniqueId());
      if (corePlayer != null) {
        if (!corePlayer.getCubidPlayer().hasRankOrHigher(CubidRank.MOD)) {
          for (int i = 0; i < 100; i++) {
            player.sendMessage("\n");
          }
          corePlayer.sendMessage("core.clearchat.cleared");
        }
      }
    }
    Map<Language, JsonDocument> thing = new HashMap<>();
    String senderName;
    if (sender.isConsole()) {
      senderName = Sender.CONSOLE_NAME;
    } else {
      senderName =
          sender.getAsCorePlayer().getCubidPlayer().getRank().getColor() + sender.getAsPlayer().getName();
    }
    for (Language l : Language.values()) {
      thing.put(l, new JsonDocument().append("message",
          HexColor.translate(CubidLanguage.getLanguageAPI()
              .getLanguageString("core.clearchat.cleared-staff", l,
                  CubidCloud.getBridge().getLocalService().getServiceId().getName(), senderName))));
    }
    StaffMessagePacket packet = new StaffMessagePacket(thing, "clearchat",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), false);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

}
