package net.venade.internal.core.impl.report;

import cloud.cubid.bridge.CubidCloud;
import com.google.common.base.Joiner;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.annotation.CustomProvider;
import net.venade.internal.api.command.annotation.Optional;
import net.venade.internal.api.report.Report;
import net.venade.internal.api.report.Report.ReportReason;
import net.venade.internal.core.impl.gui.report.ReportGUI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 26/10/2021
 */
public class ReportCommand extends VenadeCommand {

  private final JavaPlugin plugin;

  public ReportCommand(JavaPlugin plugin) {
    super("report", "core.report.desc", null);
    this.setAsync(true);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender, @CustomProvider("doNotFetch") IOfflineCubidPlayer target,
      @ConsumeRest @Optional String reason) {
    if (target.getUniqueId().equals(sender.getUniqueId())) {
      sender.sendMessage("core.report.self");
      return;
    }
    if (VenadeAPI.getReportService().hasOpenReport(sender.getUniqueId(), target.getUniqueId())) {
      sender.sendMessage("core.report.already", target.getRank().getColor() + target.getName());
      return;
    }
    if (reason == null) {
      Bukkit.getScheduler().runTask(plugin,
          () -> new ReportGUI.ReasonGUI(sender.getAsCorePlayer(), target).openInventory());
      return;
    }
    ReportReason reportReason = Report.REPORT_REASONS.get(reason.toUpperCase());
    if (reportReason == null) {
      sender.sendMessage("core.report.reason.unknown",
          Joiner.on(", ").join(Report.REPORT_REASONS.keySet().toArray()));
      return;
    }
    new GetChatMessage(sender.getAsPlayer(), sender.getLanguageString("core.report.confirm",
        target.getRank().getColor() + target.getName(), reportReason.getDisplayReason()), (s) -> {
      if (s.equalsIgnoreCase("confirm")) {
        Report report = Report.builder()
            .reporter(sender.getUniqueId())
            .reported(target.getUniqueId())
            .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
            .reason(reportReason.getReportReason())
            .closedByMute(reportReason.isClosedByMute())
            .reportedAt(System.currentTimeMillis())
            .build();

        VenadeAPI.getReportService().saveReport(report);
        VenadeAPI.getReportService().broadcastReport(report);
        sender.sendMessage("core.report.success", target.getRank().getColor() + target.getName(),
            reportReason.getDisplayReason());
      } else {
        sender.sendMessage("core.report.cancelled");
      }
    });

  }

}
