package net.venade.internal.core.impl.punish.command.template;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.core.impl.gui.template.TemplateMainMenu;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public class PunishTemplateCommand extends VenadeCommand {

  public PunishTemplateCommand() {
    super(
        "punishtemplate",
        "core.punish.template.desc",
        Permission.has(CubidRank.ADMIN, CubidRank.LEAD),
        "pt"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    TemplateMainMenu gui = new TemplateMainMenu(sender.getAsCorePlayer());
    gui.openInventory();
  }

}
