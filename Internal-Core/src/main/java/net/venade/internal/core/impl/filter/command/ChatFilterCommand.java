package net.venade.internal.core.impl.filter.command;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.permission.Permission;

/**
 * @author JakeMT04
 * @since 21/11/2021
 */
public class ChatFilterCommand extends VenadeCommand {
  public ChatFilterCommand() {
    super(
        "chatfilter",
        "core.filter.command.desc",
        Permission.hasOrHigher(CubidRank.SR_DEV),
        true
    );
    this.setAsync(true);
    this.registerSubCommand(new AddEntryCommand());
    this.registerSubCommand(new RemoveEntryCommand());
  }

}
