package net.venade.internal.core.bukkit.listener;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.core.bukkit.BukkitCore;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class PlayerChatListener implements Listener {

  @EventHandler
  public void onChat(AsyncPlayerChatEvent e) {
    if (GetChatMessage.users.containsKey(e.getPlayer().getUniqueId())) {
      e.setCancelled(true);
      Bukkit.getScheduler().runTask(JavaPlugin.getPlugin(BukkitCore.class),
          () -> GetChatMessage.users.get(e.getPlayer().getUniqueId()).handle(e.getPlayer(), e));
    }
  }

  @EventHandler
  public void onCommand(PlayerCommandPreprocessEvent e) {
    if (GetChatMessage.users.containsKey(e.getPlayer().getUniqueId())) {
      e.setCancelled(true);
      GetChatMessage.users.get(e.getPlayer().getUniqueId()).handle(e.getPlayer(), e);
    }
  }
}
