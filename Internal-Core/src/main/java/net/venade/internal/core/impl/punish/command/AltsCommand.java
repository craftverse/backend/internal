package net.venade.internal.core.impl.punish.command;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.CustomProvider;
import net.venade.internal.api.date.Dates;
import net.venade.internal.api.player.PlayerAssociation;
import net.venade.internal.api.player.PlayerIPEntry;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.digest.DigestUtils;

/**
 * @author JakeMT04
 * @since 15/12/2021
 */
public class AltsCommand extends VenadeCommand {

  public AltsCommand() {
    super(
        "alts",
        "core.command.alts",
        DefaultPunishmentService.COMMAND_PERMISSION
    );
    this.setAsync(true);
  }

  @CommandMethod
  public void onCommand(Sender sender, @CustomProvider("doNotFetch") IOfflineCubidPlayer target) {
    sender.sendMessage("core.alts.processing");
    List<PlayerAssociation> associatedPlayers = VenadeAPI.getPlayerService()
        .getMatchingPlayers(target.getUniqueId());
    if (associatedPlayers.isEmpty()) {
      sender.sendMessage("core.alts.none", target.getRank().getColor() + target.getName());
      return;
    }
    List<UUID> processed = new ArrayList<>();
    HashMap<UUID, Set<IOfflineCubidPlayer>> matchList = new HashMap<>();

    HashMap<UUID, IOfflineCubidPlayer> playerNameLookupTable = new HashMap<>();
    playerNameLookupTable.put(target.getUniqueId(), target);
    for (PlayerAssociation assoc : associatedPlayers) {
      Set<IOfflineCubidPlayer> set = matchList.computeIfAbsent(assoc.getTarget(),
          e -> new HashSet<>());
      IOfflineCubidPlayer associationTarget = playerNameLookupTable.computeIfAbsent(
          assoc.getSource(), uuid -> CubidCloud.getBridge().getPlayerManager()
              .getOfflinePlayer(uuid).orElse(null));
      set.add(associationTarget);

    }

    TextComponent alts = HexColor.translateToCompoment(
        sender.getNonTranslatedLanguageString("core.alts.header",
            target.getRank().getColor() + target.getName(), matchList.size()));
    boolean first = true;

    String currentIp = DigestUtils.md5Hex(target.getIp());
    for (PlayerAssociation association : associatedPlayers) {
      if (processed.contains(association.getTarget())) {
        continue;
      }
      processed.add(association.getTarget());
      IOfflineCubidPlayer associationTarget = playerNameLookupTable.computeIfAbsent(
          association.getTarget(), uuid -> CubidCloud.getBridge().getPlayerManager()
              .getOfflinePlayer(uuid).orElse(null));
      if (associationTarget == null) {
        if (first) {
          first = false;
        } else {
          alts.addExtra(", ");
        }
        alts.addExtra("Unable to process");
        continue;
      }
      TextComponent component = new TextComponent(associationTarget.getName());
      // Check if banned / muted
      Ban ban = VenadeAPI.getPunishmentService()
          .getActivePunishment(Ban.class, associationTarget.getUniqueId());
      Mute mute = VenadeAPI.getPunishmentService()
          .getActivePunishment(Mute.class, associationTarget.getUniqueId());
      boolean isOnline = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(associationTarget.getUniqueId()).isPresent();
      if (isOnline) {
        component.setColor(ChatColor.GREEN);
      } else {
        component.setColor(ChatColor.GRAY);
      }
      StringBuilder hoverText = new StringBuilder(
          sender.getNonTranslatedLanguageString("core.alts.hover.match"));
      boolean f = true;
      for (IOfflineCubidPlayer matchedPlayer : matchList.get(association.getTarget())) {
        if (f) {
          f = false;
        } else {
          hoverText.append("§r, ");
        }
        hoverText.append(matchedPlayer.getRank().getColor()).append(matchedPlayer.getName());
      }
      if (association.getMatching().isEmpty()) {
        hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.never"));
      } else if (association.getMatching().size() == 1) {
        PlayerIPEntry entry = association.getMatching().get(0);
        hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.main-one",
            Dates.FORMATTER.format(entry.getFirstSeen()),
            Dates.FORMATTER.format(entry.getLastSeen()), entry.getLoginCount()));
        if (entry.getIp().equals(currentIp)) {
          hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.current-match"));
        }
      } else {
        hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.main-mult",
            association.getMatching().size()));
        if (association.getMatching().get(0).getIp().equals(currentIp)) {
          hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.current-match"));
        }
      }
      if (ban != null) {
        String banSource;
        if (ban.getExecutor().equals(Sender.CONSOLE_UUID)) {
          banSource = Sender.CONSOLE_NAME;
        } else {
          IOfflineCubidPlayer player = playerNameLookupTable.computeIfAbsent(
              ban.getExecutor(), uuid -> CubidCloud.getBridge().getPlayerManager()
                  .getOfflinePlayer(uuid).orElse(null));
          if (player != null) {
            banSource = player.getRank().getColor() + player.getName();
          } else {
            banSource = "error";
          }
        }
        hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.banned",
            banSource,
            ban.getExpiry() == 0 ? "Never" : Dates.FORMATTER.format(ban.getExpiry()),
            ban.getReason()));
        component.setColor(ChatColor.RED);
      }
      if (mute != null) {
        String muteSource;
        if (mute.getExecutor().equals(Sender.CONSOLE_UUID)) {
          muteSource = Sender.CONSOLE_NAME;
        } else {
          IOfflineCubidPlayer player = playerNameLookupTable.computeIfAbsent(
              mute.getExecutor(), uuid -> CubidCloud.getBridge().getPlayerManager()
                  .getOfflinePlayer(uuid).orElse(null));
          if (player != null) {
            muteSource = player.getRank().getColor() + player.getName();
          } else {
            muteSource = "error";
          }
        }
        hoverText.append(sender.getNonTranslatedLanguageString("core.alts.hover.muted",
            muteSource,
            mute.getExpiry() == 0 ? "Never" : Dates.FORMATTER.format(mute.getExpiry()),
            mute.getReason()));
        component.setItalic(true);
      }
      TextComponent hoverComponent = HexColor.translateToCompoment(hoverText.toString());
      component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
          new Text(hoverComponent.getExtra().toArray(new BaseComponent[0]))));
      if (first) {
        first = false;
      } else {
        alts.addExtra(", ");
      }
      alts.addExtra(component);
    }
    sender.getAsCommandSender().spigot().sendMessage(alts);

  }
}
