package net.venade.internal.core.bukkit.listener;

import cloud.cubid.bridge.CubidCloud;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.venade.internal.api.chat.ChatLogMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author Nikolas Rummel
 * @since 20.12.21
 */
public class ChatlogListener implements Listener {

    private static final String CHATLOG = "CHATLOG";
    private static final int CHATLOG_DB = 4;
    private Gson gson = new GsonBuilder().create();

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        String jasonMessage = gson.toJson(new ChatLogMessage(
                System.currentTimeMillis(),
                event.getMessage(),
                CubidCloud.getBridge().getLocalService().getServiceId().getName()
        ));

        CubidCloud.getBridge().getRedisManager().async(CHATLOG_DB).sadd("CHATLOG:" + event.getPlayer().getUniqueId(), jasonMessage);
        CubidCloud.getBridge().getRedisManager().async(CHATLOG_DB).expire("CHATLOG:" + event.getPlayer().getUniqueId(), 86400);
    }
}
