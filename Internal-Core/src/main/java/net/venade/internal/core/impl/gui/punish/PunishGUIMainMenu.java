package net.venade.internal.core.impl.gui.punish;

import cubid.cloud.modules.player.IOfflineCubidPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class PunishGUIMainMenu implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final IOfflineCubidPlayer targetPlayer;
  private final SmartInventory inventory;
  private final int currentBanPoints;
  private final int currentMutePoints;
  private final boolean isBanned;
  private final boolean isMuted;

  public PunishGUIMainMenu(ICorePlayer corePlayer, IOfflineCubidPlayer targetPlayer,
      int currentBanPoints, int currentMutePoints, boolean isBanned, boolean isMuted) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("punish")
        .title(HexColor.translate(
            "Punish " + targetPlayer.getRank().getColor() + targetPlayer.getName()))
        .closeable(true)
        .size(1, 9)
        .build();
    this.corePlayer = corePlayer;
    this.targetPlayer = targetPlayer;
    this.currentBanPoints = currentBanPoints;
    this.currentMutePoints = currentMutePoints;
    this.isBanned = isBanned;
    this.isMuted = isMuted;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    ItemBuilder banItemBuilder = ItemBuilder.builder(Material.CHEST)
        .display((isBanned ? "§c" : "§b") + "Ban")
        .amount(Math.min(Math.max(1, currentBanPoints), 64));
    if (isBanned) {
      banItemBuilder.lore("§fCurrent points: §b" + currentBanPoints,
          "§cCannot ban: User already has active ban");
    } else {
      banItemBuilder.lore("§fCurrent points: §b" + currentBanPoints);
    }
    ItemStack banItem = banItemBuilder.build();

    ClickableItem banItemClickable = ClickableItem.of(banItem, (event) -> {
      if (isBanned) {
        return;
      }
      int currentPoints = VenadeAPI.getPunishmentService()
          .getPunishmentPoints("ban", targetPlayer.getUniqueId());
      new PunishGUI(corePlayer, targetPlayer, currentPoints, "ban").openInventory();
    });

    ItemBuilder muteItemBuilder = ItemBuilder.builder(Material.OAK_SIGN)
        .display((isMuted ? "§c" : "§b") + "Mute")
        .amount(Math.min(Math.max(1, currentMutePoints), 64));
    if (isMuted) {
      muteItemBuilder.lore("§fCurrent points: §b" + currentBanPoints,
          "§cCannot mute: User already has active mute");
    } else {
      muteItemBuilder.lore("§fCurrent points: §b" + currentMutePoints);
    }
    ItemStack muteItem = muteItemBuilder.build();
    ClickableItem muteItemClickable = ClickableItem.of(muteItem, (event) -> {
      if (isMuted) {
        return;
      }
      int currentPoints = VenadeAPI.getPunishmentService()
          .getPunishmentPoints("mute", targetPlayer.getUniqueId());
      new PunishGUI(corePlayer, targetPlayer, currentPoints, "mute").openInventory();
    });
    contents.set(0, 2, banItemClickable);
    contents.set(0, 6, muteItemClickable);

  }

  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
