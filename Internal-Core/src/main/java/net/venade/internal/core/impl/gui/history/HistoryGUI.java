package net.venade.internal.core.impl.gui.history;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.date.Dates;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.content.SlotIterator;
import net.venade.internal.api.inventory.content.SlotIterator.Type;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.RemovablePunishment;
import net.venade.internal.api.punish.type.TemporaryPunishment;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public class HistoryGUI implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final List<? extends Punishment> punishments;
  private final Map<UUID, String> playerNames;
  private final SmartInventory inventory;

  public HistoryGUI(ICorePlayer corePlayer, IOfflineCubidPlayer targetPlayer,
      List<? extends Punishment> punishments, Map<UUID, String> playerNames) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("hist")
        .title(HexColor.translate(
            "History for " + targetPlayer.getRank().getColor() + targetPlayer.getName()))
        .closeable(true)
        .size(4, 9)
        .build();

    this.corePlayer = corePlayer;
    this.punishments = punishments;
    this.playerNames = playerNames;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  public Material getMaterial(Punishment punishment) {
    if (punishment.isActive()) {
      return Material.GREEN_WOOL;
    }
    if (punishment instanceof RemovablePunishment) {
      if (((RemovablePunishment) punishment).wasRemoved()) {
        return Material.RED_WOOL;
      }
    }
    if (punishment instanceof TemporaryPunishment) {
      if (((TemporaryPunishment) punishment).hasExpired()) {
        return Material.GRAY_WOOL;
      }
    }
    return Material.RED_WOOL;
  }

  public boolean canModify() {
    ICubidPlayer cubidPlayer = this.corePlayer.getCubidPlayer();
    for (CubidRank rank : new CubidRank[]{CubidRank.COMM, CubidRank.LEAD, CubidRank.ADMIN,
        CubidRank.SR_DEV, CubidRank.SR_MOD, CubidRank.MOD}) {
      if (cubidPlayer.hasRank(rank)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    List<ClickableItem> items = new ArrayList<>();
    for (Punishment punishment : punishments) {

      boolean isActive = punishment.isActive();
      boolean wasRemoved = punishment instanceof RemovablePunishment
          && ((RemovablePunishment) punishment).wasRemoved();
      boolean wasReduced = punishment instanceof TemporaryPunishment
          && ((TemporaryPunishment) punishment).wasReduced();
      boolean hasExpired = punishment instanceof TemporaryPunishment
          && ((TemporaryPunishment) punishment).hasExpired();
      List<String> lore = new ArrayList<>();
      lore.add("");
      lore.add(corePlayer.getLanguageString("core.history.gui.header.added"));
      lore.add(corePlayer.getLanguageString("core.history.gui.by",
          playerNames.getOrDefault(punishment.getExecutor(), "§7§ounknown :(")));
      String reason = corePlayer.getLanguageString("core.history.gui.reason",
          punishment.getReason());
      for (String line : WordUtils.wrap(reason, 50, "\n", false)
          .split("\n")) {
        lore.add("§b" + line);
      }
      lore.add(corePlayer.getLanguageString("core.history.gui.server", punishment.getServer()));
      lore.add(corePlayer.getLanguageString("core.history.gui.id", punishment.getPunishmentId()));
      lore.add(
          corePlayer.getLanguageString("core.history.gui.points", punishment.getOriginalPoints()));
      if (punishment instanceof TemporaryPunishment tp) {
        if (tp.isPermanent()) {
          lore.add(corePlayer.getLanguageString("core.history.gui.duration.perm"));
        } else {
          if (!wasRemoved) {
            lore.add(0, "§c" + Dates.FORMATTER.format(tp.getExpiry()));
          }
          if (tp.getOriginalExpiry() == 0) {
            lore.add(corePlayer.getLanguageString("core.history.gui.duration.perm"));
          } else {
            String initialDuration = Dates.formatDateDiff(punishment.getIssued(),
                tp.getOriginalExpiry(), 0, false);
            lore.add(
                corePlayer.getLanguageString("core.history.gui.duration.temp", initialDuration));
          }

          if (isActive && !tp.hasExpired()) {
            String remaining = Dates.formatDateDiff(System.currentTimeMillis(), tp.getExpiry(), 0,
                true);
            lore.add(corePlayer.getLanguageString("core.history.gui.remain", remaining));
          }
        }
      }
      lore.add("");
      lore.add(corePlayer.getLanguageString("core.history.gui.note"));
      for (String line : WordUtils.wrap(punishment.getInternalNotes(), 50, "\n", false)
          .split("\n")) {
        lore.add("§b" + line);
      }
      if (wasReduced && !wasRemoved) {
        TemporaryPunishment tp = (TemporaryPunishment) punishment;
        lore.add("");
        lore.add(corePlayer.getLanguageString("core.history.gui.header.reduced"));
        lore.add(corePlayer.getLanguageString("core.history.gui.by",
            playerNames.getOrDefault(tp.getReducedBy(), "§7§ounknown :(")));
        lore.add(corePlayer.getLanguageString("core.history.gui.reason", tp.getReducedReason()));
        lore.add(corePlayer.getLanguageString("core.history.gui.server", tp.getReducedServer()));
        lore.add(corePlayer.getLanguageString("core.history.gui.duration.new",
            Dates.formatDateDiff(tp.getReducedOn(), tp.getExpiry(), 0, false)));
      }
      if (!isActive) {
        if (wasRemoved) {
          RemovablePunishment rp = (RemovablePunishment) punishment;
          lore.add(0, "§c" + Dates.FORMATTER.format(rp.getRemovedOn()));
          lore.add("");
          lore.add(corePlayer.getLanguageString("core.history.gui.header.removed"));
          lore.add(corePlayer.getLanguageString("core.history.gui.by",
              playerNames.getOrDefault(rp.getRemovedBy(), "§7§ounknown :(")));
          lore.add(corePlayer.getLanguageString("core.history.gui.reason", rp.getRemovedReason()));
          lore.add(corePlayer.getLanguageString("core.history.gui.server", rp.getRemovedServer()));
          if (punishment.getOriginalPoints() != punishment.getPoints()) {
            lore.add(corePlayer.getLanguageString("core.history.gui.points.new",
                punishment.getPoints()));
          }
        } else {
          if (punishment.getOriginalPoints() != punishment.getPoints()) {
            lore.add(corePlayer.getLanguageString("core.history.gui.points.new",
                punishment.getPoints()));
          }
          if (hasExpired) {
            lore.add("");
            lore.add(corePlayer.getLanguageString("core.history.gui.header.expired"));
          }
        }

      } else {
        if (punishment.getOriginalPoints() != punishment.getPoints()) {
          lore.add(
              corePlayer.getLanguageString("core.history.gui.points.new", punishment.getPoints()));
        }
        if (canModify()) {
          lore.add("");
          lore.add(corePlayer.getLanguageString("core.history.gui.remove"));
          if (!wasReduced) {
            lore.add(corePlayer.getLanguageString("core.history.gui.reduce"));
          }
        }
      }

      ItemStack item = ItemBuilder.builder(getMaterial(punishment))
          .display(
              "§a" + Dates.FORMATTER.format(punishment.getIssued()) + (isActive ? " §a§l(Active)"
                  : ""))
          .setLore(lore)
          .build();
      ClickableItem clickableItem = ClickableItem.of(item, (e) -> doClick(e, punishment));
      items.add(clickableItem);

    }
    contents.pagination()
        .setItemsPerPage(this.inventory.getColumns() * (this.inventory.getRows() - 1));
    contents.pagination().setItems(items);
    fillPage(0, contents);
  }

  public void doClick(InventoryClickEvent event, Punishment punishment) {
    if (!canModify()) {
      return;
    }
    if (!punishment.isActive()) {
      return;
    }
    if (event.getClick() == ClickType.LEFT) {
      this.doRemove((RemovablePunishment) punishment);
    } else if (event.getClick() == ClickType.RIGHT) {
      this.doReduce(punishment);
    }
  }

  public void doReduce(Punishment punishment) {
    TemporaryPunishment tp = (TemporaryPunishment) punishment;
    if (!punishment.isActive() || tp.wasReduced()) {
      return;
    }
    this.closeInventory();
    new GetChatMessage(this.corePlayer.getPlayer(),
        corePlayer.getLanguageString("core.punish.reduce.duration"), (gcm, duration) -> {
      if (duration.equalsIgnoreCase("cancel")) {
        this.openInventory();
        return;
      }
      long now = System.currentTimeMillis();
      long expiry = Dates.parseDateDiff(duration, true);
      if (expiry == 0) {
        corePlayer.sendMessage("core.punish.reduce.error.perm");
        corePlayer.sendMessage("core.punish.reduce.duration");
        gcm.reissue();
        return;
      }
      long diffNew = expiry - now;
      long diffOld = tp.getOriginalExpiry() - punishment.getIssued();
      if (!tp.isPermanent() && diffNew > diffOld) {
        corePlayer.sendMessage("core.punish.reduce.error.longer");
        corePlayer.sendMessage("core.punish.reduce.duration");
        gcm.reissue();
        return;
      }
      new GetChatMessage(this.corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.reduce.reason"), (reason) -> {
        if (reason.equalsIgnoreCase("cancel")) {
          this.openInventory();
          return;
        }
        new GetChatMessage(this.corePlayer.getPlayer(),
            corePlayer.getLanguageString("core.punish.reduce.points", punishment.getPoints()),
            (gcm2, pointsStr) -> {
              if (pointsStr.equalsIgnoreCase("cancel")) {
                this.openInventory();
                return;
              }
              int points;
              try {
                points = Integer.parseInt(pointsStr);
                if (points < 0) {
                  throw new CommandException("core.command.argument.error.int.negative", points);
                }
              } catch (NumberFormatException e) {
                corePlayer.sendMessage("core.command.argument.error.int", pointsStr);
                corePlayer.sendMessage("core.punish.reduce.points");
                gcm.reissue();
                return;
              } catch (CommandException e) {
                corePlayer.sendMessage(e.getMessage(corePlayer.getCubidPlayer().getLanguage()));
                corePlayer.sendMessage("core.punish.reduce.points");
                gcm.reissue();
                return;
              }
              tp.setReducedOn(now);
              tp.setExpiry(expiry);
              tp.setReducedServer(
                  CubidCloud.getBridge().getLocalService().getServiceId().getName());
              tp.setReducedBy(corePlayer.getUniqueId());
              tp.setReducedReason(reason);
              punishment.setPoints(points);
              VenadeAPI.getPunishmentService().savePunishment(punishment);
              VenadeAPI.getPunishmentService().broadcastReducedPunishment(punishment);
            });
      });
    });

  }

  public void doRemove(RemovablePunishment punishment) {
    if (!punishment.isActive()) {
      return;
    }
    this.closeInventory();
    new GetChatMessage(this.corePlayer.getPlayer(),
        corePlayer.getLanguageString("core.punish.remove.reason"), (reason) -> {
      if (reason.equalsIgnoreCase("cancel")) {
        this.openInventory();
        return;
      }
      new GetChatMessage(this.corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.remove.points", punishment.getPoints()),
          (gcm, pointsStr) -> {
            if (pointsStr.equalsIgnoreCase("cancel")) {
              this.openInventory();
              return;
            }
            int points;
            try {
              points = Integer.parseInt(pointsStr);
              if (points < 0) {
                throw new CommandException("core.command.argument.error.int.negative", points);
              }
            } catch (NumberFormatException e) {
              corePlayer.sendMessage("core.command.argument.error.int", pointsStr);
              corePlayer.sendMessage("core.punish.remove.points");
              gcm.reissue();
              return;
            } catch (CommandException e) {
              corePlayer.sendMessage(e.getMessage(corePlayer.getCubidPlayer().getLanguage()));
              corePlayer.sendMessage("core.punish.remove.points");
              gcm.reissue();
              return;
            }
            punishment.setRemoved(corePlayer.getPlayer().getUniqueId(), reason,
                CubidCloud.getBridge().getLocalService().getServiceId().getName(),
                System.currentTimeMillis());
            punishment.setPoints(points);
            VenadeAPI.getPunishmentService().savePunishment(punishment);
            VenadeAPI.getPunishmentService().broadcastRemovedPunishment(punishment);
          });
    });
  }

  public void fillPage(int page, InventoryContents contents) {
    contents.clear();
    SlotIterator iterator = contents.newIterator(Type.HORIZONTAL, 0, 0);
    contents.pagination().page(page);
    contents.pagination().addToIterator(iterator);
    contents.fillRow(3, ClickableItem.empty(
        ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
    if (!contents.pagination().isFirst()) {
      ItemStack backButton = ItemBuilder.builder(Material.MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.prev"))
          .amount(1)
          .build();
      contents.set(3, 0, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() - 1, contents)));
    }
    if (!contents.pagination().isLast()) {
      ItemStack backButton = ItemBuilder.builder(Material.GLISTERING_MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.next"))
          .amount(1)
          .build();
      contents.set(3, 8, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() + 1, contents)));
    }
  }

  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
