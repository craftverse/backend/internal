package net.venade.internal.core.impl.punish;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.database.redis.IRedisManager;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.punish.service.IReportService;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.report.Report;
import net.venade.internal.core.impl.network.ReportUpdatePacket;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * @author JakeMT04
 * @since 27/10/2021
 */
public class DefaultReportService implements IReportService {

  public static final Permission COMMAND_PERMISSION = Permission.has(CubidRank.COMM, CubidRank.MOD, CubidRank.SR_MOD,
      CubidRank.ADMIN, CubidRank.LEAD);
  private final IMongoManager mongoManager;
  private final IRedisManager redisManager;

  public DefaultReportService(IMongoManager mongoManager, IRedisManager redisManager) {
    this.mongoManager = mongoManager;
    this.redisManager = redisManager;
  }

  private void doReportSave(Report report) {
    try {
      if (report.get_id() == null) {
        Document document = this.mongoManager.toDocument(report);
        this.mongoManager.getCollection("venade_reports").insertOne(document);
        report.set_id(document.getObjectId("_id"));
      } else {
        Document doc = this.mongoManager.toDocument(report);
        doc.remove("_id");
        this.mongoManager.getCollection("venade_reports")
            .replaceOne(Filters.eq("_id", report.get_id()), doc);

      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  @Override
  public List<Report> getReportsForPlayer(UUID uuid, boolean activeOnly) {
    return this.mongoManager.getCollection("venade_reports")
        .find(activeOnly ? Filters.and(Filters.eq("reported", uuid.toString()),
            Filters.eq("closed", false))
            : Filters.eq("reported", uuid.toString()))
        .sort(Sorts.descending("reportedAt"))
        .map((document) -> this.mongoManager.toObject(document, Report.class))
        .into(new ArrayList<>());
  }

  @Override
  public List<Report> getAllOpenReports(boolean includeAssigned) {
    return this.mongoManager.getCollection("venade_reports")
        .find(includeAssigned ? Filters.eq("closed", false)
            : Filters.and(Filters.eq("closed", false), Filters.exists("assignee", false)))
        .sort(Sorts.descending("reportedAt"))
        .map(document -> this.mongoManager.toObject(document, Report.class))
        .into(new ArrayList<>());
  }

  @Override
  public boolean isAlreadyAssignedToReport(UUID uuid) {
    return this.mongoManager.getCollection("venade_reports")
        .countDocuments(
            Filters.and(Filters.eq("assignee", uuid.toString()), Filters.eq("closed", false))) != 0;
  }

  @Override
  public Report getAssignedReport(UUID uuid) {
    return this.mongoManager.getCollection("venade_reports")
        .find(Filters.and(Filters.eq("assignee", uuid.toString()), Filters.eq("closed", false)))
        .limit(1)
        .map(doc -> this.mongoManager.toObject(doc, Report.class))
        .first();
  }

  @Override
  public void sendClosedMessages(Report report) {
    IOfflineCubidPlayer target = CubidCloud.getBridge().getPlayerManager()
        .getOfflinePlayer(report.getReported()).orElseThrow();
    this.mongoManager.runAsync(
        () -> CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(report.getReporter())
            .ifPresent((player) -> player.sendMessage(
                HexColor.translate(player.getLanguageString("core.report.closed",
                    target.getRank().getColor() + target.getName())))));
  }

  @Override
  public void closeApplicableReports(Punishment punishment) {
    IOfflineCubidPlayer target = CubidCloud.getBridge().getPlayerManager()
        .getOfflinePlayer(punishment.getTarget()).orElseThrow();
    List<Report> reports = this.getReportsForPlayer(punishment.getTarget(), true);
    boolean hasClosed = false;
    for (Report report : reports) {
      boolean toClose = false;
      if (punishment.getType().equalsIgnoreCase("ban")) {
        toClose = true;
      } else if (punishment.getType().equalsIgnoreCase("mute") && report.isClosedByMute()) {
        toClose = true;
      }
      if (!toClose) {
        continue;
      }
      hasClosed = true;
      report.setClosed(true);
      report.setClosedAt(System.currentTimeMillis());
      report.setClosedReason(
          "User punished (" + punishment.getType() + " #" + punishment.getPunishmentId() + ")");
      if (report.getAssignee() == null) {
        report.setAssignee(punishment.getExecutor());
      } else {
        this.mongoManager.runAsync(
            () -> CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(report.getAssignee())
                .ifPresent((player) -> player.sendMessage(
                    HexColor.translate(player.getLanguageString("core.report.closed.punished",
                        target.getRank().getColor() + target.getName())))));
      }
      this.sendClosedMessages(report);
      this.saveReport(report);
    }
    if (hasClosed) {
      CubidCloud.getBridge().getCubidNetwork().getPublisher()
          .publish(CubidChannel.SERVER, new ReportUpdatePacket());
    }
  }

  @Override
  public boolean hasOpenReport(UUID reporter, UUID reported) {
    return this.mongoManager.getCollection("venade_reports")
        .countDocuments(Filters.and(Filters.eq("reporter", reporter.toString()),
            Filters.eq("reported", reported.toString()), Filters.eq("closed", false))) != 0;
  }

  @Override
  public Report getReportById(ObjectId id) {
    return this.mongoManager.getObject("venade_reports", Filters.eq("_id", id), Report.class);
  }

  @Override
  public void saveReport(Report report, boolean async) {
    if (async) {
      this.mongoManager.runAsync(() -> this.doReportSave(report));
    } else {
      this.doReportSave(report);
    }
  }

  @Override
  public void broadcastReport(Report report) {
    // fun
    // generate messages
    CubidCloud.getBridge().getCubidNetwork().getPublisher()
        .publish(CubidChannel.SERVER, new ReportUpdatePacket());

    Map<Language, JsonDocument> messages = report.getBroadcastMessage();
    StaffMessagePacket packet = new StaffMessagePacket(messages, "report",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }


}
