package net.venade.internal.core.impl.gui.history;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public class LoadingMenu implements InventoryProvider {

  private final SmartInventory inventory;
  private final ICorePlayer corePlayer;
  private int pos = -1;

  public LoadingMenu(ICorePlayer corePlayer) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("history")
        .title(HexColor.translate("Loading..."))
        .closeable(false)
        .size(1, 9)
        .build();
    this.corePlayer = corePlayer;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    contents.fill(ClickableItem.empty(
        ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§bLoading....").build()));
    update(player, contents);
  }

  @Override
  public void update(Player player, InventoryContents contents) {
    pos += 1;
    if (pos + 1 >= 9) {
      pos = 0;
    }
    contents.fill(ClickableItem.empty(
        ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§bLoading....").build()));
    contents.set(0, pos, ClickableItem.empty(
        ItemBuilder.builder(Material.GREEN_STAINED_GLASS_PANE).display("§bLoading....").build()));
    contents.set(0, pos + 1, ClickableItem.empty(
        ItemBuilder.builder(Material.GREEN_STAINED_GLASS_PANE).display("§bLoading....").build()));

  }
}
