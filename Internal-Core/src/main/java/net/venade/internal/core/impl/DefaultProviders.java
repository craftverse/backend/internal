package net.venade.internal.core.impl;

import cloud.cubid.bridge.CubidCloud;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.CollationStrength;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import cubid.cloud.modules.player.CubidPlayer;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.exc.CommandArgumentException;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Provider;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.ICorePlayer.RecvMode;
import net.venade.internal.api.player.UUIDFetcher;
import net.venade.internal.api.report.Report;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
public class DefaultProviders {

  public static void registerNativeTypes() {
    Providers.bind(Integer.TYPE, (ctx) -> {
      try {
        return Integer.parseInt(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.int", ctx.getArgument());
      }
    });
    Providers.bind(Integer.class, (ctx) -> {
      try {
        return Integer.parseInt(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.int", ctx.getArgument());
      }
    });
    Providers.bind(Long.TYPE, (ctx) -> {
      try {
        return Long.parseLong(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.int", ctx.getArgument());
      }
    });
    Providers.bind(Long.class, (ctx) -> {
      try {
        return Long.parseLong(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.int", ctx.getArgument());
      }
    });
    Providers.bind(Double.TYPE, (ctx) -> {
      try {
        return Double.parseDouble(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.number", ctx.getArgument());
      }
    });
    Providers.bind(Double.class, (ctx) -> {
      try {
        return Double.parseDouble(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.number", ctx.getArgument());
      }
    });
    Providers.bind(Float.TYPE, (ctx) -> {
      try {
        return Float.parseFloat(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.number", ctx.getArgument());
      }
    });
    Providers.bind(Float.class, (ctx) -> {
      try {
        return Float.parseFloat(ctx.getArgument());
      } catch (NumberFormatException e) {
        throw new CommandArgumentException("core.command.argument.error.number", ctx.getArgument());
      }
    });
    Providers.bind(String.class, Context::getArgument);
    Providers.bind(UUID.class, (ctx) -> {
      try {
        return UUID.fromString(ctx.getArgument());
      } catch (IllegalArgumentException e) {
        throw new CommandArgumentException("core.command.argument.error.uuid", ctx.getArgument());
      }
    });
  }

  private static UUID isUUID(String inputString) {
    try {
      return UUID.fromString(inputString);
    } catch (IllegalArgumentException ignored) {
      return null;
    }
  }

  public static void registerBukkitTypes() {
    Providers.bind(Player.class, new Provider<>() {
      @Override
      public Player provide(Context ctx) throws CommandException {
        String searchTerm = ctx.getArgument();
        CommandSender sender = ctx.getSender().getAsCommandSender();
        Player player;
        UUID uuid = isUUID(searchTerm);
        if (uuid != null) {
          player = Bukkit.getPlayer(uuid);
        } else {
          player = Bukkit.getPlayerExact(searchTerm);
          if (player == null) {
            List<Player> matches = matchPlayer(searchTerm)
                .stream()
                .filter(otherPlayer -> canSee(sender, otherPlayer)).toList();
            if (matches.size() > 1) {
              String res = matches.stream().map(Player::getName).sorted()
                  .collect(Collectors.joining(", "));
              throw new CommandArgumentException("core.command.argument.error.player.multiple",
                  searchTerm, res, matches.size());
            }
            if (!matches.isEmpty()) {
              player = matches.get(0);
            }
          }
        }
        if (player == null || !canSee(sender, player)) {
          throw new CommandArgumentException("core.command.argument.error.player.notfound",
              searchTerm);
        }
        return player;
      }

      @Override
      public List<String> suggest(Context ctx) {
        List<String> players = new ArrayList<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
          if (canSee(ctx.getSender().getAsCommandSender(), player)) {
            players.add(player.getName());
          }
        }
        players.sort(Comparator.comparing(String::toLowerCase));
        return players;
      }
    });
    Providers.bind(World.class, new Provider<>() {
      @Override
      public World provide(Context ctx) throws CommandException {
        World world = Bukkit.getWorld(ctx.getArgument());
        if (world == null) {
          throw new CommandArgumentException("core.command.argument.error.world",
              ctx.getArgument());
        }
        return world;
      }

      @Override
      public List<String> suggest(Context ctx) {
        return Bukkit.getWorlds().stream().map(World::getName)
            .sorted(Comparator.comparing(String::toLowerCase)).collect(Collectors.toList());
      }
    });
  }


  public static void register() {
    registerNativeTypes();
    registerBukkitTypes();
    registerCustomTypes();
  }

  public static void registerCustomTypes() {
    Providers.bind(RecvMode.class, "staff", new Provider<>() {
      @Override
      public RecvMode provide(Context context) throws CommandException {
        if (context.getArgument().equalsIgnoreCase("next")) {
          if (!context.getSender().isConsole()) {
            if (context.getSender().getAsCorePlayer() == null) {
              throw new CommandArgumentException(true, "whoops");
            }
            return context.getSender().getAsCorePlayer().getStaffMessagesMode().next();
          }
        }
        try {
          return RecvMode.valueOf(context.getArgument().toUpperCase());
        } catch (IllegalArgumentException e) {
          String accepted =
              Arrays.stream(RecvMode.values())
                  .map(l -> "§f" + l.name().toLowerCase())
                  .collect(Collectors.joining("§c, "));
          throw new CommandArgumentException(true,
              "§cMode must be one of: " + accepted + "§c, §fnext§c.");
        }
      }

      @Override
      public List<String> suggest(Context context) {
        return List.of(RecvMode.tabComplete());
      }
    });
    Providers.bind(RecvMode.class, "filter", new Provider<>() {
      @Override
      public RecvMode provide(Context context) throws CommandException {
        if (context.getArgument().equalsIgnoreCase("next")) {
          if (!context.getSender().isConsole()) {
            if (context.getSender().getAsCorePlayer() == null) {
              throw new CommandArgumentException(true, "whoops");
            }
            return context.getSender().getAsCorePlayer().getFilterMessagesMode().next();
          }
        }
        try {
          return RecvMode.valueOf(context.getArgument().toUpperCase());
        } catch (IllegalArgumentException e) {
          String accepted =
              Arrays.stream(RecvMode.values())
                  .map(l -> "§f" + l.name().toLowerCase())
                  .collect(Collectors.joining("§c, "));
          throw new CommandArgumentException(true,
              "§cMode must be one of: " + accepted + "§c, §fnext§c.");
        }
      }

      @Override
      public List<String> suggest(Context context) {
        return List.of(RecvMode.tabComplete());
      }
    });
    Providers.bind(ICorePlayer.class, new Provider<>() {
      @Override
      public ICorePlayer provide(Context context) throws CommandException {
        Player player = Providers.provider(Player.class).provide(context);
        ICorePlayer corePlayer = VenadeAPI.getPlayer(player.getUniqueId());
        if (corePlayer == null) {
          throw new IllegalStateException("Player is not registered with Venade API");
        }
        return corePlayer;
      }

      @Override
      public List<String> suggest(Context context) {
        return Providers.provider(Player.class).suggest(context);
      }
    });

    Providers.bind(ICubidPlayer.class, new Provider<>() {
      @Override
      public ICubidPlayer provide(Context context) throws CommandException {
        if (!context.getCommand().isAsync()) {
          throw new RuntimeException("Command must be async for this provider to work!");
        }
        ICubidPlayer result = null;
        try {
          Player player = Providers.provider(Player.class).provide(context);
          result = CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(player.getUniqueId())
              .orElse(null);
        } catch (CommandException ignored) {

        }
        if (result != null) {
          return result;
        }
        UUID uuid = uuidFromName(context.getArgument());
        if (uuid == null) {
          throw new CommandArgumentException("core.command.argument.error.player.notfound.network",
              context.getArgument());
        }
        result = CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(uuid)
            .orElse(null);
        if (result == null || result.getConnectedService() == null) {
          throw new CommandArgumentException("core.command.argument.error.player.notfound.network",
              context.getArgument());
        }
        return result;
      }

      @Override
      public List<String> suggest(Context context) {
        return Providers.provider(Player.class).suggest(context);
      }
    });
    Providers.bind(IOfflineCubidPlayer.class, "doNotFetch", new Provider<>() {
      @Override
      public IOfflineCubidPlayer provide(Context context) throws CommandException {
        if (!context.getCommand().isAsync()) {
          throw new RuntimeException("Command must be async for this provider to work!");
        }
        IOfflineCubidPlayer result = null;
        try {
          Player player = Providers.provider(Player.class).provide(context);
          result = CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(player.getUniqueId())
              .orElse(null);
        } catch (CommandException ignored) {

        }
        if (result != null) {
          return result;
        }
        UUID uuid = isUUID(context.getArgument());
        if (uuid != null) {
          result = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(uuid).orElse(null);
        } else {
          result = getByNameCaseInsensitive(context.getArgument()).orElse(null);
        }
        if (result == null || ((CubidPlayer) result).getLastLogin() == 0) {
          throw new CommandArgumentException(
              "core.command.argument.error.player.nonexist.network",
              context.getArgument());
        }
        return result;
      }

      @Override
      public List<String> suggest(Context context) {
        return Providers.provider(Player.class).suggest(context);
      }
    });
    Providers.bind(IOfflineCubidPlayer.class, new Provider<>() {
      @Override
      public IOfflineCubidPlayer provide(Context context) throws CommandException {
        if (!context.getCommand().isAsync()) {
          throw new RuntimeException("Command must be async for this provider to work!");
        }
        IOfflineCubidPlayer result = null;
        try {
          Player player = Providers.provider(Player.class).provide(context);
          result = CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(player.getUniqueId())
              .orElse(null);
        } catch (CommandException ignored) {

        }
        if (result != null) {
          return result;
        }
        UUID uuid = isUUID(context.getArgument());
        if (uuid != null) {
          result = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(uuid).orElse(null);
          if (result == null) {
            context.getSender()
                .sendMessage("core.command.argument.player.fetching", context.getArgument());
            String name = UUIDFetcher.getName(uuid);
            if (name == null) {
              throw new CommandArgumentException("core.command.argument.error.player.nonexist",
                  context.getArgument());
            }
            result = CubidCloud.getBridge().getPlayerManager()
                .createPlayer(name, uuid, "DEFAULT_IP");
            ((CubidPlayer) result).setLastLogin(0);
            CubidCloud.getBridge().getPlayerManager().update(result);

          }
        } else {
          result = getByNameCaseInsensitive(context.getArgument()).orElse(null);
          if (result == null) {
            context.getSender()
                .sendMessage("core.command.argument.player.fetching", context.getArgument());
            uuid = UUIDFetcher.getUUID(context.getArgument());
            if (uuid == null) {
              throw new CommandArgumentException("core.command.argument.error.player.nonexist",
                  context.getArgument());
            }
            String name = UUIDFetcher.getName(uuid);
            // see if they exist by UUID before creating them
            result = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(uuid).orElse(null);
            if (result != null) {
              // they've joined before but changed their name
              if (!result.getName().equalsIgnoreCase(name)) {
                ((CubidPlayer) result).setName(name);
                CubidCloud.getBridge().getPlayerManager().update(result);
              }
            } else {
              result = CubidCloud.getBridge().getPlayerManager()
                  .createPlayer(name, uuid, "DEFAULT_IP");
              ((CubidPlayer) result).setLastLogin(0);
              CubidCloud.getBridge().getPlayerManager().update(result);

            }
          } else {
            uuid = result.getUniqueId();
          }
          // Various calls here end up saving things into redis
          // This will cause them to get stuck in redis as they never log out, causing them to be "phantom online"
          // We will manually remove them again to solve this problem, unless they genuinely are online
          UUID finalUuid = uuid;
          CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(uuid).ifPresentOrElse((p) -> {
            if (p.getConnectedProxy() == null || p.getConnectedProxy().isEmpty()) {
              CubidCloud.getBridge().getRedisManager().async(2)
                  .hdel("CUBID_PLAYERS", finalUuid.toString());
            }
          }, () -> CubidCloud.getBridge().getRedisManager().async(2)
              .hdel("CUBID_PLAYERS", finalUuid.toString()));

        }
        return result;
      }

      @Override
      public List<String> suggest(Context context) {
        return Providers.provider(Player.class).suggest(context);
      }
    });

    Providers.bind(Report.class, (context) -> {
      if (!context.getCommand().isAsync()) {
        throw new RuntimeException("Command must be async for this provider to work!");
      }
      if (!ObjectId.isValid(context.getArgument())) {
        throw new CommandArgumentException("core.command.argument.error.objectid",
            context.getArgument());
      }
      ObjectId id = new ObjectId(context.getArgument());
      Report report = VenadeAPI.getReportService().getReportById(id);
      if (report == null) {
        throw new CommandArgumentException("core.command.argument.error.report", id.toString());
      }
      return report;

    });

  }

  private static UUID uuidFromName(String name) {
    Document doc = CubidCloud.getBridge().getMongoManager()
        .getCollection("players")
        .find(Filters.eq("name", name))
        .collation(
            Collation.builder().locale("en").collationStrength(CollationStrength.SECONDARY).build()
        )
        .limit(1)
        .projection(Projections.include("uuid"))
        .first();
    if (doc == null) {
      return null;
    }
    return UUID.fromString(doc.getString("uuid"));
  }

  private static Optional<ICubidPlayer> getByNameCaseInsensitive(String name) {
    Document playerDocument = CubidCloud.getBridge().getMongoManager()
        .getCollection("players")
        .find(Filters.eq("name", name))
        .collation(
            Collation.builder().locale("en").collationStrength(CollationStrength.SECONDARY).build())
        .limit(1)
        .first();
    if (playerDocument == null) {
      return Optional.empty();
    }
    return Optional.of(CubidCloud.getBridge().getMongoManager()
        .toObject(playerDocument, CubidPlayer.class));

  }

  // Stole from bukkit but adapter to use startsWith instead of contains
  public static List<Player> matchPlayer(String partialName) {
    List<Player> matchedPlayers = new ArrayList<>();
    for (Player iterPlayer : Bukkit.getOnlinePlayers()) {
      String iterPlayerName = iterPlayer.getName();
      if (partialName.equalsIgnoreCase(iterPlayerName)) {
        matchedPlayers.clear();
        matchedPlayers.add(iterPlayer);
        break;
      }

      if (iterPlayerName.toLowerCase(Locale.ENGLISH)
          .startsWith(partialName.toLowerCase(Locale.ENGLISH))) {
        matchedPlayers.add(iterPlayer);
      }
    }

    return matchedPlayers;
  }

  public static boolean canSee(CommandSender sender, Player target) {
    if (sender instanceof ConsoleCommandSender || sender instanceof BlockCommandSender) {
      return true;
    }
    if (sender instanceof Player) {
      return ((Player) sender).canSee(target);
    }
    return false;
  }
}
