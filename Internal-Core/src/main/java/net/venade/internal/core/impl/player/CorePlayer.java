package net.venade.internal.core.impl.player;

import cloud.cubid.bridge.CubidCloud;
import com.google.gson.JsonObject;
import cubid.cloud.modules.player.CubidPlayer;
import cubid.cloud.modules.player.ICubidPlayer;
import java.util.Optional;
import java.util.UUID;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.PlayerUnvanishEvent;
import net.venade.internal.api.player.PlayerVanishEvent;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.bukkit.BukkitCore;
import net.venade.internal.core.labymod.LabyModProtocol;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 25.02.2021, 16:17 Copyright (c) 2021
 */
public class CorePlayer implements ICorePlayer {

  private final UUID uniqueId;
  private final boolean subscriber;
  private int glowstoneDust, amethysts;
  private int globalLevel, globalXp;
  private long playTime, onlineTime;
  private boolean vanished = false;
  private RecvMode staffMessages = RecvMode.ON, filterMessages = RecvMode.ON;

  public CorePlayer(UUID uniqueId) {
    this.uniqueId = uniqueId;
    this.amethysts = 150;
    this.glowstoneDust = 75;
    this.globalLevel = 0;
    this.globalXp = 0;
    this.playTime = 0;
    this.onlineTime = 0;
    this.subscriber = false;
  }

  @Override
  public void sendMessage(String path, Object... replacements) {
    getPlayer().sendMessage(this.getLanguageString(path, replacements));
  }

  @Override
  public Player getPlayer() {
    return Bukkit.getPlayer(uniqueId);
  }

  @Override
  public ICubidPlayer getCubidPlayer() {
    Optional<CubidPlayer> optional =
        CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(uniqueId);
    return optional.orElse(null);
  }

  @Override
  public String getLanguageString(String path) {
    if (getCubidPlayer() == null) {
      return "Loading...";
    }
    return HexColor.translate(getCubidPlayer().getLanguageString(path));
  }

  @Override
  public String getLanguageString(String path, Object... replacements) {
    if (getCubidPlayer() == null) {
      return "Loading...";
    }
    return HexColor.translate(getCubidPlayer().getLanguageString(path, replacements));
  }

  @Override
  public String[] getLanguageArray(String path, Object... replacements) {
    return this.getLanguageString(path, replacements).split("%n%");
  }

  @Override
  public String getOnlineTimeFormatted() {
    long millis =
        (System.currentTimeMillis() - VenadeCore.getInstance().getPlayTime().get(uniqueId))
            + getOnlineTime();
    long minutes = (millis / (1000 * 60)) % 60;
    long hours = ((millis / (1000 * 60 * 60)));
    return String.format("%02d:%02d", hours, minutes);
  }

  @Override
  public void sendServerBanner() {
    JsonObject object = new JsonObject();
    object.addProperty(
        "url",
        "https://cdn.discordapp.com/attachments/527845905991467018/882013917134589952/Labymod_Tablist.png");
    LabyModProtocol.sendLabyModMessage(getPlayer(), "server_banner", object);
  }

  @Override
  public UUID getUniqueId() {
    return this.uniqueId;
  }

  @Override
  public int getAmethysts() {
    return this.amethysts;
  }

  @Override
  public int getGlowstoneDust() {
    return this.glowstoneDust;
  }

  @Override
  public void addAmethysts(int number) {
    this.amethysts += number;
  }

  @Override
  public void removeAmethysts(int number) {
    this.amethysts -= number;
  }

  @Override
  public void addGlowstoneDust(int number) {
    this.glowstoneDust += number;
  }

  @Override
  public void removeGlowstoneDust(int number) {
    this.glowstoneDust -= number;
  }

  @Override
  public int getGlobalLevel() {
    return this.globalLevel;
  }

  @Override
  public int getGlobalXp() {
    return this.globalXp;
  }

  @Override
  public long getOnlineTime() {
    return this.onlineTime;
  }

  @Override
  public long getPlayTime() {
    return this.playTime;
  }

  @Override
  public boolean isSubscriber() {
    return subscriber;
  }

  @Override
  public void incrementGlobalLevel() {
    this.globalLevel = this.globalLevel + 1;
  }

  @Override
  public void addGlobalXp(int number) {
    this.globalXp = this.globalXp + number;
  }

  @Override
  public void addPlayTime(long playTime) {
    this.playTime = this.playTime + playTime;
  }

  @Override
  public void addOnlineTime(long onlineTime) {
    this.onlineTime = this.onlineTime + onlineTime;
  }

  @Override
  public RecvMode getFilterMessagesMode() {
    return this.filterMessages == null ? RecvMode.ON : this.filterMessages;
  }

  @Override
  public void setFilterMessagesMode(RecvMode mode) {
    this.filterMessages = mode;
  }

  @Override
  public RecvMode getStaffMessagesMode() {
    return this.staffMessages == null ? RecvMode.ON : this.staffMessages;
  }

  @Override
  public void setStaffMessagesMode(RecvMode mode) {
    this.staffMessages = mode;
  }

  @Override
  public boolean isVanished() {
    return this.vanished;
  }

  @Override
  public void setVanished(boolean vanished) {
    setVanishedInternal(vanished);
    Bukkit.getScheduler().runTaskAsynchronously(BukkitCore.getInstance(), () -> {
      if (this.vanished) {
        Bukkit.getPluginManager().callEvent(new PlayerVanishEvent(this));
      } else {
        Bukkit.getPluginManager().callEvent(new PlayerUnvanishEvent(this));
      }
    });

  }

  @Override
  public void setVanishedActionBar(boolean shouldDisplay) {
    if (shouldDisplay) {
      VanishActionBarTask.IGNORED.remove(this.getUniqueId());
    } else {
      VanishActionBarTask.IGNORED.add(this.getUniqueId());
    }
  }


  public boolean canSeeVanished() {
    return this.getCubidPlayer().getRank().isStaff();
  }

  public void setVanishedInternal(boolean vanished) {
    this.vanished = vanished;
    Player player = this.getPlayer();
    if (this.vanished) {
      player.setMetadata("vanished", new FixedMetadataValue(BukkitCore.getInstance(), true));
      player.setMetadata("vanishedMessage", new FixedMetadataValue(BukkitCore.getInstance(),
          this.getCubidPlayer().getLanguageString("core.vanished.actionbar")));
      for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
        if (otherPlayer.hasMetadata("canSeeVanished")) {
          continue;
        }
        otherPlayer.hidePlayer(BukkitCore.getInstance(), player);
      }
      player.setSleepingIgnored(true);
      player.setCollidable(false);
    } else {
      player.removeMetadata("vanished", BukkitCore.getInstance());
      player.removeMetadata("vanishedMessage", BukkitCore.getInstance());
      for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
        if (!otherPlayer.canSee(player)) {
          otherPlayer.showPlayer(BukkitCore.getInstance(), player);
        }
      }
      player.setSleepingIgnored(false);
      player.setCollidable(true);
    }
    CubidCloud.getBridge().updatePlayerNameTag(this.uniqueId);
  }
}
