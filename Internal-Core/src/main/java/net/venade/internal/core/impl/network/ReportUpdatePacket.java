package net.venade.internal.core.impl.network;

import cloud.cubid.networking.message.NetworkingMessage;

/**
 * @author JakeMT04
 * @since 29/10/2021
 */
public class ReportUpdatePacket extends NetworkingMessage {
  public ReportUpdatePacket() {
    super(9002);
  }

}
