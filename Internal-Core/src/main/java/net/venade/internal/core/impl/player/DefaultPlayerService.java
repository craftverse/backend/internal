package net.venade.internal.core.impl.player;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.database.redis.IRedisManager;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import com.google.common.collect.ImmutableList;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import cubid.cloud.modules.player.ICubidPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import lombok.Getter;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.IPlayerService;
import net.venade.internal.api.player.PlayerAssociation;
import net.venade.internal.api.player.PlayerIPEntry;
import net.venade.internal.api.punish.service.IPunishmentService;
import net.venade.internal.api.punish.service.IPunishmentTemplateService;
import net.venade.internal.api.punish.service.IReportService;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import net.venade.internal.core.impl.punish.DefaultPunishmentTemplateService;
import net.venade.internal.core.impl.punish.DefaultReportService;
import org.bukkit.Bukkit;

/**
 * @author Nikolas Rummel
 * @since 14.08.2021
 */
@Getter
public class DefaultPlayerService implements IPlayerService {

  private static final int REDIS_DB = 0;
  private final IMongoManager mongoManager;
  private final IRedisManager redisManager;
  private final IPunishmentTemplateService punishmentTemplateService;
  private final IReportService reportService;
  private final IPunishmentService punishmentService;

  public DefaultPlayerService(IMongoManager mongoManager, IRedisManager redisManager) {
    this.mongoManager = mongoManager;
    this.redisManager = redisManager;
    this.punishmentService = new DefaultPunishmentService(this.mongoManager);
    this.punishmentTemplateService = new DefaultPunishmentTemplateService(this.mongoManager,
        this.redisManager);
    this.reportService = new DefaultReportService(this.mongoManager, this.redisManager);
  }

  public CorePlayer handleJoin(UUID uuid) {
    return this.createPlayer(uuid);
  }

  public void handleQuit(CorePlayer player) {
    this.savePlayer(player);
  }

  @Override
  public Optional<ICorePlayer> getOnlinePlayer(UUID uuid) {
    try {
      String json = this.redisManager.async(REDIS_DB).hget("VENADE_PLAYERS", uuid.toString()).get();
      if (json == null) {
        return Optional.empty();
      }

      return Optional.of(this.redisManager.toObject(json, CorePlayer.class));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return Optional.empty();
  }

  public void deletePlayerInRedis(UUID uuid) {
    this.redisManager.async(REDIS_DB).hdel("VENADE_PLAYERS", uuid.toString());
  }


  @Override
  public void updateInRedis(ICorePlayer corePlayer) {
    CorePlayer player = (CorePlayer) corePlayer;
    this.redisManager
        .async(REDIS_DB)
        .hset(
            "VENADE_PLAYERS",
            player.getUniqueId().toString(),
            this.redisManager.fromObject(player));
  }

  public CorePlayer createPlayer(UUID uuid) {
    CorePlayer player = new CorePlayer(uuid);

    if (this.mongoManager
        .getDatabase()
        .getCollection("venade_players")
        .countDocuments(Filters.eq("uniqueId", uuid.toString()))
        == 0) {
      this.savePlayer(player);
    } else {
      player = this.getPlayerFromMongo(uuid);
    }

    this.updateInRedis(player);
    return player;
  }

  public void savePlayer(ICorePlayer iCorePlayer) {
    CorePlayer player = (CorePlayer) iCorePlayer;
    this.updateInRedis(player);

    if (this.mongoManager
        .getCollection("venade_players")
        .countDocuments(Filters.eq("uniqueId", player.getUniqueId().toString()))
        == 0) {
      this.mongoManager.insertObject("venade_players", player);
    } else {
      this.mongoManager.replaceObject(
          "venade_players", Filters.eq("uniqueId", player.getUniqueId().toString()), player);
    }
  }

  public CorePlayer getPlayerFromMongo(UUID uuid) {
    return this.mongoManager.getObject(
        "venade_players", Filters.eq("uniqueId", uuid.toString()), CorePlayer.class);
  }

  @Override
  public void updateIPHistory(UUID uuid, String ip) {
    // see if they've used the IP before
    this.mongoManager.runAsync(() -> {
      PlayerIPEntry previous = this.mongoManager.getObject("venade_ip_history",
          Filters.and(Filters.eq("uuid", uuid.toString()), Filters.eq("ip", ip)),
          PlayerIPEntry.class);
      long now = System.currentTimeMillis();
      boolean exists = previous != null;
      if (!exists) {
        previous = new PlayerIPEntry(uuid, ip, now, 0, 0);
      } else {
        if (Math.abs(previous.getLastSeen() - System.currentTimeMillis()) < 15_000L) {
          // They were already updated
          Bukkit.getLogger().info(
              "[VenadeCore] Player IP Address was already updated in the last ~15 seconds. Avoiding second update.");
          return;
        }
      }
      previous.setLastSeen(now);
      previous.setLoginCount(previous.getLoginCount() + 1);
      if (exists) {
        this.mongoManager.replaceObject("venade_ip_history",
            Filters.and(Filters.eq("uuid", uuid.toString()), Filters.eq("ip", ip)),
            previous);
      } else {
        this.mongoManager.insertObject("venade_ip_history", previous);
      }
    });
  }


  @Override
  public List<PlayerAssociation> getMatchingPlayers(UUID uuid) {
    if (Bukkit.getServer().isPrimaryThread()) {
      throw new IllegalStateException(
          "Cannot fetch IP matches on the main thread. Do you realise how expensive this is?");
    }
    List<String> ipHistory = this.mongoManager.getCollection("venade_ip_history")
        .find(Filters.eq("uuid", uuid.toString()))
        .sort(Sorts.descending("lastSeen"))
        .projection(Projections.include("ip"))
        .map(d -> d.getString("ip"))
        .into(new ArrayList<>());
    LinkedHashMap<UUID, List<PlayerIPEntry>> associations = new LinkedHashMap<>();
    associations.put(uuid, new ArrayList<>());
    List<UUID> playersToProcess = new ArrayList<>();
    for (String ip : ipHistory) {
      // Directly process the players IP history
      List<PlayerIPEntry> playersThatHaveUsed = getPlayersThatHaveUsedIp(uuid, ip);
      if (!playersThatHaveUsed.isEmpty()) {
        associations.get(uuid).addAll(playersThatHaveUsed);
        playersThatHaveUsed.forEach(e -> playersToProcess.add(e.getUuid()));
      }
    }
    playersToProcess.remove(uuid);
    List<UUID> playersProcessed = new ArrayList<>();
    List<String> ipsProcessed = new ArrayList<>();
    playersProcessed.add(uuid);
    while (!playersToProcess.isEmpty()) {
      UUID item = playersToProcess.iterator().next();
      playersToProcess.remove(item);
      if (playersProcessed.contains(item)) {
        continue;
      }
      playersProcessed.add(item);
      for (String ip : getIPHistory(item)) {
        if (ipsProcessed.contains(ip)) {
          continue;
        }
        ipsProcessed.add(ip);
        List<PlayerIPEntry> players = getPlayersThatHaveUsedIp(item, ip);
        players.forEach(e -> {
          if (e.getUuid().equals(item)) {
            return;
          }
          if (!playersProcessed.contains(e.getUuid())) {
            playersToProcess.add(e.getUuid());
          }
          associations.computeIfAbsent(item, z -> new ArrayList<>()).add(e);
        });
      }
    }
    List<PlayerAssociation> associationList = new ArrayList<>();
    for (Map.Entry<UUID, List<PlayerIPEntry>> list : associations.entrySet()) {
      LinkedHashMap<UUID, PlayerAssociation> f = new LinkedHashMap<>();
      for (PlayerIPEntry entry : list.getValue()) {
        if (entry.getUuid().equals(uuid)) {
          continue;
        }
        PlayerAssociation association = f.computeIfAbsent(entry.getUuid(),
            (u) -> new PlayerAssociation(list.getKey(), u, new ArrayList<>()));
        if (!association.getMatching().contains(entry) && ipHistory.contains(entry.getIp())) {
          association.getMatching().add(entry);
        }
      }
      associationList.addAll(f.values());
    }
    return ImmutableList.copyOf(associationList);
  }

  public List<PlayerIPEntry> getPlayersThatHaveUsedIp(UUID uuid, String ip) {
    return this.mongoManager.getCollection("venade_ip_history")
        .find(Filters.and(Filters.ne("uuid", uuid), Filters.eq("ip", ip)))
        .sort(Sorts.descending("lastSeen"))
        .map(d -> this.mongoManager.toObject(d, PlayerIPEntry.class))
        .into(new ArrayList<>());
  }

  @Override
  public List<String> getIPHistory(UUID uuid) {
    return this.mongoManager.getCollection("venade_ip_history")
        .find(Filters.eq("uuid", uuid.toString()))
        .sort(Sorts.descending("lastSeen"))
        .projection(Projections.include("ip"))
        .map(d -> d.getString("ip"))
        .into(new ArrayList<>());
  }

  @Override
  public void sendStaffChatMessage(UUID source, String message) {
    String sourceName;
    if (source.equals(Sender.CONSOLE_UUID)) {
      sourceName = Sender.CONSOLE_NAME;
    } else {
      ICubidPlayer player = CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(source)
          .orElse(null);
      if (player == null) {
        sourceName = Optional.ofNullable(Bukkit.getPlayer(source)).orElseThrow().getName();
      } else {
        sourceName = player.getRank().getColor() + player.getName();
      }
    }
    Map<Language, JsonDocument> keys = new HashMap<>();
    // Generate messages for all languages
    for (Language language : Language.values()) {
      String formattedMessage = HexColor.translate(CubidLanguage.getLanguageAPI()
          .getLanguageString("core.staffchat.format", language,
              CubidCloud.getBridge().getLocalService().getServiceId().getName(),
              sourceName, message));
      keys.put(language, new JsonDocument().append("message", formattedMessage));
    }

    StaffMessagePacket packet = new StaffMessagePacket(keys, "chat",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), false);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

}
