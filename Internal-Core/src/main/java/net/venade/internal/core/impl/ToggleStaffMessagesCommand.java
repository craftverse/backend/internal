package net.venade.internal.core.impl;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.CustomProvider;
import net.venade.internal.api.command.annotation.Default;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.ICorePlayer.RecvMode;

/**
 * @author JakeMT04
 * @since 23/10/2021
 */
public class ToggleStaffMessagesCommand extends VenadeCommand {


  public ToggleStaffMessagesCommand() {
    super(
        "togglestaff",
        "core.togglestaff.command.desc",
        Permission.STAFF,
        "tsm"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, @Default("next") @CustomProvider("staff") RecvMode mode) {
    ICorePlayer corePlayer = sender.getAsCorePlayer();
    corePlayer.setStaffMessagesMode(mode);
    VenadeAPI.getPlayerService().savePlayer(corePlayer);
    sender.sendMessage("core.togglestaff." + mode.keyName());
  }
}
