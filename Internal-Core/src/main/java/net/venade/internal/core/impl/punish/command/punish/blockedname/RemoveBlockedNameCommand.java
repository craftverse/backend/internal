package net.venade.internal.core.impl.punish.command.punish.blockedname;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.punish.BlockedName;

/**
 * @author JakeMT04
 * @since 23/12/2021
 */
public class RemoveBlockedNameCommand extends VenadeCommand {

  public RemoveBlockedNameCommand() {
    super(
        "remove",
        "core.blockedname.command.remove.desc",
        null
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, String name) {
    name = name.toUpperCase();
    if (!VenadeAPI.getPunishmentService().isBlockedName(name)) {
      sender.sendMessage("core.blockedname.not", name);
      return;
    }
    BlockedName blockedName = VenadeAPI.getPunishmentService().getBlockedName(name);
    VenadeAPI.getPunishmentService().removeBlockedName(name);
    VenadeAPI.getPunishmentService().broadcastUnblockedName(blockedName, sender.getUniqueId());
//    sender.sendMessage("core.blockedname.removed", name);
  }

}
