package net.venade.internal.core.impl.punish;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.TypeAdapters;
import com.mongodb.BasicDBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.CollationStrength;
import com.mongodb.client.model.CountOptions;
import com.mongodb.client.model.DeleteOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Sorts;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.punish.BlockedName;
import net.venade.internal.api.punish.service.IPunishmentService;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.RemovablePunishment;
import net.venade.internal.api.punish.type.TemporaryPunishment;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import org.bson.Document;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public class DefaultPunishmentService implements IPunishmentService {

  public static final Permission COMMAND_PERMISSION = new Permission() {
    @Override
    public boolean hasPermission(Sender sender) {
      return sender.hasRankOrHigher(CubidRank.SR_CONT) || sender.hasRank(CubidRank.MOD)
          || sender.hasRank(CubidRank.COMM);
    }
  };
  private static final Gson GSON = (new GsonBuilder()).registerTypeAdapter(UUID.class,
          TypeAdapters.UUID)
      .registerTypeAdapter(Punishment.class, new Punishment.PunishmentDeserializer()).create();
  private final IMongoManager mongoManager;

  public DefaultPunishmentService(IMongoManager mongoManager) {
    this.mongoManager = mongoManager;
    this.mongoManager.getCollection("venade_punishments")
        .createIndex(new Document("punishmentId", 1).append("type", 1),
            new IndexOptions().unique(true));
  }

  @Override
  public void applyPunishment(Punishment punishment) {
    if (punishment.getType().equalsIgnoreCase("ban") || punishment.getType()
        .equalsIgnoreCase("kick")) {
      // Try and kick the player from the proxy
      this.mongoManager.runAsync(
          () -> CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(punishment.getTarget())
              .ifPresent((target) -> target.kick(punishment.getApplyScreen(target.getLanguage()))));
    } else if (punishment.getType().equalsIgnoreCase("mute")) {
      this.mongoManager.runAsync(
          () -> CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(punishment.getTarget())
              .ifPresent(
                  (target) -> target.sendMessage(punishment.getApplyScreen(target.getLanguage()))));
    }
  }

  @Override
  public void savePunishment(Punishment punishment, boolean async) {
    if (async) {
      this.mongoManager.runAsync(() -> this.doPunishmentSave(punishment));
    } else {
      this.doPunishmentSave(punishment);
    }
  }

  private void doPunishmentSave(Punishment punishment) {
    try {
      if (punishment.get_id() == null) {
        Document document = this.mongoManager.toDocument(punishment);
        while (true) {
          try {
            this.mongoManager.getCollection("venade_punishments").insertOne(document);
            break;
          } catch (DuplicateKeyException e) {
            punishment.regeneratePunishmentId();
            document = this.mongoManager.toDocument(punishment);
          }
        }
        punishment.setId(document.getObjectId("_id"));
      } else {
        Document doc = this.mongoManager.toDocument(punishment);
        doc.remove("_id");
        this.mongoManager.getCollection("venade_punishments")
            .replaceOne(Filters.eq("_id", punishment.get_id()), doc);

      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  // TODO: Add a cache maybe?
  @Override
  public List<Punishment> getPunishments(UUID uuid) {
    return this.mongoManager.getCollection("venade_punishments")
        .find(Filters.eq("target", uuid.toString()))
        .map((document) -> GSON.fromJson(GSON.toJson(document), Punishment.class))
        .into(new ArrayList<>());
  }

  @Override
  public <T extends Punishment> List<T> getPunishments(Class<T> type, UUID uuid, boolean reversed) {
    return this.mongoManager.getCollection("venade_punishments")
        .find(Filters.and(Filters.eq("target", uuid.toString()),
            Filters.eq("type", type.getSimpleName().toLowerCase())))
        .sort(
            reversed ? Sorts.descending("issued") : Sorts.ascending("issued")
        )
        .map((document) -> GSON.fromJson(GSON.toJson(document), type))
        .into(new ArrayList<>());
  }

  @Override
  public <T extends Punishment> T getActivePunishment(Class<T> type, UUID uuid) {
    T punishment = this.mongoManager.getCollection("venade_punishments")
        .find(Filters.and(Filters.eq("target", uuid.toString()),
            Filters.eq("type", type.getSimpleName().toLowerCase()),
            Filters.eq("active", true)))
        .limit(1)
        .map((document) -> GSON.fromJson(GSON.toJson(document), type))
        .first();
    if (punishment == null) {
      return null;
    }
    if (!punishment.isActive()) {
      return null;
    }
    return punishment;
  }

  @Override
  public <T extends Punishment> T getPunishmentById(String id, Class<T> clazz) {
    return this.mongoManager.getCollection("venade_punishments")
        .find(Filters.and(Filters.eq("punishmentId", id), Filters.eq("type", clazz.getSimpleName().toLowerCase())))
        .limit(1)
        .map((document) -> GSON.fromJson(GSON.toJson(document), clazz))
        .first();
  }

  @Override
  public boolean isBlockedName(String name) {
    return this.mongoManager.getCollection("venade_blocked_names")
        .countDocuments(
            Filters.eq("name", name),
            new CountOptions()
                .limit(1)
                .collation(
                    Collation.builder()
                        .locale("en")
                        .collationStrength(CollationStrength.SECONDARY)
                        .build()
                )
        ) != 0;
  }

  @Override
  public void saveBlockedName(BlockedName blockedName) {
    if (blockedName.get_id() == null) {
      Document document = this.mongoManager.toDocument(blockedName);
      this.mongoManager.getCollection("venade_blocked_names").insertOne(document);
      blockedName.set_id(document.getObjectId("_id"));
    } else {
      this.mongoManager.replaceObject("venade_blocked_names",
          Filters.eq("_id", blockedName.get_id()), blockedName);
    }
  }

  @Override
  public void applyBlockedName(BlockedName blockedName) {
    this.mongoManager.runAsync(
        () -> CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(blockedName.getName())
            .flatMap(
                offlinePlayer -> CubidCloud.getBridge().getPlayerManager()
                    .getOnlinePlayer(offlinePlayer.getUniqueId()))
            .ifPresent(onlinePlayer -> onlinePlayer.kick(
                HexColor.translate(onlinePlayer.getLanguageString("core.blockedname.kick",
                    onlinePlayer.getName())))));
  }

  @Override
  public BlockedName getBlockedName(String name) {
    return this.mongoManager.getCollection("venade_blocked_names")
        .find(Filters.eq("name", name))
        .limit(1)
        .collation(
            Collation.builder().locale("en").collationStrength(CollationStrength.SECONDARY).build())
        .map(d -> this.mongoManager.toObject(d, BlockedName.class))
        .first();
  }

  @Override
  public CompletableFuture<List<BlockedName>> getBlockedNames() {
    return CompletableFuture.supplyAsync(
        () -> this.mongoManager.getCollection("venade_blocked_names")
            .find()
            .sort(Sorts.ascending("name"))
            .map(d -> this.mongoManager.toObject(d, BlockedName.class))
            .into(new ArrayList<>()));
  }

  @Override
  public void removeBlockedName(String name) {
    this.mongoManager.getCollection("venade_blocked_names")
        .deleteOne(
            Filters.eq("name", name),
            new DeleteOptions()
                .collation(
                    Collation.builder()
                        .locale("en")
                        .collationStrength(CollationStrength.SECONDARY)
                        .build()
                )
        );

  }

  @Override
  public void broadcastBlockedName(BlockedName blockedName) {
    Map<Language, JsonDocument> messages = blockedName.getBroadcastMessage();
    StaffMessagePacket packet = new StaffMessagePacket(messages, "punish",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), false);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

  @Override
  public void broadcastUnblockedName(BlockedName blockedName, UUID unblockedBy) {
    // fun
    // generate messages
    Map<Language, JsonDocument> messages = blockedName.getRemoveMessage(unblockedBy);
    StaffMessagePacket packet = new StaffMessagePacket(messages, "punish",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }


  @Override
  public int getPunishmentPoints(String type, UUID uuid) {
    return Optional.ofNullable(this.mongoManager.getCollection("venade_punishments").aggregate(
            Arrays.asList(Aggregates.match(Filters.and(Filters.eq("target", uuid.toString()),
                    Filters.eq("type", type.toLowerCase()), Filters.ne("points", 0))),
                Aggregates.group(null, Accumulators.sum("points", "$points")),
                Aggregates.project(new BasicDBObject().append("totalPoints", "$points")))).first())
        .map(d -> d.getDouble("totalPoints").intValue()).orElse(0);
  }

  @Override
  public void broadcastPunishment(Punishment punishment) {
    // fun
    // generate messages
    Map<Language, JsonDocument> messages = punishment.getBroadcastMessage();
    StaffMessagePacket packet = new StaffMessagePacket(messages, "punish",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

  @Override
  public void broadcastRemovedPunishment(RemovablePunishment punishment) {
    Map<Language, JsonDocument> messages = punishment.getRemoveMessage();
    StaffMessagePacket packet = new StaffMessagePacket(messages, "punish",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

  @Override
  public void broadcastReducedPunishment(Punishment punishment) {
    TemporaryPunishment tp = (TemporaryPunishment) punishment;
    Map<Language, JsonDocument> messages = tp.getReducedMessage();
    StaffMessagePacket packet = new StaffMessagePacket(messages, "punish",
        CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
    CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
  }

}
