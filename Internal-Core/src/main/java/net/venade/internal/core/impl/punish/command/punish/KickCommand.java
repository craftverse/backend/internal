package net.venade.internal.core.impl.punish.command.punish;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.ICubidPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.punish.type.Kick;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;

/**
 * @author JakeMT04
 * @since 24/10/2021
 */
public class KickCommand extends VenadeCommand {

  public KickCommand() {
    super(
        "kick",
        "core.kick.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        "k"
    );
    this.setAsync(true);
  }

  @CommandMethod
  public void onCommand(Sender sender, ICubidPlayer target, @ConsumeRest String reason) {
    if (!sender.isConsole() && target.getRank().isStaff()) {
      sender.sendMessage("core.punish.error.staff",
          target.getRank().getColor() + target.getName());
      return;
    }
    String server;
    String localServiceName = CubidCloud.getBridge().getLocalService().getServiceId().getName();
    if (localServiceName.equalsIgnoreCase(target.getConnectedService())) {
      server = localServiceName;
    } else {
      sender.sendMessage("core.punish.kick.fromother",
          target.getRank().getColor() + target.getName(), target.getConnectedService());
      server = localServiceName + " -> " + target.getConnectedService();
    }
    Kick kick = Kick.builder()
        .executor(sender.getUniqueId())
        .target(target.getUniqueId())
        .issued(System.currentTimeMillis())
        .reason(reason)
        .server(server)
        .build();
    VenadeAPI.getPunishmentService().savePunishment(kick);
    VenadeAPI.getPunishmentService().applyPunishment(kick);
    VenadeAPI.getPunishmentService().broadcastPunishment(kick);
  }

}
