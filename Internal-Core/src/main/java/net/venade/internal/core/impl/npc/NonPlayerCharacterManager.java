package net.venade.internal.core.impl.npc;

import java.util.List;
import java.util.Optional;
import net.venade.internal.api.entities.EntityClickType;
import net.venade.internal.api.entities.event.EntityCharacterClickEvent;
import net.venade.internal.api.entities.event.IEntityClickEvent;
import net.venade.internal.api.entities.hologram.Hologram;
import net.venade.internal.api.entities.npc.AbstractNonPlayerCharacterManager;
import net.venade.internal.api.entities.npc.INonPlayerCharacter;
import net.venade.internal.api.entities.npc.NonPlayerCharacterSkin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 21:47 Copyright (c) 2021
 */
public class NonPlayerCharacterManager extends AbstractNonPlayerCharacterManager {

  @Override
  public void sendPackets(Player player) {
    this.NON_PLAYER_CHARACTERS.forEach(nonPlayerCharacter -> nonPlayerCharacter.sendPacket(player));
  }

  @Override
  public void sendPackets() {
    Bukkit.getOnlinePlayers().forEach(player -> sendPackets());
  }

  @Override
  public void add(
      String id,
      String displayName,
      Location location,
      NonPlayerCharacterSkin skin,
      boolean lookAtPlayer,
      Hologram hologram,
      List<IEntityClickEvent> events) {
    boolean present =
        this.NON_PLAYER_CHARACTERS.stream()
            .anyMatch(iNonPlayerCharacter -> iNonPlayerCharacter.getId() == id);
    if (present) {
      return;
    }
    NonPlayerCharacter character =
        new NonPlayerCharacter(id, displayName, location, skin, lookAtPlayer, hologram, events);
    this.NON_PLAYER_CHARACTERS.add(character);
  }

  @Override
  public INonPlayerCharacter getPlayer(String id) {
    return this.NON_PLAYER_CHARACTERS.stream()
        .filter(iNonPlayerCharacter -> iNonPlayerCharacter.getId() == id)
        .findFirst()
        .get();
  }

  public Optional<INonPlayerCharacter> getPlayerByEntityId(int entityId) {
    return this.NON_PLAYER_CHARACTERS.stream()
        .filter(iNonPlayerCharacter -> iNonPlayerCharacter.getEntityPlayer().getId() == entityId)
        .findFirst();
  }

  @Override
  public void handleLeftClick(final Player player, int entityId) {
    Optional<INonPlayerCharacter> result = getPlayerByEntityId(entityId);
    if (result.isEmpty()) {
      return;
    }

    EntityCharacterClickEvent clickEvent =
        new EntityCharacterClickEvent(player, EntityClickType.LEFT, result.get());
    result.get().getListOfEvents().forEach(event -> event.onLeftClicked(clickEvent));
  }

  @Override
  public void handleRightClick(final Player player, int entityId) {
    Optional<INonPlayerCharacter> result = getPlayerByEntityId(entityId);
    if (result.isEmpty()) {
      return;
    }
    player.swingMainHand();
    EntityCharacterClickEvent clickEvent =
        new EntityCharacterClickEvent(player, EntityClickType.RIGHT, result.get());
    result.get().getListOfEvents().forEach(event -> event.onRightClicked(clickEvent));
  }
}
