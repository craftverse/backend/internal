package net.venade.internal.core.impl.report;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.networking.channel.CubidChannel;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.report.Report;
import net.venade.internal.core.impl.network.ReportTeleportPacket;
import net.venade.internal.core.impl.punish.DefaultReportService;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 28/10/2021
 */
public class AcceptReportCommand extends VenadeCommand {

  private final JavaPlugin plugin;

  public AcceptReportCommand(JavaPlugin plugin) {
    super(
        "acceptreport",
        "core.report.acceptreport.desc",
        DefaultReportService.COMMAND_PERMISSION
    );
    this.setAsync(true);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender, Report report) {
    if (sender.isConsole()) {
      return;
    }
    if (VenadeAPI.getReportService().isAlreadyAssignedToReport(sender.getUniqueId())) {
      sender.sendMessage("core.report.assigned.multiple");
      return;
    }
    if (report.isClosed()) {
      sender.sendMessage("core.report.closed.already");
      return;
    }
    if (report.getAssignee() != null) {
      sender.sendMessage("core.report.assigned.already");
      return;
    }
    report.setAssignee(sender.getUniqueId());
    VenadeAPI.getReportService().saveReport(report);
    IOfflineCubidPlayer offlinePlayer = CubidCloud.getBridge().getPlayerManager()
        .getOfflinePlayer(report.getReported()).orElseThrow();
    sender.sendMessage("core.report.assigned",
        offlinePlayer.getRank().getColor() + offlinePlayer.getName());
    // see if the player is online
    ICubidPlayer cubidTarget = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(report.getReported()).orElse(null);
    if (cubidTarget == null) {
      sender.sendMessage("core.report.notonline",
          offlinePlayer.getRank().getColor() + offlinePlayer.getName(), report.getServer());
      return;
    }
    sender.sendMessage("core.report.assigned.teleporting",
        cubidTarget.getRank().getColor() + cubidTarget.getName(),
        cubidTarget.getConnectedService());
    ICorePlayer corePlayer = sender.getAsCorePlayer();
    if (!corePlayer.isVanished()) {
      corePlayer.setVanished(true);
      VenadeAPI.getPlayerService().savePlayer(corePlayer);
    }
    if (cubidTarget.getConnectedService()
        .equalsIgnoreCase(CubidCloud.getBridge().getLocalService().getServiceId().getName())) {
      Player player = Bukkit.getPlayer(report.getReported());
      if (player == null) {
        sender.sendMessage("core.report.assigned.error",
            cubidTarget.getRank().getColor() + cubidTarget.getName());
        return;
      }
      Bukkit.getScheduler().runTask(this.plugin, () -> sender.getAsPlayer().teleport(player));
    } else {
      // send packet
      ReportTeleportPacket packet = new ReportTeleportPacket(sender.getUniqueId(),
          cubidTarget.getUniqueId(), cubidTarget.getConnectedService());
      CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
      sender.getAsCorePlayer().getCubidPlayer().connect(cubidTarget.getConnectedService());
    }
  }

}
