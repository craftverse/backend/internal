package net.venade.internal.core.impl.gui.history;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Kick;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.RemovablePunishment;
import net.venade.internal.core.bukkit.BukkitCore;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class HistoryGUIMainMenu implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final IOfflineCubidPlayer targetPlayer;
  private final SmartInventory inventory;
  private final int currentBanPoints;
  private final int currentMutePoints;

  public HistoryGUIMainMenu(ICorePlayer corePlayer, IOfflineCubidPlayer targetPlayer,
      int currentBanPoints, int currentMutePoints) {
    this.corePlayer = corePlayer;
    this.targetPlayer = targetPlayer;
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("history")
        .title(HexColor.translate(
            "History for " + targetPlayer.getRank().getColor() + targetPlayer.getName()))
        .closeable(true)
        .size(1, 9)
        .build();
    this.currentBanPoints = currentBanPoints;
    this.currentMutePoints = currentMutePoints;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    ItemStack banItem = ItemBuilder.builder(Material.CHEST)
        .display("§bBan")
        .lore("§fCurrent points: §b" + currentBanPoints)
        .amount(Math.min(Math.max(1, currentBanPoints), 64))
        .build();

    ClickableItem banItemClickable = ClickableItem.of(banItem, (event) -> {
      LoadingMenu menu = new LoadingMenu(corePlayer);
      menu.openInventory();
      Bukkit.getScheduler().runTaskAsynchronously(JavaPlugin.getPlugin(BukkitCore.class), () -> {
        List<Ban> bans = VenadeAPI.getPunishmentService()
            .getPunishments(Ban.class, targetPlayer.getUniqueId(), true);
        Map<UUID, String> names = getPlayerNames(bans);
        HistoryGUI gui = new HistoryGUI(corePlayer, targetPlayer, bans, names);
        Bukkit.getScheduler()
            .runTask(JavaPlugin.getPlugin(BukkitCore.class), gui::openInventory);
      });
    });
    ItemStack kickItem = ItemBuilder.builder(Material.ARROW)
        .display("§bKick")
        .amount(1)
        .build();

    ClickableItem kickItemClickable = ClickableItem.of(kickItem, (event) -> {
      List<Kick> bans = VenadeAPI.getPunishmentService()
          .getPunishments(Kick.class, targetPlayer.getUniqueId(), true);
      Map<UUID, String> names = getPlayerNames(bans);
      HistoryGUI gui = new HistoryGUI(corePlayer, targetPlayer, bans, names);
      Bukkit.getScheduler()
          .runTask(JavaPlugin.getPlugin(BukkitCore.class), gui::openInventory);
    });
    ItemStack muteItem = ItemBuilder.builder(Material.OAK_SIGN)
        .display("§bMute")
        .lore("§fCurrent points: §b" + currentMutePoints)
        .amount(Math.min(Math.max(1, currentMutePoints), 64))
        .build();

    ClickableItem muteItemClickable = ClickableItem.of(muteItem, (event) -> {
      List<Mute> bans = VenadeAPI.getPunishmentService()
          .getPunishments(Mute.class, targetPlayer.getUniqueId(), true);
      Map<UUID, String> names = getPlayerNames(bans);
      HistoryGUI gui = new HistoryGUI(corePlayer, targetPlayer, bans, names);
      Bukkit.getScheduler()
          .runTask(JavaPlugin.getPlugin(BukkitCore.class), gui::openInventory);
    });

    contents.set(0, 2, banItemClickable);
    contents.set(0, 4, kickItemClickable);
    contents.set(0, 6, muteItemClickable);
  }

  public Map<UUID, String> getPlayerNames(List<? extends Punishment> list) {
    if (list.isEmpty()) {
      return Collections.emptyMap();
    }
    HashMap<UUID, String> playerNames = new HashMap<>();
    playerNames.put(Sender.CONSOLE_UUID, HexColor.translate(Sender.CONSOLE_NAME));
    for (Punishment p : list) {
      // this is history, so only need to load the executor
      if (p instanceof RemovablePunishment) {
        if (((RemovablePunishment) p).wasRemoved()) {
          UUID removedBy = ((RemovablePunishment) p).getRemovedBy();
          if (!playerNames.containsKey(removedBy)) {
            IOfflineCubidPlayer player = CubidCloud.getBridge().getPlayerManager()
                .getOnlinePlayer(removedBy).orElse(null);
            if (player == null) {
              player = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(removedBy)
                  .orElse(null);
            }
            if (player == null) {
              playerNames.put(removedBy, "§7§ounknown :(");
            } else {
              playerNames.put(removedBy,
                  HexColor.translate(player.getRank().getColor() + player.getName()));
            }
          }
        }
      }
      UUID executor = p.getExecutor();
      if (playerNames.containsKey(executor)) {
        continue;
      }
      // see if player is online
      IOfflineCubidPlayer player = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(executor).orElse(null);
      if (player == null) {
        player = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(executor).orElse(null);
      }
      if (player == null) {
        playerNames.put(executor, "§7§ounknown :(");
      } else {
        playerNames.put(executor,
            HexColor.translate(player.getRank().getColor() + player.getName()));
      }
    }
    return playerNames;
  }


  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
