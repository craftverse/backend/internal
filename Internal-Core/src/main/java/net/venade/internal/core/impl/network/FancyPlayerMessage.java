package net.venade.internal.core.impl.network;

import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.networking.message.NetworkingMessage;
import java.util.UUID;
import lombok.Getter;

/**
 * @author JakeMT04
 * @since 16/10/2021
 */
@Getter
public class FancyPlayerMessage extends NetworkingMessage {

  private final UUID target;
  private final JsonDocument document;

  public FancyPlayerMessage(UUID target, JsonDocument document) {
    super(9001);
    this.target = target;
    this.document = document;
  }
}
