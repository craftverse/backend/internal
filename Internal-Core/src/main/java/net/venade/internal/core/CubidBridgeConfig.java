package net.venade.internal.core;

import cloud.cubid.common.database.mongo.MongoConfiguration;
import cloud.cubid.common.database.redis.RedisConfiguration;
import cloud.cubid.module.service.service.CloudService;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CubidBridgeConfig {

    private String version;
    private RedisConfiguration redisConfiguration;
    private MongoConfiguration mongoConfiguration;
    private CloudService cloudService;
}