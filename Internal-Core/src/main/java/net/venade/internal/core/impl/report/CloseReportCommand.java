package net.venade.internal.core.impl.report;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.networking.channel.CubidChannel;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.report.Report;
import net.venade.internal.core.impl.network.ReportUpdatePacket;
import net.venade.internal.core.impl.punish.DefaultReportService;

/**
 * @author JakeMT04
 * @since 29/10/2021
 */
public class CloseReportCommand extends VenadeCommand {

  public CloseReportCommand() {
    super(
        "closereport",
        "core.report.closereport.desc",
        DefaultReportService.COMMAND_PERMISSION,
        "cr"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, @ConsumeRest String reason) {
    Report report = VenadeAPI.getReportService().getAssignedReport(sender.getUniqueId());
    if (report == null) {
      sender.sendMessage("core.report.closereport.notassigned");
      return;
    }
    report.setClosed(true);
    report.setClosedReason(reason);
    report.setClosedAt(System.currentTimeMillis());
    VenadeAPI.getReportService().saveReport(report);
    VenadeAPI.getReportService().sendClosedMessages(report);
    CubidCloud.getBridge().getCubidNetwork().getPublisher()
        .publish(CubidChannel.SERVER, new ReportUpdatePacket());
    sender.sendMessage("core.report.closereport.closed", reason);
  }

}
