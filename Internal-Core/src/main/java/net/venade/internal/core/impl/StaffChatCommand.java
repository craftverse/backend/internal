package net.venade.internal.core.impl;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.player.ICorePlayer.RecvMode;

/**
 * @author JakeMT04
 * @since 23/10/2021
 */
public class StaffChatCommand extends VenadeCommand {

  public StaffChatCommand() {
    super(
        "staffchat",
        "core.staffchat.command.desc",
        Permission.STAFF,
        "sc"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, @ConsumeRest String message) {
    if (!sender.isConsole()) {
      if (sender.getAsCorePlayer().getStaffMessagesMode() == RecvMode.OFF) {
        sender.sendMessage("core.staffchat.off");
        return;
      }
    }
    VenadeAPI.getPlayerService().sendStaffChatMessage(sender.getUniqueId(), message);
  }
}
