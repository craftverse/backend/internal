package net.venade.internal.core.bukkit.listener;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.networking.channel.CubidChannel;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.ICorePlayer.RecvMode;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.impl.network.CoreMessagesListener;
import net.venade.internal.core.impl.network.ReportTeleportPacket;
import net.venade.internal.core.impl.npc.packet.PacketReader;
import net.venade.internal.core.impl.player.CorePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
public class PlayerJoinQuitListener implements Listener {

  private final VenadeCore core;
  private final PacketReader packetReader;
  private final JavaPlugin plugin;

  public PlayerJoinQuitListener(VenadeCore core, JavaPlugin plugin) {
    this.core = core;
    this.packetReader = new PacketReader();
    this.plugin = plugin;
    plugin.getServer().getPluginManager().registerEvents(this, plugin);
  }

  public static void doReportTeleport(Player player, ReportTeleportPacket packet) {
    // teleport now
    // make sure player is vanished
    ICorePlayer corePlayer = VenadeAPI.getPlayer(player.getUniqueId());
    if (corePlayer == null) {
      return;
    }
    if (!corePlayer.isVanished()) {
      corePlayer.setVanished(true);
    }
    CoreMessagesListener.TELEPORT_CACHE.invalidate(packet.getSourcePlayer());
    // check target player is still online
    Player target = Bukkit.getPlayer(packet.getTargetPlayer());
    if (target == null) {
      // they are not online, see if we can move the player to them
      ICubidPlayer onlinePlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(packet.getTargetPlayer()).orElse(null);
      if (onlinePlayer == null) {
        corePlayer.sendMessage("core.report.loggedout"); // they're gone gone
      } else {
        if (onlinePlayer.getConnectedService() != null) {
          // teleport them there
          ReportTeleportPacket newPacket = new ReportTeleportPacket(packet,
              onlinePlayer.getConnectedService());
          CubidCloud.getBridge().getCubidNetwork().getPublisher()
              .publish(CubidChannel.SERVER, newPacket);
          corePlayer.getCubidPlayer().connect(onlinePlayer.getConnectedService());
        } else {
          corePlayer.sendMessage("core.report.loggedout"); // they're gone gone
        }
      }
    } else {
      player.teleport(target);
    }
  }

  public static void doReportTeleport(Player player) {
    // check pending requests
    ReportTeleportPacket packet = CoreMessagesListener.TELEPORT_CACHE.getIfPresent(
        player.getUniqueId());
    if (packet == null) {
      return;
    }
    doReportTeleport(player, packet);

  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerJoin(PlayerJoinEvent event) {
    CorePlayer corePlayer = core.getPlayerService().handleJoin(event.getPlayer().getUniqueId());
    core.getPlayerService().updateInRedis(corePlayer);

    this.core.getPlayTime().put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
    this.packetReader.inject(event.getPlayer());
    corePlayer.setVanishedInternal(corePlayer.isVanished());
    if (corePlayer.canSeeVanished()) {
      event.getPlayer().setMetadata("canSeeVanished", new FixedMetadataValue(plugin, true));
    } else {
      event.getPlayer().removeMetadata("canSeeVanished", plugin);
      for (Player player : Bukkit.getOnlinePlayers()) {
        if (player.hasMetadata("vanished")) {
          event.getPlayer().hidePlayer(plugin, player);
        }
      }
    }
    corePlayer.sendServerBanner();
    this.plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
      if (event.getPlayer().isOnline()) {
        ICubidPlayer cubidPlayer = corePlayer.getCubidPlayer();
        if (cubidPlayer.hasRankOrHigher(CubidRank.CONT) || cubidPlayer.hasRankOrHigher(
            CubidRank.MOD)) {
          ICorePlayer newCorePlayer = VenadeAPI.getPlayer(corePlayer.getUniqueId());
          if (newCorePlayer.getStaffMessagesMode() != RecvMode.ON) {
            newCorePlayer.sendMessage(
                "core.togglestaff.still." + newCorePlayer.getStaffMessagesMode().keyName());
          }
        }
      }
    }, 2 * 20L);
    doReportTeleport(event.getPlayer());
  }

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {
    CorePlayer player = (CorePlayer) VenadeAPI.getPlayer(event.getPlayer().getUniqueId());
    if (player == null) {
      this.plugin.getLogger().warning("Player left but does not exist in redis");
      player = this.core.getPlayerService().getPlayerFromMongo(event.getPlayer().getUniqueId());
    }
    player.setVanishedActionBar(true);
    player.addOnlineTime(
        System.currentTimeMillis() - this.core.getPlayTime().get(player.getUniqueId()));
    this.core.getPlayTime().remove(player.getUniqueId());
    this.packetReader.uninject(event.getPlayer());
    VenadeCore.getInstance().getPlayerService().handleQuit(player);
  }


  @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
  public void onChat(AsyncPlayerChatEvent event) {
    if (event.getMessage().equalsIgnoreCase("particle")) {
      VenadeAPI.getParticleManager()
          .spawnCircleParticle(Particle.SPELL_MOB, event.getPlayer().getLocation());
    }
  }

}
