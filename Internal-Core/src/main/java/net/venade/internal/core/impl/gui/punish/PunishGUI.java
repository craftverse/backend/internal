package net.venade.internal.core.impl.gui.punish;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.ArrayList;
import java.util.List;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.api.date.Dates;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.content.SlotIterator;
import net.venade.internal.api.inventory.content.SlotIterator.Type;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.punish.template.PunishmentTemplate;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.core.bukkit.BukkitCore;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class PunishGUI implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final SmartInventory inventory;
  private final IOfflineCubidPlayer targetPlayer;
  private final String type;
  private final int currentUserPoints;

  public PunishGUI(ICorePlayer corePlayer, IOfflineCubidPlayer targetPlayer, int currentUserPoints,
      String type) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("punish-" + type)
        .title(corePlayer.getLanguageString("core.punish.gui.title",
            targetPlayer.getRank().getColor() + targetPlayer.getName()))
        .closeable(true)
        .size(4, 9)
        .build();
    this.targetPlayer = targetPlayer;
    this.type = type;
    this.currentUserPoints = currentUserPoints;
    this.corePlayer = corePlayer;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    List<ClickableItem> items = new ArrayList<>();
    for (PunishmentTemplate template : VenadeAPI.getPunishmentTemplateService()
        .getPunishmentTemplates(this.type)) {
      Material mat = Material.RED_WOOL;
      if (this.type.equals("ban")) {
        mat = Material.CHEST;
      } else if (this.type.equals("mute")) {
        mat = Material.OAK_SIGN;
      }
      int newPoints = template.getPointsToIssue() + this.currentUserPoints;
      long days = template.totalDays(this.currentUserPoints);
      ItemStack item = ItemBuilder.builder(mat)
          .amount(1)
          .unbreakable()
          .display("§b§l" + template.getName())
          .lore(
              corePlayer.getLanguageString("core.punish.gui.reason", template.getReason()),
              corePlayer.getLanguageString("core.punish.gui.points", template.getPointsToIssue(),
                  newPoints),
              corePlayer.getLanguageString(
                  "core.punish.gui.duration." + (days == 0 ? "permanent" : "temp"), days),
              "",
              corePlayer.getLanguageString("core.punish.gui.confirm")
          )
          .build();
      items.add(ClickableItem.of(item, (e) -> {
        e.setCancelled(true);
        this.process(player, template);
      }));

    }
    contents.pagination()
        .setItemsPerPage(this.inventory.getColumns() * (this.inventory.getRows() - 1));
    contents.pagination().setItems(items);
    fillPage(0, contents);
  }

  public void fillPage(int page, InventoryContents contents) {
    contents.clear();
    SlotIterator iterator = contents.newIterator(Type.HORIZONTAL, 0, 0);
    contents.pagination().page(page);
    contents.pagination().addToIterator(iterator);
    contents.fillRow(3, ClickableItem.empty(
        ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
    if (!contents.pagination().isFirst()) {
      ItemStack backButton = ItemBuilder.builder(Material.MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.prev"))
          .amount(1)
          .build();
      contents.set(3, 0, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() - 1, contents)));
    }
    if (!contents.pagination().isLast()) {
      ItemStack backButton = ItemBuilder.builder(Material.GLISTERING_MELON_SLICE)
          .display(corePlayer.getLanguageString("core.gui.page.next"))
          .amount(1)
          .build();
      contents.set(3, 8, ClickableItem.of(backButton,
          (e) -> fillPage(contents.pagination().getPage() + 1, contents)));
    }
    if (corePlayer.getCubidPlayer().hasRankOrHigher(CubidRank.SR_MOD)) {
      ItemStack newItem = ItemBuilder.builder(Material.PAPER)
          .display(corePlayer.getLanguageString("core.punish.gui.custom"))
          .build();

      contents.set(3, 4, ClickableItem.of(newItem, (e) -> this.doCustom()));
    }
  }

  public void doCustom() {
    this.closeInventory();
    new GetChatMessage(corePlayer.getPlayer(),
        corePlayer.getLanguageString("core.punish.duration"), (durationString) -> {
      if (durationString.equalsIgnoreCase("cancel")) {
        this.openInventory();
        return;
      }
      new GetChatMessage(corePlayer.getPlayer(),
          corePlayer.getLanguageString("core.punish.points"), (gcm, pointsString) -> {
        if (pointsString.equalsIgnoreCase("cancel")) {
          this.openInventory();
          return;
        }
        int points;
        try {
          points = Integer.parseInt(pointsString);
        } catch (NumberFormatException e) {
          corePlayer.sendMessage("core.command.argument.error.int", pointsString);
          corePlayer.sendMessage("core.punish.points");
          gcm.reissue();
          return;
        }
        new GetChatMessage(corePlayer.getPlayer(),
            corePlayer.getLanguageString("core.punish.reason"), (reason) -> {
          if (reason.equalsIgnoreCase("cancel")) {
            this.openInventory();
            return;
          }
          new GetChatMessage(corePlayer.getPlayer(),
              corePlayer.getLanguageString("core.punish.internal-notes"),
              (notes) -> {
                if (notes.equalsIgnoreCase("cancel")) {
                  this.openInventory();
                  return;
                }
                Bukkit.getScheduler()
                    .runTaskAsynchronously(JavaPlugin.getPlugin(BukkitCore.class), () -> {
                      long now = System.currentTimeMillis();
                      long expiry = Dates.parseDateDiff(durationString, true);
                      finalProcess(now, expiry, points, reason, notes);
                    });
              });
        });
      });
    });
  }

  public void finalProcess(long issued, long expiry, int points, String reason, String notes) {
    Punishment punishment;
    if (this.type.equals("ban")) {
      punishment = Ban.builder()
          .executor(corePlayer.getUniqueId())
          .target(targetPlayer.getUniqueId())
          .points(points)
          .issued(issued)
          .expiry(expiry)
          .reason(reason)
          .internalNotes(notes)
          .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
          .build();
    } else if (this.type.equals("mute")) {
      punishment = Mute.builder()
          .executor(corePlayer.getUniqueId())
          .target(targetPlayer.getUniqueId())
          .points(points)
          .issued(issued)
          .expiry(expiry)
          .reason(reason)
          .internalNotes(notes)
          .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
          .build();
    } else {
      return;
    }
    VenadeAPI.getPunishmentService().savePunishment(punishment);
    VenadeAPI.getPunishmentService().applyPunishment(punishment);
    VenadeAPI.getPunishmentService().broadcastPunishment(punishment);
    VenadeAPI.getReportService().closeApplicableReports(punishment);
  }

  public void process(Player player, PunishmentTemplate template) {
    // would do a thing
    this.closeInventory();
    new GetChatMessage(player, corePlayer.getLanguageString("core.punish.internal-notes"),
        (notes) -> {
          if (notes.equalsIgnoreCase("cancel")) {
            this.openInventory();
            return;
          }
          Bukkit.getScheduler()
              .runTaskAsynchronously(JavaPlugin.getPlugin(BukkitCore.class), () -> {
                long now = System.currentTimeMillis();
                long toAdd = template.totalDuration(this.currentUserPoints);
                long expiry;
                if (toAdd == 0) {
                  expiry = 0;
                } else {
                  expiry = now + template.totalDuration(this.currentUserPoints);
                }
                finalProcess(now, expiry, template.getPointsToIssue(), template.getReason(), notes);
              });
        });
  }

  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
