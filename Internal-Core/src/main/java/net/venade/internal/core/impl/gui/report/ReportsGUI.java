package net.venade.internal.core.impl.gui.report;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.date.Dates;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.content.SlotIterator;
import net.venade.internal.api.inventory.content.SlotIterator.Type;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.report.Report;
import net.venade.internal.core.VenadeCore;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 29/10/2021
 */
public class ReportsGUI {

  public static UUID STATUS_UUID = UUID.randomUUID();

  public static ClickableItem processReport(SmartInventory inventory, ICorePlayer corePlayer,
      Report report,
      Map<UUID, String> playerNameLookupTable, boolean includeTarget, boolean senderAssigned) {
    String reportedName = playerNameLookupTable.computeIfAbsent(report.getReported(), uuid -> {
      IOfflineCubidPlayer offlineCubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOfflinePlayer(uuid).orElse(null);
      if (offlineCubidPlayer == null) {
        return "§7§ounknown :(";
      }
      return offlineCubidPlayer.getRank().getColor() + offlineCubidPlayer.getName();
    });
    String reporterName = playerNameLookupTable.computeIfAbsent(report.getReporter(), uuid -> {
      IOfflineCubidPlayer offlineCubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOfflinePlayer(uuid).orElse(null);
      if (offlineCubidPlayer == null) {
        return "§7§ounknown :(";
      }
      return offlineCubidPlayer.getRank().getColor() + offlineCubidPlayer.getName();
    });
    List<String> lore = new ArrayList<>();
    lore.add("");
    if (includeTarget) {
      lore.add(corePlayer.getLanguageString("core.report.gui.reports.target", reportedName));
      lore.add("");
    }
    lore.add(corePlayer.getLanguageString("core.report.gui.reports.header.added"));
    lore.add(corePlayer.getLanguageString("core.history.gui.by", reporterName));
    lore.add(corePlayer.getLanguageString("core.history.gui.reason", report.getReason()));
    lore.add(corePlayer.getLanguageString("core.history.gui.server", report.getServer()));
    lore.add("");
    if (report.isClosed()) {
      lore.add(0, "§c" + Dates.FORMATTER.format(report.getClosedAt()));
      lore.add(corePlayer.getLanguageString("core.report.gui.reports.header.closed"));
      if (report.getAssignee() != null) {
        String assigneeName = playerNameLookupTable.computeIfAbsent(report.getAssignee(), uuid -> {
          IOfflineCubidPlayer offlineCubidPlayer = CubidCloud.getBridge().getPlayerManager()
              .getOfflinePlayer(uuid).orElse(null);
          if (offlineCubidPlayer == null) {
            return "§7§ounknown :(";
          }
          return offlineCubidPlayer.getRank().getColor() + offlineCubidPlayer.getName();
        });
        lore.add(corePlayer.getLanguageString("core.history.gui.by", assigneeName));
      }
      lore.add(corePlayer.getLanguageString("core.history.gui.reason", report.getClosedReason()));
    } else {
      if (report.getAssignee() == null) {
        lore.add(corePlayer.getLanguageString("core.report.gui.reports.assign"));
      } else {
        String assigneeName = playerNameLookupTable.computeIfAbsent(report.getAssignee(), uuid -> {
          IOfflineCubidPlayer offlineCubidPlayer = CubidCloud.getBridge().getPlayerManager()
              .getOfflinePlayer(uuid).orElse(null);
          if (offlineCubidPlayer == null) {
            return "§7§ounknown :(";
          }
          return offlineCubidPlayer.getRank().getColor() + offlineCubidPlayer.getName();
        });
        lore.add(corePlayer.getLanguageString("core.report.gui.reports.assigned", assigneeName));
      }
    }

    ItemStack item = ItemBuilder.builder(report.isClosed() ? Material.RED_WOOL
            : report.getAssignee() == null ? Material.LIME_WOOL : Material.GREEN_WOOL)
        .display("§a" + Dates.FORMATTER.format(report.getReportedAt()))
        .setLore(lore)
        .build();
    if (report.isClosed() || report.getAssignee() != null || senderAssigned) {
      return ClickableItem.empty(item);
    } else {
      return ClickableItem.of(item, (e) -> doClaim(inventory, corePlayer, report.get_id()));
    }
  }

  public static void doClaim(SmartInventory inventory, ICorePlayer sender, ObjectId report) {
    inventory.close(sender.getPlayer());
    sender.getPlayer().performCommand("acceptreport " + report.toString());
  }

  public static final class AllReportsGUI implements InventoryProvider {

    private final SmartInventory inventory;
    private final ICorePlayer player;
    private final HashMap<UUID, String> playerNameLookupTable = new HashMap<>();
    private final JavaPlugin plugin;
    private final List<Report> reports;
    private final boolean assigned;
    private UUID lastUuid;

    public AllReportsGUI(ICorePlayer player, List<Report> reports, boolean assigned,
        JavaPlugin plugin) {
      this.player = player;
      this.inventory = SmartInventory.builder()
          .manager(VenadeAPI.getInventoryManager())
          .provider(this)
          .id("all-reports")
          .title(player.getLanguageString("core.report.gui.all.title"))
          .closeable(true)
          .size(4, 9)
          .build();
      this.plugin = plugin;
      this.assigned = assigned;
      this.reports = reports;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
      List<ClickableItem> items = new ArrayList<>();
      playerNameLookupTable.put(Sender.CONSOLE_UUID, Sender.CONSOLE_NAME);
      for (Report report : reports) {
        items.add(processReport(this.inventory, this.player, report, playerNameLookupTable, true,
            assigned));
      }
      this.lastUuid = ReportsGUI.STATUS_UUID;
      contents.pagination()
          .setItemsPerPage(this.inventory.getColumns() * (this.inventory.getRows() - 1));
      contents.pagination().setItems(items);
      fillPage(0, contents);
      reports.clear(); // let the objects get garbage collected from here, they don't need to still be loaded
    }

    public void openInventory() {
      this.inventory.open(this.player.getPlayer());
    }

    @Override
    public void update(Player player, InventoryContents contents) {
      if (!this.lastUuid.equals(ReportsGUI.STATUS_UUID)) {
        this.lastUuid = ReportsGUI.STATUS_UUID;

        List<ClickableItem> items = new ArrayList<>();
        playerNameLookupTable.put(Sender.CONSOLE_UUID, Sender.CONSOLE_NAME);
        VenadeCore.getInstance().getMongoManager().runAsync(() -> {
          boolean assigned = VenadeAPI.getReportService()
              .isAlreadyAssignedToReport(player.getUniqueId());
          for (Report report : VenadeAPI.getReportService().getAllOpenReports(false)) {
            items.add(
                processReport(this.inventory, this.player, report, playerNameLookupTable, true,
                    assigned));
          }
          contents.pagination().setItems(items);
          Bukkit.getScheduler()
              .runTask(plugin, () -> fillPage(contents.pagination().getPage(), contents));

        });

      }
    }

    public void fillPage(int page, InventoryContents contents) {
      contents.clear();
      SlotIterator iterator = contents.newIterator(Type.HORIZONTAL, 0, 0);
      contents.pagination().page(page);
      contents.pagination().addToIterator(iterator);
      contents.fillRow(3, ClickableItem.empty(
          ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
      if (!contents.pagination().isFirst()) {
        ItemStack backButton = ItemBuilder.builder(Material.MELON_SLICE)
            .display(player.getLanguageString("core.gui.page.prev"))
            .amount(1)
            .build();
        contents.set(3, 0, ClickableItem.of(backButton,
            (e) -> fillPage(contents.pagination().getPage() - 1, contents)));
      }
      if (!contents.pagination().isLast()) {
        ItemStack backButton = ItemBuilder.builder(Material.GLISTERING_MELON_SLICE)
            .display(player.getLanguageString("core.gui.page.next"))
            .amount(1)
            .build();
        contents.set(3, 8, ClickableItem.of(backButton,
            (e) -> fillPage(contents.pagination().getPage() + 1, contents)));
      }
    }
  }

  public static final class PlayerReportsGUI implements InventoryProvider {

    private final SmartInventory inventory;
    private final ICorePlayer player;
    private final IOfflineCubidPlayer target;
//    private UUID lastUuid;

    public PlayerReportsGUI(ICorePlayer player, IOfflineCubidPlayer target) {
      this.player = player;
      this.inventory = SmartInventory.builder()
          .manager(VenadeAPI.getInventoryManager())
          .provider(this)
          .id("reports")
          .title(player.getLanguageString("core.report.gui.player.title",
              target.getRank().getColor() + target.getName()))
          .closeable(true)
          .size(4, 9)
          .build();
      this.target = target;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
      List<ClickableItem> items = new ArrayList<>();
      HashMap<UUID, String> playerNameLookupTable = new HashMap<>();
      playerNameLookupTable.put(Sender.CONSOLE_UUID, Sender.CONSOLE_NAME);
      boolean assigned = VenadeAPI.getReportService()
          .isAlreadyAssignedToReport(player.getUniqueId());
      for (Report report : VenadeAPI.getReportService()
          .getReportsForPlayer(target.getUniqueId(), false)) {
        items.add(processReport(this.inventory, this.player, report, playerNameLookupTable, false,
            assigned));
      }
      contents.pagination()
          .setItemsPerPage(this.inventory.getColumns() * (this.inventory.getRows() - 1));
      contents.pagination().setItems(items);
      fillPage(0, contents);
    }

    public void openInventory() {
      this.inventory.open(this.player.getPlayer());
    }

    @Override
    public void update(Player player, InventoryContents contents) {
    }

    public void fillPage(int page, InventoryContents contents) {
      contents.clear();
      SlotIterator iterator = contents.newIterator(Type.HORIZONTAL, 0, 0);
      contents.pagination().page(page);
      contents.pagination().addToIterator(iterator);
      contents.fillRow(3, ClickableItem.empty(
          ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
      if (!contents.pagination().isFirst()) {
        ItemStack backButton = ItemBuilder.builder(Material.MELON_SLICE)
            .display("§cPrevious page")
            .amount(1)
            .build();
        contents.set(3, 0, ClickableItem.of(backButton,
            (e) -> fillPage(contents.pagination().getPage() - 1, contents)));
      }
      if (!contents.pagination().isLast()) {
        ItemStack backButton = ItemBuilder.builder(Material.GLISTERING_MELON_SLICE)
            .display("§aNext page")
            .amount(1)
            .build();
        contents.set(3, 8, ClickableItem.of(backButton,
            (e) -> fillPage(contents.pagination().getPage() + 1, contents)));
      }
    }
  }
}
