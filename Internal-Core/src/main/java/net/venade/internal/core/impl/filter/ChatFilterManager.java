package net.venade.internal.core.impl.filter;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.channel.CubidChannel;
import com.mongodb.client.model.Projections;
import cubid.cloud.modules.player.ICubidPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.core.impl.network.StaffMessagePacket;
import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author JakeMT04
 * @since 20/11/2021
 */
@AllArgsConstructor
public class ChatFilterManager implements Listener {

  private final IMongoManager mongoManager;

  @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
  public void onChat(AsyncPlayerChatEvent event) {
    String strippedMessage = ChatColor.stripColor(event.getMessage());
    ChatFilterMatch match = this.shouldFilter(strippedMessage);
    if (match != null) {
      event.setCancelled(true);
      ICubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(event.getPlayer().getUniqueId()).orElse(null);
      if (cubidPlayer == null) {
        return;
      }
      Object[] messageParameters = new Object[]{
          CubidCloud.getBridge().getLocalService().getServiceId().getName(),
          cubidPlayer.getRank().getColor() + cubidPlayer.getName(),
          strippedMessage.replace(match.getMatch(), "§c§n" + match.getMatch() + "&#e8f5ff")
      };
      Map<Language, JsonDocument> messages = new HashMap<>();
      for (Language language : Language.values()) {
        TextComponent message = HexColor.translateToCompoment(CubidLanguage.getLanguageAPI()
            .getLanguageString("core.filter.broadcast", language, messageParameters));
        messages.put(language,
            new JsonDocument().append("message", ComponentSerializer.toString(message)));
      }
      StaffMessagePacket packet = new StaffMessagePacket(messages, "filter",
          CubidCloud.getBridge().getLocalService().getServiceId().getName(), true);
      CubidCloud.getBridge().getCubidNetwork().getPublisher().publish(CubidChannel.SERVER, packet);
      cubidPlayer.sendLanguageMessage("core.filter.playersend." + match.getType().name().toLowerCase());
    }
  }

  public ChatFilterMatch shouldFilter(String message) {
    List<Document> regexMatches = this.mongoManager.getCollection("venade_chat_filter")
        .find()
        .projection(Projections.include("match", "type"))
        .into(new ArrayList<>());
//        .map(doc -> new ChatFilterMatch(doc.getString("match"),
//            ChatFilterType.valueOf(doc.getString("type"))))
//        .into(new ArrayList<>());
    for (Document match : regexMatches) {
      Pattern pattern = Pattern.compile(match.getString("match"));
      Matcher matcher = pattern.matcher(message);
      if (matcher.find()) {
        return new ChatFilterMatch(matcher.group(),
            ChatFilterType.valueOf(match.getString("type")));
      }
    }
    return null;
  }

  @AllArgsConstructor
  @Getter
  public enum ChatFilterType {
    INAPPROPRIATE, TOXICITY, BOT
  }

  @AllArgsConstructor
  @Getter
  public static final class ChatFilterMatch {

    private final String match;
    private final ChatFilterType type;
  }


}
