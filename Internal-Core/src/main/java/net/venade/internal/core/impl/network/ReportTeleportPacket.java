package net.venade.internal.core.impl.network;

import cloud.cubid.networking.message.NetworkingMessage;
import java.util.UUID;
import lombok.Getter;

/**
 * @author JakeMT04
 * @since 23/11/2021
 */
@Getter
public class ReportTeleportPacket extends NetworkingMessage {

  private final UUID sourcePlayer;
  private final UUID targetPlayer;
  private final String targetServer;

  public ReportTeleportPacket(ReportTeleportPacket old, String targetServer) {
    this(old.sourcePlayer, old.targetPlayer, targetServer);
  }

  public ReportTeleportPacket(UUID sourcePlayer, UUID targetPlayer, String targetServer) {
    super(9004);
    this.sourcePlayer = sourcePlayer;
    this.targetPlayer = targetPlayer;
    this.targetServer = targetServer;

  }
}
