package net.venade.internal.core.impl.npc;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.minecraft.network.protocol.game.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.network.protocol.game.PacketPlayOutEntityHeadRotation;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.protocol.game.PacketPlayOutNamedEntitySpawn;
import net.minecraft.network.protocol.game.PacketPlayOutPlayerInfo;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardTeam;
import net.minecraft.network.syncher.DataWatcherObject;
import net.minecraft.network.syncher.DataWatcherRegistry;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.EntityPlayer;
import net.minecraft.server.level.WorldServer;
import net.minecraft.server.network.PlayerConnection;
import net.minecraft.util.MathHelper;
import net.minecraft.world.scores.Scoreboard;
import net.minecraft.world.scores.ScoreboardTeam;
import net.minecraft.world.scores.ScoreboardTeamBase;
import net.venade.internal.api.entities.event.IEntityClickEvent;
import net.venade.internal.api.entities.hologram.Hologram;
import net.venade.internal.api.entities.npc.INonPlayerCharacter;
import net.venade.internal.api.entities.npc.NonPlayerCharacterSkin;
import net.venade.internal.core.bukkit.BukkitCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_17_R1.CraftServer;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 17:52 Copyright (c) 2021
 */
public class NonPlayerCharacter implements INonPlayerCharacter {

  private final String id;
  private EntityPlayer entityPlayer;
  private final String displayName;
  private final Location location;
  private final NonPlayerCharacterSkin skin;
  private final List<IEntityClickEvent> events;
  private final boolean lookAtPlayer;
  private final Hologram hologram;
  private UUID uuid;
  private final ConcurrentHashMap<String, String> metadata;

  public NonPlayerCharacter(
      String id,
      String displayName,
      Location location,
      NonPlayerCharacterSkin skin,
      boolean lookAtPlayer,
      Hologram hologram,
      List<IEntityClickEvent> events) {
    this.displayName = displayName;
    this.location = location;
    this.id = id;
    this.skin = skin;
    this.events = events;
    this.lookAtPlayer = lookAtPlayer;
    this.hologram = hologram;
    this.metadata = new ConcurrentHashMap<>();
    initialize();
  }

  private void initialize() {
    Random random = new Random();
    this.uuid = new UUID(random.nextLong(), 0);

    MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();

    WorldServer world =
        ((CraftWorld) Bukkit.getWorld(this.location.getWorld().getName())).getHandle();
    GameProfile gameProfile = new GameProfile(this.uuid, this.displayName);

    this.entityPlayer =
        new EntityPlayer(server, world, gameProfile);// , new PlayerInteractManager(world));

    this.entityPlayer.setLocation(
        this.location.getX(),
        this.location.getY(),
        this.location.getZ(),
        this.location.getYaw(),
        this.location.getPitch());

    gameProfile
        .getProperties()
        .put("textures", new Property("textures", this.skin.getValue(), this.skin.getSignature()));
    entityPlayer
        .getDataWatcher()
        .set(new DataWatcherObject<>(17, DataWatcherRegistry.a), (byte) 127);
  }

  @Override
  public void sendPacket(final Player player) {
    PlayerConnection connection = ((CraftPlayer) player).getHandle().b;

    connection.sendPacket(
        new PacketPlayOutPlayerInfo(
            PacketPlayOutPlayerInfo.EnumPlayerInfoAction.a, this.entityPlayer));
    connection.sendPacket(new PacketPlayOutNamedEntitySpawn(this.entityPlayer));
    connection.sendPacket(
        new PacketPlayOutEntityHeadRotation(
            this.entityPlayer, (byte) (this.entityPlayer.getYRot() * 256 / 360)));
    connection.sendPacket(
        new PacketPlayOutEntityMetadata(entityPlayer.getId(), entityPlayer.getDataWatcher(), true));

    hologram.showPlayer(player);
    removeName(player);

    new BukkitRunnable() {
      @Override
      public void run() {
        connection.sendPacket(
            new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.e, entityPlayer));
      }
    }.runTaskLaterAsynchronously(JavaPlugin.getPlugin(BukkitCore.class), 5);

    if (lookAtPlayer) {
      new BukkitRunnable() {
        @Override
        public void run() {
          Vector difference =
              player
                  .getLocation()
                  .subtract(entityPlayer.getBukkitEntity().getLocation())
                  .toVector()
                  .normalize();
          float degrees =
              (float)
                  Math.toDegrees(Math.atan2(difference.getZ(), difference.getX()) - Math.PI / 2);
          byte angle = (byte) MathHelper.d((degrees * 256.0F) / 360.0F);

          connection.sendPacket(new PacketPlayOutEntityHeadRotation(entityPlayer, angle));
          connection.sendPacket(
              new PacketPlayOutEntityLook(
                  entityPlayer.getId(), angle, (byte) 0, true));
        }
      }.runTaskTimerAsynchronously(JavaPlugin.getPlugin(BukkitCore.class), 5, 5);
    }
  }

  private void removeName(Player player) {
    Scoreboard scoreboard = ((CraftPlayer) player).getHandle().getScoreboard();
    ScoreboardTeam scoreboardTeam =
        scoreboard.getTeam("npc") == null
            ? scoreboard.createTeam("npc")
            : scoreboard.getTeam("npc");
    scoreboardTeam.setNameTagVisibility(ScoreboardTeamBase.EnumNameTagVisibility.b);
    scoreboardTeam.getPlayerNameSet().add(entityPlayer.getName());

    PlayerConnection connection = ((CraftPlayer) player).getHandle().b;
    connection.sendPacket(PacketPlayOutScoreboardTeam.a(scoreboardTeam));
    connection.sendPacket(PacketPlayOutScoreboardTeam.a(scoreboardTeam, true));
  }

  @Override
  public String getDisplayName() {
    return displayName;
  }

  @Override
  public Location getLocation() {
    return location;
  }

  @Override
  public EntityPlayer getEntityPlayer() {
    return entityPlayer;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public NonPlayerCharacterSkin getSkin() {
    return skin;
  }

  @Override
  public Hologram getHologram() {
    return hologram;
  }

  @Override
  public UUID getUniqueId() {
    return this.uuid;
  }

  @Override
  public List<IEntityClickEvent> getListOfEvents() {
    return events;
  }

  @Override
  public ConcurrentHashMap<String, String> getMetadata() {
    return metadata;
  }
}
