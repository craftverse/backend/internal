package net.venade.internal.core.impl.filter.command;

import cloud.cubid.common.database.mongo.IMongoManager;
import com.mongodb.client.model.Filters;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.impl.filter.ChatFilterManager.ChatFilterType;
import org.bson.Document;

/**
 * @author JakeMT04
 * @since 23/11/2021
 */
public class AddEntryCommand extends VenadeCommand {

  public AddEntryCommand() {
    super(
        "addentry",
        "core.filter.command.addentry.desc",
        null
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, String identifier, ChatFilterType type,
      @ConsumeRest String match) {
    IMongoManager mongoManager = VenadeCore.getInstance().getMongoManager();
    boolean exists = mongoManager.getCollection("venade_chat_filter")
        .countDocuments(Filters.eq("identifier", identifier)) != 0;
    if (exists) {
      sender.sendMessage("core.filter.exists", identifier);
      return;
    }

    Document document = new Document().append("identifier", identifier).append("match", match)
        .append("type", type.name().toUpperCase());
    mongoManager.getCollection("venade_chat_filter").insertOne(document);
    sender.sendMessage("core.filter.added", identifier, match);
  }

}
