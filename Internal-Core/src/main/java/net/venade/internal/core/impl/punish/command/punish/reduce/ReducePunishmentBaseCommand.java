package net.venade.internal.core.impl.punish.command.punish.reduce;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.lang.ref.WeakReference;
import java.util.Collections;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.annotation.Name;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.date.Dates;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.TemporaryPunishment;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;

/**
 * @author JakeMT04
 * @since 21/10/2021
 */
public class ReducePunishmentBaseCommand extends VenadeCommand {

  private final Class<? extends Punishment> type;

  public ReducePunishmentBaseCommand(Class<? extends Punishment> type, String... aliases) {
    super("reduce" + type.getSimpleName().toLowerCase(),
        "core.reduce" + type.getSimpleName().toLowerCase() + ".desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        false, aliases);
    this.type = type;
    this.setAsync(true);
  }

  @CommandMethod
  public void onCommand(Sender sender,
      @Name("target | id") String targetStr,
      @Name("duration") String newDurationStr,
      @Name("points | c | /2") String newPointsStr,
      @ConsumeRest String reason) {
    WeakReference<Sender> weakSender = new WeakReference<>(sender);
    Punishment punishment;
    TemporaryPunishment tp;
    if (targetStr.startsWith("#")) {
      String id = targetStr.substring(1).toUpperCase();
      Punishment punish = VenadeAPI.getPunishmentService()
          .getPunishmentById(id, type);
      if (punish == null) {
        sender.sendMessage("core." + this.getName() + ".error.not.found", id);
        return;
      }
      if (!punish.isActive()) {
        sender.sendMessage("core." + this.getName() + ".error.not.active", id);
        return;
      }
      punishment = punish;
    } else {
      IOfflineCubidPlayer target;
      try {
        target = Providers.provider(IOfflineCubidPlayer.class)
            .provide(
                new Context(this, weakSender, Collections.emptyList(), targetStr));
      } catch (CommandException e) {
        sender.sendMessage(e.getMessage(sender));
        return;
      }

      punishment = VenadeAPI.getPunishmentService()
          .getActivePunishment(this.type, target.getUniqueId());
      if (punishment == null) {
        sender.sendMessage("core." + this.getName() + ".error.not.punished",
            target.getRank().getColor() + target.getName());
        return;
      }
    }
    int newPoints;
    if (newPointsStr.equalsIgnoreCase("c") || newPointsStr.equalsIgnoreCase("~")
        || newPointsStr.equalsIgnoreCase("-")) {
      newPoints = punishment.getPoints();
    } else if (newPointsStr.equalsIgnoreCase("/2")) {
      newPoints = punishment.getPoints() / 2;
    } else {
      try {
        newPoints = Providers.provider(Integer.class)
            .provide(new Context(this, weakSender,
                Collections.emptyList(), newPointsStr));
        if (newPoints < 0) {
          throw new CommandException("core.command.argument.error.int.negative", newPoints);
        }
      } catch (CommandException e) {
        sender.sendMessage(e.getMessage(sender));
        return;
      }
    }
    tp = (TemporaryPunishment) punishment;
    if (tp.wasReduced()) {
      sender.sendMessage("core." + this.getName() + ".error.already.reduced");
      return;
    }
    long now = System.currentTimeMillis();
    long expiry = Dates.parseDateDiff(newDurationStr, true);
    if (expiry == 0) {
      sender.sendMessage("core.punish.reduce.error.perm");
      return;
    }
    long diffNew = expiry - now;
    long diffOld = tp.getOriginalExpiry() - punishment.getIssued();
    if (!tp.isPermanent() && diffNew > diffOld) {
      sender.sendMessage("core.punish.reduce.error.longer");
      return;
    }

    tp.setReducedOn(now);
    tp.setExpiry(expiry);
    tp.setReducedServer(
        CubidCloud.getBridge().getLocalService().getServiceId().getName());
    tp.setReducedBy(sender.getUniqueId());
    tp.setReducedReason(reason);
    punishment.setPoints(newPoints);
    VenadeAPI.getPunishmentService().savePunishment(punishment);
    VenadeAPI.getPunishmentService().broadcastReducedPunishment(punishment);
  }

  public static class ReduceBanCommand extends ReducePunishmentBaseCommand {

    public ReduceBanCommand() {
      super(Ban.class, "rb", "rban");
    }
  }

  public static class ReduceMuteCommand extends ReducePunishmentBaseCommand {

    public ReduceMuteCommand() {
      super(Mute.class, "rm", "rmute");
    }
  }
}
