package net.venade.internal.core.impl.punish;

import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.database.redis.IRedisManager;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import java.util.ArrayList;
import java.util.List;
import net.venade.internal.api.punish.service.IPunishmentTemplateService;
import net.venade.internal.api.punish.template.PunishmentTemplate;
import org.bson.Document;

/**
 * @author JakeMT04
 * @since 27/10/2021
 */
public class DefaultPunishmentTemplateService implements IPunishmentTemplateService {
  private final IMongoManager mongoManager;
  private final IRedisManager redisManager;

  public DefaultPunishmentTemplateService(IMongoManager mongoManager, IRedisManager redisManager) {
    this.mongoManager = mongoManager;
    this.redisManager = redisManager;
  }
  @Override
  public void deletePunishmentTemplate(PunishmentTemplate template) {
    if (template.get_id() == null) {
      throw new IllegalArgumentException("This punishment is not saved to the database");
    }
    this.mongoManager.getCollection("venade_punishment_templates")
        .deleteOne(Filters.eq("_id", template.get_id()));

  }
  @Override
  public PunishmentTemplate getPunishmentTemplate(String type, String identifier) {
    return this.mongoManager.getCollection("venade_punishment_templates")
        .find(Filters.and(Filters.eq("type", type),
            Filters.eq("identifier", identifier.toLowerCase())))
        .map((document) -> this.mongoManager.toObject(document, PunishmentTemplate.class))
        .first();
  }

  @Override
  public boolean punishmentTemplateExists(String type, String identifier) {
    return this.mongoManager.getCollection("venade_punishment_templates")
        .countDocuments(Filters.and(Filters.eq("type", type),
            Filters.eq("identifier", identifier.toLowerCase()))) != 0;
  }

  // TODO: Add a cache maybe?
  @Override
  public List<PunishmentTemplate> getPunishmentTemplates(String type) {
    return this.mongoManager.getCollection("venade_punishment_templates")
        .find(
            Filters.eq("type", type.toLowerCase())
        )
        .sort(Sorts.ascending("name"))
        .map(document -> this.mongoManager.toObject(document, PunishmentTemplate.class))
        .into(new ArrayList<>());
  }
  @Override
  public void savePunishmentTemplate(PunishmentTemplate template, boolean async) {
    if (async) {
      this.mongoManager.runAsync(() -> this.doTemplateSave(template));
    } else {
      this.doTemplateSave(template);
    }
  }

  private void doTemplateSave(PunishmentTemplate template) {
    try {
      if (template.get_id() == null) {
        Document document = this.mongoManager.toDocument(template);
        this.mongoManager.getCollection("venade_punishment_templates").insertOne(document);
        template.set_id(document.getObjectId("_id"));
      } else {
        Document doc = this.mongoManager.toDocument(template);
        doc.remove("_id");
        this.mongoManager.getCollection("venade_punishment_templates")
            .replaceOne(Filters.eq("_id", template.get_id()), doc);

      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw ex;
    }
  }
}
