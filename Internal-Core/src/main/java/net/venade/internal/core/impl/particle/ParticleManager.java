package net.venade.internal.core.impl.particle;

import net.venade.internal.api.particle.IParticleManager;
import net.venade.internal.core.bukkit.BukkitCore;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author Nikolas Rummel
 * @since 23.08.2021
 */
public class ParticleManager implements IParticleManager {

  private final BukkitCore plugin;

  public ParticleManager(BukkitCore plugin) {
    this.plugin = plugin;
  }

  @Override
  public void spawnParticle(Particle effect, Location location) {
  }

  @Override
  public void spawnCircleParticle(Particle effect, Location location) {
    new BukkitRunnable() {

      @Override
      public void run() {
        for (int i = 0; i < 360; i += 360 / 30) {
          double angle = (i * Math.PI / 180);
          double x = 0.5 * Math.cos(angle);
          double z = 0.5 * Math.sin(angle);

          double r = 41 / 255F;
          double g = 216 / 255F;
          double b = 255 / 255F;

          location.getWorld().spawnParticle(effect, location.add(x, 0, z), 2, 0F, 0.01F, 0F, 0F);
        }
      }
    }.runTaskTimerAsynchronously(plugin, 20, 20);
  }
}
