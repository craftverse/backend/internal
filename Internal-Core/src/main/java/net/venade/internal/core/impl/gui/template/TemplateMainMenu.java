package net.venade.internal.core.impl.gui.template;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
public class TemplateMainMenu implements InventoryProvider {

  private final ICorePlayer corePlayer;
  private final SmartInventory inventory;

  public TemplateMainMenu(ICorePlayer corePlayer) {
    this.inventory = SmartInventory.builder()
        .manager(VenadeAPI.getInventoryManager())
        .provider(this)
        .id("punish")
        .title(HexColor.translate("Edit templates"))
        .closeable(true)
        .size(1, 9)
        .build();
    this.corePlayer = corePlayer;
  }

  public void openInventory() {
    this.inventory.open(corePlayer.getPlayer());
  }

  public void closeInventory() {
    this.inventory.close(corePlayer.getPlayer());
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    ItemStack banItem = ItemBuilder.builder(Material.CHEST)
        .display("§bBan")
        .amount(1)
        .build();

    ClickableItem banItemClickable = ClickableItem.of(banItem,
        (event) -> new TemplateGUI(corePlayer, "ban").openInventory());
    ItemStack muteItem = ItemBuilder.builder(Material.OAK_SIGN)
        .display("§bMute")
        .amount(1)
        .build();

    ClickableItem muteItemClickable = ClickableItem.of(muteItem,
        (event) -> new TemplateGUI(corePlayer, "mute").openInventory());

    contents.set(0, 2, banItemClickable);
    contents.set(0, 6, muteItemClickable);

  }

  @Override
  public void update(Player player, InventoryContents contents) {

  }
}
