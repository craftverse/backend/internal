package net.venade.internal.core.impl.npc.packet;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.PacketPlayInUseEntity;
import net.minecraft.network.protocol.game.PacketPlayInUseEntity.c;
import net.minecraft.world.EnumHand;
import net.minecraft.world.phys.Vec3D;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.entities.IPacketReader;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 10.03.2021, 14:33 Copyright (c) 2021
 */
public class PacketReader implements IPacketReader {

  private Channel channel;
  private final HashMap<UUID, Channel> channels = new HashMap<>();

  @Override
  public void inject(Player player) {
    CraftPlayer craftPlayer = (CraftPlayer) player;
    channel = craftPlayer.getHandle().b.a.k;
    channels.put(player.getUniqueId(), channel);
    if (channel.pipeline().get("PacketInjector") != null) {
      return;
    }

    channel
        .pipeline()
        .addAfter(
            "decoder",
            "PacketInjector",
            new MessageToMessageDecoder<PacketPlayInUseEntity>() {
              @Override
              protected void decode(
                  ChannelHandlerContext channelHandlerContext,
                  PacketPlayInUseEntity packet,
                  List<Object> arg) {
                arg.add(packet);
                readPacket(player, packet);
              }
            });
  }

  @Override
  public void uninject(Player player) {
    channel = channels.get(player.getUniqueId());
    if (channel.pipeline().get("PacketInjector") != null) {
      channel.pipeline().remove("PacketInjector");
    }
  }

  @Override
  public void readPacket(Player player, Packet<?> packet) {
    try {
      if (packet instanceof PacketPlayInUseEntity entityPacket) {
        entityPacket.a(new c() {
          @Override
          public void a(EnumHand enumHand) {
            if (enumHand == EnumHand.a) {
              int id = (int) getValue(packet, "a");
              VenadeAPI.getNonPlayerCharacterManager().handleRightClick(player, id);
            }
          }

          @Override
          public void a(EnumHand enumHand, Vec3D vec3D) {
          }

          @Override
          public void a() {
            int id = (int) getValue(packet, "a");
            VenadeAPI.getNonPlayerCharacterManager().handleLeftClick(player, id);
          }
        });
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Object getValue(Object packet, String name) {
    Object result = null;
    try {
      Field field = packet.getClass().getDeclaredField(name);
      field.setAccessible(true);
      result = field.get(packet);
      field.setAccessible(false);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return result;
  }

  @Override
  public Channel getChannel() {
    return channel;
  }

  @Override
  public Map<UUID, Channel> getChannelMap() {
    return channels;
  }
}
