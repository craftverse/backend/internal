package net.venade.internal.core.impl.language;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 01.03.2021, 19:37 Copyright (c) 2021
 */
public class LanguageManager {

  private static LanguageManager languageManager;
  private Properties properties;

  public LanguageManager() {
    languageManager = this;
    loadMessages();
  }

  public static LanguageManager getInstance() {
    return languageManager;
  }

  private void loadMessages() {
    properties = new Properties();
    try {
      FileInputStream input = new FileInputStream("plugins//language//messages.properties");
      properties.load(new InputStreamReader(input, StandardCharsets.UTF_8));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Properties getProperties() {
    return properties;
  }
}
