package net.venade.internal.core.impl.filter.command;

import cloud.cubid.common.database.mongo.IMongoManager;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.core.VenadeCore;

/**
 * @author JakeMT04
 * @since 23/11/2021
 */
public class RemoveEntryCommand extends VenadeCommand {

  public RemoveEntryCommand() {
    super(
        "removeentry",
        "core.filter.command.removeentry.desc",
        null
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, String identifier) {
    IMongoManager mongoManager = VenadeCore.getInstance().getMongoManager();
    DeleteResult result = mongoManager.getCollection("venade_chat_filter").deleteOne(Filters.eq("identifier", identifier));
    if (result.getDeletedCount() == 0) {
      sender.sendMessage("core.filter.nonexist", identifier);
      return;
    }
    sender.sendMessage("core.filter.remove", identifier);
  }

}
