package net.venade.internal.core.impl.player;

import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.md_5.bungee.api.ChatMessageType;
import net.venade.internal.api.color.HexColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 20/12/2021
 */
public class VanishActionBarTask implements Runnable {

  static final Set<UUID> IGNORED = new HashSet<>();

  @Override
  public void run() {
    for (Player player : Bukkit.getOnlinePlayers()) {
      if (player.hasMetadata("vanished")) {
        if (IGNORED.contains(player.getUniqueId())) {
          continue;
        }
        String message;
        try {
          message = player.getMetadata("vanishedMessage").get(0).asString();
        } catch (IndexOutOfBoundsException e) {
          message = null;
        }
        if (message == null) {
          message = CubidLanguage.getLanguageAPI().getLanguageString("core.vanished.actionbar",
              Language.ENGLISH);
        }
        player.spigot()
            .sendMessage(ChatMessageType.ACTION_BAR, HexColor.translateToCompoment(message));
      }
    }
  }
}
