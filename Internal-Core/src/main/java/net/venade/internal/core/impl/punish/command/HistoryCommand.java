package net.venade.internal.core.impl.punish.command;

import cubid.cloud.modules.player.IOfflineCubidPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.core.impl.gui.history.HistoryGUIMainMenu;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public class HistoryCommand extends VenadeCommand {

  private final JavaPlugin plugin;

  public HistoryCommand(JavaPlugin plugin) {
    super("history", "core.history.desc",
        DefaultPunishmentService.COMMAND_PERMISSION, "c", "check", "hist"
    );
    this.plugin = plugin;
    this.setAsync(true);
  }

  @CommandMethod
  public void onCommand(Sender sender, IOfflineCubidPlayer target) {
    int currentBanPoints = VenadeAPI.getPunishmentService().getPunishmentPoints("ban", target.getUniqueId());
    int currentMutePoints = VenadeAPI.getPunishmentService().getPunishmentPoints("mute", target.getUniqueId());
    HistoryGUIMainMenu gui = new HistoryGUIMainMenu(sender.getAsCorePlayer(), target, currentBanPoints, currentMutePoints);
    Bukkit.getScheduler().runTask(plugin, gui::openInventory);
  }

}
