package net.venade.internal.core.impl.gui.report;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.List;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.report.Report;
import net.venade.internal.api.report.Report.ReportReason;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 28/10/2021
 */
public class ReportGUI {

  public static class ConfirmGUI implements InventoryProvider {

    private final SmartInventory inventory;
    private final IOfflineCubidPlayer reported;
    private final ICorePlayer player;
    private final Report.ReportReason reason;

    public ConfirmGUI(IOfflineCubidPlayer reported,
        ICorePlayer corePlayer, ReportReason reason) {
      this.reported = reported;
      this.player = corePlayer;
      this.reason = reason;
      this.inventory = SmartInventory.builder()
          .type(InventoryType.CHEST)
          .size(3, 9)
          .closeable(true)
          .title(player.getLanguageString("core.report.gui.confirm.title"))
          .provider(this)
          .manager(VenadeAPI.getInventoryManager())
          .id("report-confirm")
          .build();
    }

    public void openInventory() {
      this.inventory.open(this.player.getPlayer());
    }

    public void closeInventory() {
      this.inventory.close(this.player.getPlayer());
    }


    @Override
    public void init(Player player, InventoryContents contents) {
      OfflinePlayer op = Bukkit.getOfflinePlayer(this.reported.getUniqueId());
      ClickableItem skull = ClickableItem.empty(
          ItemBuilder.builder(Material.PLAYER_HEAD, ItemBuilder.SKULL)
              .owner(op)
              .display(this.player.getLanguageString("core.report.gui.confirm.skull.name"))
              .lore(this.player.getLanguageString("core.report.gui.confirm.skull.lore",
                  this.reported.getRank().getColor() + this.reported.getName()))
              .build()
      );
      List<String> lore = new ArrayList<>();
      for (String part : WordUtils.wrap(this.player.getLanguageString(reason.getDescription()), 40,
          "\n", false).split("\n")) {
        lore.add("§7" + part);
      }
      ClickableItem reasonItem = ClickableItem.empty(
          ItemBuilder.builder(reason.getItem())
              .display("§b" + reason.getDisplayReason())
              .setLore(lore)
              .unbreakable()
              .build()
      );

      ClickableItem confirmItem = ClickableItem.of(
          ItemBuilder.builder(Material.BELL)
              .display(this.player.getLanguageString("core.report.gui.confirm.confirm.title"))
              .lore(
                  this.player.getLanguageString("core.report.gui.confirm.confirm.lore.name",
                      this.reported.getRank().getColor() + this.reported.getName()),
                  this.player.getLanguageString("core.report.gui.confirm.confirm.lore.reason",
                      reason.getDisplayReason()),
                  "",
                  this.player.getLanguageString("core.report.gui.confirm.confirm.lore.abuse")
              ).build(), (e) -> this.doConfirmation());
      contents.set(1, 2, skull)
          .set(1, 4, reasonItem)
          .set(1, 6, confirmItem);
    }

    private void doConfirmation() {
      this.closeInventory();
      Report report = Report.builder()
          .reportedAt(System.currentTimeMillis())
          .reporter(this.player.getUniqueId())
          .reported(this.reported.getUniqueId())
          .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
          .reason(reason.getReportReason())
          .closedByMute(reason.isClosedByMute())
          .build();
      VenadeAPI.getReportService().saveReport(report);
      VenadeAPI.getReportService().broadcastReport(report);
      this.player.sendMessage("core.report.success",
          reported.getRank().getColor() + reported.getName(), reason.getDisplayReason());

    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
  }


  public static class ReasonGUI implements InventoryProvider {

    private final SmartInventory inventory;
    private final IOfflineCubidPlayer reported;
    private final ICorePlayer player;

    public ReasonGUI(ICorePlayer player, IOfflineCubidPlayer reported) {
      this.reported = reported;
      this.player = player;
      this.inventory = SmartInventory.builder()
          .type(InventoryType.CHEST)
          .size((int) Math.ceil((float) Report.REPORT_REASONS.size() / 7) + 2, 9)
          .closeable(true)
          .title("Select a reason")
          .provider(this)
          .manager(VenadeAPI.getInventoryManager())
          .id("report")
          .build();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
      contents.fillBorders(ClickableItem.empty(
          ItemBuilder.builder(Material.BLACK_STAINED_GLASS_PANE).display("§r").build()));
      int col = 1;
      int row = 1;
      for (Report.ReportReason reason : Report.REPORT_REASONS.values()) {
        List<String> lore = new ArrayList<>();
        for (String part : WordUtils.wrap(this.player.getLanguageString(reason.getDescription()),
            40, "\n", false).split("\n")) {
          lore.add("§7" + part);
        }
        ItemStack item = ItemBuilder.builder(reason.getItem())
            .display("§b" + reason.getDisplayReason())
            .setLore(lore)
            .unbreakable()
            .build();
        contents.set(row, col, ClickableItem.of(item,
            (e) -> new ConfirmGUI(reported, this.player, reason).openInventory()));
        if (col == 7) {
          col = 1;
          row++;
        } else {
          col++;
        }
      }
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    public void openInventory() {
      this.inventory.open(this.player.getPlayer());
    }
  }


}