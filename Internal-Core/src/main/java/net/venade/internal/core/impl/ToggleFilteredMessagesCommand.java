package net.venade.internal.core.impl;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.CustomProvider;
import net.venade.internal.api.command.annotation.Default;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.ICorePlayer.RecvMode;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;

/**
 * @author JakeMT04
 * @since 23/10/2021
 */
public class ToggleFilteredMessagesCommand extends VenadeCommand {


  public ToggleFilteredMessagesCommand() {
    super(
        "togglefilter",
        "core.togglefilter.command.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        "tfm"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender, @Default("next") @CustomProvider("filter") RecvMode mode) {
    ICorePlayer corePlayer = sender.getAsCorePlayer();
    corePlayer.setFilterMessagesMode(mode);
    VenadeAPI.getPlayerService().savePlayer(corePlayer);
    sender.sendMessage("core.togglefilter." + mode.keyName());
  }
}
