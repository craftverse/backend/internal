package net.venade.internal.core.bukkit.listener;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.Optional;
import lombok.AllArgsConstructor;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.impl.network.CoreMessagesListener;
import net.venade.internal.core.impl.punish.command.punish.MuteChatCommand;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.digest.DigestUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

/**
 * @author JakeMT04
 * @since 16/12/2021
 */
@AllArgsConstructor
public class PunishmentPreventionsListener implements Listener {

  private final VenadeCore core;

  @EventHandler(priority = EventPriority.LOWEST)
  public void onMute(AsyncPlayerChatEvent event) {
    if (MuteChatCommand.IS_MUTED) {
      CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(event.getPlayer().getUniqueId())
          .ifPresentOrElse((p) -> {
            if (!p.hasRankOrHigher(CubidRank.MOD)) {
              event.setCancelled(true);
              event.getPlayer()
                  .sendMessage(HexColor.translate(p.getLanguageString("core.mutechat.current")));
            }
          }, () -> {
            event.setCancelled(true);
            event.getPlayer().sendMessage(HexColor.translate(
                CubidLanguage.getLanguageAPI()
                    .getLanguageString("core.mutechat.current", Language.ENGLISH)));
          });
    }
    Mute active = core.getPlayerService().getPunishmentService()
        .getActivePunishment(Mute.class, event.getPlayer().getUniqueId());
    if (active != null) {
      event.setCancelled(true);
      CubidCloud.getBridge().getPlayerManager().getOnlinePlayer(event.getPlayer().getUniqueId())
          .ifPresent((player) -> event.getPlayer()
              .sendMessage(active.getDisplayScreen(player.getLanguage())));

    }
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPreLogin(AsyncPlayerPreLoginEvent event) {
    // Need to obtain the cubid player, need their active language
    Language lang = Language.ENGLISH;
    Optional<IOfflineCubidPlayer> offline = CubidCloud.getBridge().getPlayerManager()
        .getOfflinePlayer(event.getUniqueId());
    if (offline.isPresent()) {
      lang = offline.get().getLanguage();
    }
    if (VenadeAPI.getPunishmentService().isBlockedName(event.getName().toUpperCase())) {
      event.disallow(Result.KICK_BANNED,
          CubidLanguage.getLanguageAPI()
              .getLanguageString("core.blockedname.kick", lang, event.getName()));
      return;
    }
    Ban active = core.getPlayerService().getPunishmentService()
        .getActivePunishment(Ban.class, event.getUniqueId());
    if (active != null) {
      event.disallow(Result.KICK_BANNED, active.getDisplayScreen(lang));
    } else {
      if (CoreMessagesListener.UPDATE_CACHE.getIfPresent(event.getUniqueId()) != null) {
        CoreMessagesListener.UPDATE_CACHE.invalidate(event.getUniqueId());
        String ipToSave = DigestUtils.md5Hex(event.getAddress().getHostAddress());
        core.getPlayerService().updateIPHistory(event.getUniqueId(), ipToSave);
      }
    }
  }
}
