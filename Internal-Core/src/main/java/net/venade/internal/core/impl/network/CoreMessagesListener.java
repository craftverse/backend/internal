package net.venade.internal.core.impl.network;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.networking.message.MessageId;
import cloud.cubid.networking.subscription.ICubidChannelListener;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import cubid.cloud.modules.player.CubidPlayer;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.message.PlayerJoinMessage;
import cubid.cloud.modules.player.message.PlayerQuitMessage;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.core.VenadeCore;
import net.venade.internal.core.bukkit.listener.PlayerJoinQuitListener;
import net.venade.internal.core.impl.gui.report.ReportsGUI;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.digest.DigestUtils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 16/10/2021
 */
public class CoreMessagesListener implements ICubidChannelListener {

  public static final Cache<UUID, ReportTeleportPacket> TELEPORT_CACHE = CacheBuilder.newBuilder()
      .expireAfterWrite(30, TimeUnit.SECONDS)
      .build();
  public static final Cache<UUID, Boolean> UPDATE_CACHE = CacheBuilder.newBuilder()
      .expireAfterWrite(15, TimeUnit.SECONDS)
      .build();
  private final Gson gson = (new GsonBuilder()).create();
  private final VenadeCore core;
  private final JavaPlugin plugin;

  public CoreMessagesListener(VenadeCore core, JavaPlugin plugin) {
    this.core = core;
    this.plugin = plugin;
  }

  public void handle(String channel, String jsonMessage) {
    JsonObject jsonObject = this.gson.fromJson(jsonMessage, JsonObject.class);
    if (jsonObject.has("packetId")) {
      JsonElement element = jsonObject.get("packetId");
      int messageId = element.getAsJsonPrimitive().getAsInt();
      if (messageId == 9001) {
        FancyPlayerMessage action = this.gson.fromJson(jsonMessage, FancyPlayerMessage.class);
        BaseComponent[] message = ComponentSerializer.parse(
            action.getDocument().get("message").getAsString());
        Player player = Bukkit.getPlayer(action.getTarget());
        if (player == null) {
          return;
        }
        if (!player.isOnline()) {
          return;
        }
        player.spigot().sendMessage(message);
      } else if (messageId == 9002) {
        ReportsGUI.STATUS_UUID = UUID.randomUUID();
      } else if (messageId == 9003) {
        StaffMessagePacket packet = this.gson.fromJson(jsonMessage, StaffMessagePacket.class);
        for (Player player : Bukkit.getOnlinePlayers()) {
          ICorePlayer corePlayer = core.getPlayerService()
              .getOnlinePlayer(player.getUniqueId()).orElse(null);
          if (corePlayer == null) {
            continue;
          }
          ICubidPlayer cubidPlayer = corePlayer.getCubidPlayer();
          if (cubidPlayer == null) {
            continue;
          }
          if (packet.canSendTo(cubidPlayer, corePlayer)) {
            if (packet.isFancy()) {
              BaseComponent[] message = ComponentSerializer.parse(
                  packet.getMessage().get(cubidPlayer.getLanguage()).get("message").getAsString());
              player.spigot().sendMessage(message);
            } else {
              player.sendMessage(
                  packet.getMessage().get(cubidPlayer.getLanguage()).get("message").getAsString());
            }

          }
        }
      } else if (messageId == 9004) {
        ReportTeleportPacket packet = this.gson.fromJson(jsonMessage, ReportTeleportPacket.class);
        if (packet.getTargetServer()
            .equalsIgnoreCase(CubidCloud.getBridge().getLocalService().getServiceId().getName())) {
          // check the source player is online
          Player player = Bukkit.getPlayer(packet.getSourcePlayer());
          if (player == null) {
            TELEPORT_CACHE.put(packet.getSourcePlayer(), packet);
          } else {
            PlayerJoinQuitListener.doReportTeleport(player, packet);

          }
        }
      } else if (messageId == MessageId.PLAYER_JOIN) {
        // Only update IP information on initial join.
        // This saves updating it each time.
        PlayerJoinMessage message = this.gson.fromJson(jsonMessage, PlayerJoinMessage.class);
        CubidPlayer joiningCubidPlayer = message.getCubidPlayer();
        UUID uuid = joiningCubidPlayer.getUniqueId();
        Player bukkitPlayer = Bukkit.getPlayer(uuid);
        if (bukkitPlayer != null) {
          UPDATE_CACHE.invalidate(uuid);
          String ipToSave = DigestUtils.md5Hex(
              bukkitPlayer.getAddress().getAddress().getHostAddress());
          core.getPlayerService().updateIPHistory(bukkitPlayer.getUniqueId(), ipToSave);
        } else {
          UPDATE_CACHE.put(uuid, true);
        }
        if (joiningCubidPlayer.getRank().isStaff()) {

          Bukkit.getScheduler().runTaskLater(plugin, () -> {
            CubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
                .getOnlinePlayer(joiningCubidPlayer.getUniqueId()).orElse(null);
            if (cubidPlayer == null) {
              return;
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
              ICorePlayer corePlayer = core.getPlayerService()
                  .getOnlinePlayer(player.getUniqueId()).orElse(null);
              if (corePlayer == null) {
                continue;
              }
              ICubidPlayer otherCubidPlayer = corePlayer.getCubidPlayer();
              if (otherCubidPlayer == null) {
                continue;
              }
              if (StaffMessagePacket.canSendTo("join",
                  CubidCloud.getBridge().getLocalService().getServiceId().getName(),
                  otherCubidPlayer,
                  corePlayer)) {
                corePlayer.sendMessage("core.staff.joined",
                    cubidPlayer.getConnectedService(),
                    cubidPlayer.getRank().getColor() + cubidPlayer.getName());
              }
            }

          }, 2 * 20L);
        }

      } else if (messageId == MessageId.PLAYER_QUIT) {
        PlayerQuitMessage message = this.gson.fromJson(jsonMessage, PlayerQuitMessage.class);
        CubidPlayer cubidPlayer = message.getCubidPlayer();
        if (message.getServer() == null || message.getServer().equalsIgnoreCase("<unknown>")) {
          // taking a chance they got kicked early, and don't send a leave message
          return;
        }
        if (cubidPlayer.getRank().isStaff()) {
          for (Player player : Bukkit.getOnlinePlayers()) {
            ICorePlayer corePlayer = core.getPlayerService()
                .getOnlinePlayer(player.getUniqueId()).orElse(null);
            if (corePlayer == null) {
              continue;
            }
            ICubidPlayer otherCubidPlayer = corePlayer.getCubidPlayer();
            if (otherCubidPlayer == null) {
              continue;
            }
            if (StaffMessagePacket.canSendTo("leave",
                CubidCloud.getBridge().getLocalService().getServiceId().getName(), otherCubidPlayer,
                corePlayer)) {
              corePlayer.sendMessage("core.staff.left",
                  message.getCubidPlayer().getConnectedService(), cubidPlayer.getRank().getColor() + cubidPlayer.getName());
            }
          }
        }
      }

    }
  }
}

