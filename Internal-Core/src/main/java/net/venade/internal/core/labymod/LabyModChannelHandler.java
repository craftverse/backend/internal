package net.venade.internal.core.labymod;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 * @author Nikolas Rummel
 * @since 11.08.2021
 */
public class LabyModChannelHandler implements PluginMessageListener {

  public LabyModChannelHandler(JavaPlugin plugin) {
    plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, "labymod3:main", this);
  }

  @Override
  public void onPluginMessageReceived(String channel, Player player, byte[] message) {
    if (!channel.equals("labymod3:main")) {
      return;
    }

    DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));

    ByteBuf buf = Unpooled.wrappedBuffer(message);
    String key = LabyModProtocol.readString(buf, Short.MAX_VALUE);
    String json = LabyModProtocol.readString(buf, Short.MAX_VALUE);

    // LabyMod user joins the server
    if (key.equals("INFO")) {
      System.out.println(json);
    }
  }
}
