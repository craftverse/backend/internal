package net.venade.internal.core.impl.punish.command.punish.blockedname;

import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;

/**
 * @author JakeMT04
 * @since 23/12/2021
 */
public class BlockedNameCommand extends VenadeCommand {

  public BlockedNameCommand() {
    super(
        "blockedname",
        "core.blockedname.command.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        true
    );
    this.setAsync(true);
    this.registerSubCommand(new AddBlockedNameCommand());
    this.registerSubCommand(new RemoveBlockedNameCommand());
  }
}
