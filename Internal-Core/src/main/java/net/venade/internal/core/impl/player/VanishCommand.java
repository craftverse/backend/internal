package net.venade.internal.core.impl.player;

import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.core.impl.punish.DefaultReportService;

/**
 * @author JakeMT04
 * @since 20/12/2021
 */
public class VanishCommand extends VenadeCommand {

  public VanishCommand() {
    super(
        "vanish",
        "core.vanished.command.desc",
        DefaultReportService.COMMAND_PERMISSION,
        "v"
    );
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    if (sender.isConsole()) {
      return;
    }
    ICorePlayer corePlayer = sender.getAsCorePlayer();
    corePlayer.setVanished(!corePlayer.isVanished());
    VenadeAPI.getPlayerService().savePlayer(corePlayer);
    sender.sendMessage(
        corePlayer.isVanished() ? "core.vanished.vanished" : "core.vanished.unvanished");
  }
}
