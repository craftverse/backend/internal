package net.venade.internal.core.impl.network;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.Language;
import cloud.cubid.networking.message.NetworkingMessage;
import cubid.cloud.modules.player.ICubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.util.Map;
import lombok.Getter;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.ICorePlayer.RecvMode;

/**
 * @author JakeMT04
 * @since 20/11/2021
 */
public class StaffMessagePacket extends NetworkingMessage {

  @Getter
  private final Map<Language, JsonDocument> message;
  @Getter
  private final boolean fancy;
  @Getter
  private final String sourceServer;
  private final String type;

  public StaffMessagePacket(Map<Language, JsonDocument> map, String type, String sourceServer,
      boolean fancy) {
    super(9003);
    this.message = map;
    this.fancy = fancy;
    this.type = type;
    this.sourceServer = sourceServer;
  }

  public static boolean canSendTo(String type, String sourceServer, ICubidPlayer cubidPlayer,
      ICorePlayer corePlayer) {
    boolean staffMessagesEnabled = corePlayer.getStaffMessagesMode() == RecvMode.ON || (
        corePlayer.getStaffMessagesMode() == RecvMode.SERVER && sourceServer.equals(
            CubidCloud.getBridge().getLocalService().getServiceId().getName()));
    switch (type) {
      case "chat" -> {
        return cubidPlayer.getRank().isStaff() && corePlayer.getStaffMessagesMode() != RecvMode.OFF;
      }
      case "report", "punish" -> {
        boolean permissionCheck =
            cubidPlayer.hasRankOrHigher(CubidRank.SR_CONT) || cubidPlayer.hasRank(CubidRank.MOD)
                || cubidPlayer.hasRank(CubidRank.COMM);
        if (!permissionCheck) {
          return false;
        }
        return staffMessagesEnabled;
      }
      case "filter" -> {
        boolean permissionCheck =
            cubidPlayer.hasRankOrHigher(CubidRank.SR_CONT) || cubidPlayer.hasRank(CubidRank.MOD)
                || cubidPlayer.hasRank(CubidRank.COMM);
        if (!permissionCheck) {
          return false;
        }
        // can recv staff:
        return staffMessagesEnabled && (corePlayer.getFilterMessagesMode() == RecvMode.ON || (
            corePlayer.getFilterMessagesMode() == RecvMode.SERVER && sourceServer.equals(
                CubidCloud.getBridge().getLocalService().getServiceId().getName())));
      }
      default -> {
        return staffMessagesEnabled && cubidPlayer.getRank().isStaff();
      }
    }

  }

  public boolean canSendTo(ICubidPlayer cubidPlayer, ICorePlayer corePlayer) {
    return canSendTo(this.type, this.sourceServer, cubidPlayer, corePlayer);
  }

}
