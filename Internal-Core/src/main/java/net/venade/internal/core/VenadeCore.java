package net.venade.internal.core;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.database.mongo.DefaultMongoManager;
import cloud.cubid.common.database.mongo.IMongoManager;
import cloud.cubid.common.database.mongo.MongoConfiguration;
import cloud.cubid.common.database.redis.DefaultRedisManager;
import cloud.cubid.common.database.redis.IRedisManager;
import cloud.cubid.common.database.redis.RedisConfiguration;
import cloud.cubid.common.utils.adress.CubidRedisConnection;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.exc.CommandArgumentException;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Provider;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.command.registration.CommandManager;
import net.venade.internal.api.document.IConfiguration;
import net.venade.internal.api.document.json.JsonConfiguration;
import net.venade.internal.api.inventory.InventoryManager;
import net.venade.internal.core.impl.DefaultProviders;
import net.venade.internal.core.impl.StaffChatCommand;
import net.venade.internal.core.impl.ToggleFilteredMessagesCommand;
import net.venade.internal.core.impl.ToggleStaffMessagesCommand;
import net.venade.internal.core.impl.filter.ChatFilterManager.ChatFilterType;
import net.venade.internal.core.impl.filter.command.ChatFilterCommand;
import net.venade.internal.core.impl.network.CoreMessagesListener;
import net.venade.internal.core.impl.npc.NonPlayerCharacterManager;
import net.venade.internal.core.impl.player.DefaultPlayerService;
import net.venade.internal.core.impl.player.VanishActionBarTask;
import net.venade.internal.core.impl.player.VanishCommand;
import net.venade.internal.core.impl.punish.command.AltsCommand;
import net.venade.internal.core.impl.punish.command.HistoryCommand;
import net.venade.internal.core.impl.punish.command.punish.ClearChatCommand;
import net.venade.internal.core.impl.punish.command.punish.KickCommand;
import net.venade.internal.core.impl.punish.command.punish.MuteChatCommand;
import net.venade.internal.core.impl.punish.command.punish.PunishCommand;
import net.venade.internal.core.impl.punish.command.punish.PunishCommand.PunishType;
import net.venade.internal.core.impl.punish.command.punish.blockedname.BlockedNameCommand;
import net.venade.internal.core.impl.punish.command.punish.reduce.ReducePunishmentBaseCommand;
import net.venade.internal.core.impl.punish.command.punish.remove.RemovePunishmentBaseCommand;
import net.venade.internal.core.impl.punish.command.template.PunishTemplateCommand;
import net.venade.internal.core.impl.report.AcceptReportCommand;
import net.venade.internal.core.impl.report.CloseReportCommand;
import net.venade.internal.core.impl.report.ReportCommand;
import net.venade.internal.core.impl.report.ReportsCommand;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
@Getter
public class VenadeCore {

  private static VenadeCore instance;
  public final HashMap<UUID, Long> playTime;
  /* API Stuff */
  private NonPlayerCharacterManager nonPlayerCharacterManager;
  private InventoryManager inventoryManager;
  private DefaultPlayerService playerService;

  // central DB instances
  private IMongoManager mongoManager;
  private IRedisManager redisManager;
  private MongoConfiguration mongoConfiguration;
  private RedisConfiguration redisConfiguration;

  public VenadeCore() {
    instance = this;
    this.playTime = new HashMap<>();
  }

  public static VenadeCore getInstance() {
    return instance;
  }

  public void initializeBukkit(JavaPlugin plugin) {
    // connect to same db that cubid uses
    IConfiguration<CubidBridgeConfig> config = new JsonConfiguration<>(
        new File("plugins/Cubid/" + "CubidBridgeData.json"), CubidBridgeConfig.class).load();
    this.mongoConfiguration = config.get().getMongoConfiguration();
//        new MongoConfiguration("144.91.92.250", "m2jVXgX2aP8UGx9b", "cubid", "cubid", 27017);
    this.mongoManager = new DefaultMongoManager(mongoConfiguration);

    this.redisConfiguration = config.get()
        .getRedisConfiguration();//new RedisConfiguration("144.91.92.250", "m2jVXgX2aP8UGx9b", 6379, 0);

    final CubidRedisConnection connection =
        new CubidRedisConnection(
            getRedisConfiguration().getPassword(),
            getRedisConfiguration().getHost(),
            getRedisConfiguration().getPort(),
            getRedisConfiguration().getDatabase());

    this.redisManager = new DefaultRedisManager(connection);

    this.inventoryManager = new InventoryManager(plugin);
    this.inventoryManager.init(20);

    this.nonPlayerCharacterManager = new NonPlayerCharacterManager();
    this.playerService = new DefaultPlayerService(this.mongoManager, this.redisManager);
    plugin.getServer().getScheduler()
        .runTaskTimerAsynchronously(plugin, new VanishActionBarTask(), 0L, 5L);

    VenadeAPI.setInventoryManager(this.inventoryManager);
    VenadeAPI.setNonPlayerCharacterManager(this.nonPlayerCharacterManager);
    VenadeAPI.setPlayerService(this.playerService);

    DefaultProviders.register();
    Providers.bind(PunishType.class, new Provider<>() {
      @Override
      public PunishType provide(Context context) throws CommandException {
        try {
          return PunishType.valueOf(context.getArgument().toUpperCase());
        } catch (IllegalArgumentException e) {
          String accepted =
              Arrays.stream(PunishType.values())
                  .map(l -> "§f" + l.name().toLowerCase())
                  .collect(Collectors.joining("§c, "));
          throw new CommandArgumentException(true, "§cType must be one of: " + accepted + "§c.");
        }
      }

      @Override
      public List<String> suggest(Context context) {
        return Arrays.stream(PunishType.values())
            .map(l -> l.name().toLowerCase())
            .sorted()
            .collect(Collectors.toList());
      }
    });
    Providers.bind(ChatFilterType.class, new Provider<>() {
      @Override
      public ChatFilterType provide(Context context) throws CommandException {
        try {
          return ChatFilterType.valueOf(context.getArgument().toUpperCase());
        } catch (IllegalArgumentException e) {
          String accepted =
              Arrays.stream(PunishType.values())
                  .map(l -> "§f" + l.name().toLowerCase())
                  .collect(Collectors.joining("§c, "));
          throw new CommandArgumentException(true, "§cType must be one of: " + accepted + "§c.");
        }
      }

      @Override
      public List<String> suggest(Context context) {
        return Arrays.stream(ChatFilterType.values())
            .map(l -> l.name().toLowerCase())
            .sorted()
            .collect(Collectors.toList());
      }
    });
    CubidCloud.getBridge().getRedisManager()
        .addChannelListener(new CoreMessagesListener(this, plugin));
    // Punish commands
    Arrays.asList(
        new PunishCommand(plugin),
        new PunishCommand.PunishStaticCommand(plugin, PunishType.MUTE),
        new PunishCommand.PunishStaticCommand(plugin, PunishType.BAN),
        new PunishTemplateCommand(),
        new HistoryCommand(plugin),
        new RemovePunishmentBaseCommand.UnbanCommand(),
        new RemovePunishmentBaseCommand.UnmuteCommand(),
        new ReducePunishmentBaseCommand.ReduceMuteCommand(),
        new ReducePunishmentBaseCommand.ReduceBanCommand(),
        new KickCommand(),
        new AltsCommand(),
        new BlockedNameCommand()
    ).forEach(c -> CommandManager.registerCommand(c, plugin, "vpunish"));
    Arrays.asList(
        new ReportCommand(plugin),
        new AcceptReportCommand(plugin),
        new ReportsCommand(plugin),
        new CloseReportCommand()
    ).forEach(c -> CommandManager.registerCommand(c, plugin, "vreports"));
    Arrays.asList(
        new ChatFilterCommand(),
        new ToggleFilteredMessagesCommand(),
        new MuteChatCommand(),
        new ClearChatCommand()
    ).forEach(c -> CommandManager.registerCommand(c, plugin, "vchat"));
    Arrays.asList(
        new ToggleStaffMessagesCommand(),
        new StaffChatCommand(),
        new VanishCommand()
    ).forEach(c -> CommandManager.registerCommand(c, plugin, "vstaff"));
  }

  public HashMap<UUID, Long> getPlayTime() {
    return playTime;
  }
}
