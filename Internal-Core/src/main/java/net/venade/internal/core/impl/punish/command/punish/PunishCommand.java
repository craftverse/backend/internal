package net.venade.internal.core.impl.punish.command.punish;

import cloud.cubid.bridge.CubidCloud;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.UUID;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.chat.GetChatMessage;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.annotation.Optional;
import net.venade.internal.api.punish.template.PunishmentTemplate;
import net.venade.internal.api.punish.type.Ban;
import net.venade.internal.api.punish.type.Mute;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.core.impl.gui.punish.PunishGUI;
import net.venade.internal.core.impl.gui.punish.PunishGUIMainMenu;
import net.venade.internal.core.impl.punish.DefaultPunishmentService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 16/10/2021
 */
public class PunishCommand extends VenadeCommand {

  private final JavaPlugin plugin;

  public PunishCommand(JavaPlugin plugin) {
    super("punish", "core.punish.command.desc",
        DefaultPunishmentService.COMMAND_PERMISSION,
        "pu");
    this.setAsync(true);
    this.plugin = plugin;
  }

  protected static void completePunishment(UUID executor, IOfflineCubidPlayer target,
      PunishType type,
      PunishmentTemplate template, String notes, int currentUserPoints) {
    long now = System.currentTimeMillis();
    long toAdd = template.totalDuration(currentUserPoints);
    long expiry;
    if (toAdd == 0) {
      expiry = 0;
    } else {
      expiry = now + template.totalDuration(currentUserPoints);
    }
    Punishment punishment;
    if (type == PunishType.BAN) {
      punishment = Ban.builder()
          .executor(executor)
          .target(target.getUniqueId())
          .points(template.getPointsToIssue())
          .issued(now)
          .expiry(expiry)
          .reason(template.getReason())
          .internalNotes(notes)
          .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
          .build();
    } else if (type == PunishType.MUTE) {
      punishment = Mute.builder()
          .executor(executor)
          .target(target.getUniqueId())
          .points(template.getPointsToIssue())
          .issued(now)
          .expiry(expiry)
          .reason(template.getReason())
          .internalNotes(notes)
          .server(CubidCloud.getBridge().getLocalService().getServiceId().getName())
          .build();
    } else {
      return;
    }
    VenadeAPI.getPunishmentService().savePunishment(punishment);
    VenadeAPI.getPunishmentService().applyPunishment(punishment);
    VenadeAPI.getPunishmentService().broadcastPunishment(punishment);
    VenadeAPI.getReportService().closeApplicableReports(punishment);

  }

  @CommandMethod
  public void onCommand(Sender sender, IOfflineCubidPlayer target, @Optional PunishType type,
      @Optional String template, @ConsumeRest @Optional String note) {
    if (!sender.isConsole() && target.getRank().isStaff()) {
      sender.sendMessage("core.punish.error.staff",
          target.getRank().getColor() + target.getName());
      return;
    }
    boolean alreadyBanned =
        VenadeAPI.getPunishmentService().getActivePunishment(Ban.class, target.getUniqueId())
            != null;
    boolean alreadyMuted =
        VenadeAPI.getPunishmentService().getActivePunishment(Mute.class, target.getUniqueId())
            != null;
    int banPoints = VenadeAPI.getPunishmentService()
        .getPunishmentPoints("ban", target.getUniqueId());
    int mutePoints = VenadeAPI.getPunishmentService()
        .getPunishmentPoints("mute", target.getUniqueId());
    if (type != null) {
      if (type == PunishType.BAN && alreadyBanned) {
        sender.sendMessage("core.punish.error.already.ban",
            target.getRank().getColor() + target.getName());
        return;
      }
      if (type == PunishType.MUTE && alreadyMuted) {
        sender.sendMessage("core.punish.error.already.mute",
            target.getRank().getColor() + target.getName());
        return;
      }
      if (template != null) {
        PunishmentTemplate temp = VenadeAPI.getPunishmentTemplateService()
            .getPunishmentTemplate(type.name().toLowerCase(), template);
        if (temp != null) {
          if (note == null) {
            if (sender.isConsole()) {
              sender.sendMessage(buildHelpMessage(sender));
              return;
            }
            new GetChatMessage(sender.getAsPlayer(),
                sender.getLanguageString("core.punish.internal-notes"),
                (notes) -> {
                  if (notes.equalsIgnoreCase("cancel")) {
                    sender.sendMessage("core.punish.cancel");
                    return;
                  }
                  Bukkit.getScheduler().runTaskAsynchronously(this.plugin,
                      () -> completePunishment(sender.getUniqueId(), target, type, temp, notes,
                          type == PunishType.BAN ? banPoints : mutePoints));
                });
          } else {
            completePunishment(sender.getUniqueId(), target, type, temp, note,
                type == PunishType.BAN ? banPoints : mutePoints);
          }
          return;
        } // if template is null then open GUI
      }
      if (type == PunishType.BAN) {
        PunishGUI gui = new PunishGUI(sender.getAsCorePlayer(), target, banPoints, "ban");
        Bukkit.getScheduler().runTask(plugin, gui::openInventory);
        return;
      }
      if (type == PunishType.MUTE) {
        PunishGUI gui = new PunishGUI(sender.getAsCorePlayer(), target, mutePoints, "mute");
        Bukkit.getScheduler().runTask(plugin, gui::openInventory);
      }
    } else {
      sender.sendMessage("core.punish.start",
          target.getRank().getColor() + target.getName());
      PunishGUIMainMenu gui = new PunishGUIMainMenu(sender.getAsCorePlayer(), target, banPoints,
          mutePoints, alreadyBanned, alreadyMuted);
      Bukkit.getScheduler().runTask(plugin, gui::openInventory);
    }
  }

  public enum PunishType {
    BAN, MUTE
  }

  public static class PunishStaticCommand extends VenadeCommand {

    private final PunishType type;
    private final JavaPlugin plugin;

    public PunishStaticCommand(JavaPlugin plugin, PunishType type) {
      super(
          type.name().toLowerCase(),
          "core.punish.command." + type.name().toLowerCase() + ".desc",
          DefaultPunishmentService.COMMAND_PERMISSION
      );
      this.type = type;
      this.plugin = plugin;
      this.setAsync(true);
    }

    @CommandMethod
    public void onCommand(Sender sender, IOfflineCubidPlayer target, @Optional String template,
        @ConsumeRest @Optional String note) {
      if (!sender.isConsole() && target.getRank().isStaff()) {
        sender.sendMessage("core.punish.error.staff",
            target.getRank().getColor() + target.getName());
        return;
      }

      int points;

      if (type == PunishType.BAN) {
        boolean alreadyBanned =
            VenadeAPI.getPunishmentService().getActivePunishment(Ban.class, target.getUniqueId())
                != null;
        if (alreadyBanned) {
          sender.sendMessage("core.punish.error.already.ban",
              target.getRank().getColor() + target.getName());
          return;
        }
        points = VenadeAPI.getPunishmentService()
            .getPunishmentPoints("ban", target.getUniqueId());
      } else if (type == PunishType.MUTE) {
        boolean alreadyMuted =
            VenadeAPI.getPunishmentService().getActivePunishment(Mute.class, target.getUniqueId())
                != null;
        if (alreadyMuted) {
          sender.sendMessage("core.punish.error.already.mute",
              target.getRank().getColor() + target.getName());
          return;
        }
        points = VenadeAPI.getPunishmentService()
            .getPunishmentPoints("mute", target.getUniqueId());
      } else {
        return;
      }
      if (template != null) {
        PunishmentTemplate temp = VenadeAPI.getPunishmentTemplateService()
            .getPunishmentTemplate(type.name().toLowerCase(), template);
        if (temp != null) {
          if (note == null) {
            if (sender.isConsole()) {
              sender.sendMessage(buildHelpMessage(sender));
              return;
            }
            new GetChatMessage(sender.getAsPlayer(),
                sender.getLanguageString("core.punish.internal-notes"),
                (notes) -> {
                  if (notes.equalsIgnoreCase("cancel")) {
                    sender.sendMessage("core.punish.cancel");
                    return;
                  }
                  Bukkit.getScheduler().runTaskAsynchronously(this.plugin,
                      () -> completePunishment(sender.getUniqueId(), target, type, temp, notes,
                          points));
                });
          } else {
            completePunishment(sender.getUniqueId(), target, type, temp, note,
                points);
          }
          return;
        } // if template is null then open GUI
      }
      PunishGUI gui = new PunishGUI(sender.getAsCorePlayer(), target, points,
          type.name().toLowerCase());
      Bukkit.getScheduler().runTask(plugin, gui::openInventory);


    }
  }

}
