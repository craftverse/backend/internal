package net.venade.internal.core.impl.report;

import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.List;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Optional;
import net.venade.internal.api.report.Report;
import net.venade.internal.core.impl.gui.report.ReportsGUI;
import net.venade.internal.core.impl.punish.DefaultReportService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 29/10/2021
 */
public class ReportsCommand extends VenadeCommand {

  private final JavaPlugin plugin;

  public ReportsCommand(JavaPlugin plugin) {
    super(
        "reports",
        "core.report.reports.desc",
        DefaultReportService.COMMAND_PERMISSION
    );
    this.setAsync(true);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender, @Optional IOfflineCubidPlayer target) {
    if (target == null) {
      List<Report> reports = VenadeAPI.getReportService().getAllOpenReports(false);
      boolean assigned = VenadeAPI.getReportService()
          .isAlreadyAssignedToReport(sender.getUniqueId());
      if (reports.isEmpty()) {
        sender.sendMessage("core.report.none");
        return;
      }
      Bukkit.getScheduler().runTask(plugin,
          () -> new ReportsGUI.AllReportsGUI(sender.getAsCorePlayer(), reports, assigned,
              plugin).openInventory());
    } else {
      Bukkit.getScheduler().runTask(plugin,
          () -> new ReportsGUI.PlayerReportsGUI(sender.getAsCorePlayer(), target).openInventory());
    }
  }

}
