package net.venade.internal.api.command;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.venade.internal.api.command.annotation.ConsumeRest;
import net.venade.internal.api.command.annotation.CustomProvider;
import net.venade.internal.api.command.annotation.Default;
import net.venade.internal.api.command.annotation.Desc;
import net.venade.internal.api.command.annotation.Flag;
import net.venade.internal.api.command.annotation.Name;
import net.venade.internal.api.command.annotation.Optional;
import net.venade.internal.api.command.provider.Provider;
import net.venade.internal.api.command.provider.Providers;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 24/08/2021
 */
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
public class CommandArgument {

  private final Provider<?> provider;
  private final Class<?> type;
  private final String name, description, defaultArgument;
  private final boolean optional, consumeRest, showOptionalInHelp;


  static List<CommandArgument> of(Parameter[] parameters) {
    List<CommandArgument> args = new ArrayList<>();
    Set<Character> used = new HashSet<>();
    for (Parameter p : parameters) {
      Class<?> type = p.getType();
      Provider<?> provider;
      String name;
      String description = null;
      String defaultArgument = null;
      boolean optional = false;
      boolean consumeRest = false;
      boolean showOptionalInHelp = false;
      if (p.isAnnotationPresent(Desc.class)) {
        description = p.getAnnotation(Desc.class).value();
      }
      if (p.isAnnotationPresent(Flag.class)) {
        if (p.getType() != Boolean.class && p.getType() != Boolean.TYPE) {
          throw new IllegalArgumentException(
              "Flag parameter \"" + p.getName() + "\" must be of type boolean, not " + p.getType()
                  .getName());
        }
        Flag flag = p.getAnnotation(Flag.class);
        char flagChar = flag.value();
        boolean hiddenFromHelp = flag.hiddenFromHelp();
        if (used.contains(flagChar)) {
          throw new IllegalArgumentException(
              "Flag parameter \"" + p.getName() + "\" uses char \"" + flagChar
                  + "\", which is already in use");
        }
        args.add(new FlagArgument("-" + flagChar, description, flagChar, hiddenFromHelp));
        used.add(flagChar);
      } else {
        if (p.isAnnotationPresent(Name.class)) {
          name = p.getAnnotation(Name.class).value();
        } else {
          name = p.getName();
        }
        if (p.isAnnotationPresent(Default.class)) {
          defaultArgument = p.getAnnotation(Default.class).value();
        } else if (p.isAnnotationPresent(Optional.class)) {
          optional = true;
          showOptionalInHelp = p.getAnnotation(Optional.class).showAsOptional();
        }
        if (p.isAnnotationPresent(ConsumeRest.class)) {
          consumeRest = true;
        }
        if (p.isAnnotationPresent(CustomProvider.class)) {
          provider = Providers.alternateProvider(type,
              p.getAnnotation(CustomProvider.class).value());
        } else {
          provider = Providers.provider(type);
        }
        args.add(new CommandArgument(provider, type, name, description, defaultArgument, optional,
            consumeRest, showOptionalInHelp));
      }
    }
    return args;
  }

  static void stripFlags(List<FlagArgument> cmdArgs, List<String> args) {
    for (FlagArgument argument : cmdArgs) {
      String match = argument.getName();
      args.remove(match);
    }
  }

  static Map<Character, Boolean> parseFlags(List<FlagArgument> cmdArgs, List<String> args) {
    if (args.size() == 0) {
      return Collections.emptyMap();
    }
    HashMap<Character, Boolean> map = new HashMap<>();
    for (FlagArgument argument : cmdArgs) {
      char flag = argument.getFlag();
      String match = argument.getName();
      if (args.remove(match)) {
        map.put(flag, true);
      } else {
        map.put(flag, false);
      }
    }
    return map;
  }

  @Getter
  static class FlagArgument extends CommandArgument {

    private final char flag;
    private final boolean hiddenFromHelp;

    public FlagArgument(String name, String description, char flag, boolean hiddenFromHelp) {
      super(null, Boolean.class, name, description, null, true, false, true);
      this.flag = flag;
      this.hiddenFromHelp = hiddenFromHelp;
    }
  }

}
