package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the method in a command that will be used when the command is executed
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandMethod {

}
