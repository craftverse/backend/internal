package net.venade.internal.api.entities.npc;

import java.util.LinkedList;
import java.util.List;
import net.venade.internal.api.entities.EntityCharacterManager;
import net.venade.internal.api.entities.event.IEntityClickEvent;
import net.venade.internal.api.entities.hologram.Hologram;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 18:05 Copyright (c) 2021
 */
public abstract class AbstractNonPlayerCharacterManager extends EntityCharacterManager {

  public final List<INonPlayerCharacter> NON_PLAYER_CHARACTERS = new LinkedList<>();

  public abstract void sendPackets(final Player player);

  public abstract void sendPackets();

  public abstract void add(
      String id,
      String displayName,
      Location location,
      NonPlayerCharacterSkin skin,
      boolean lookAtPlayer,
      Hologram hologram,
      List<IEntityClickEvent> events);

  public abstract INonPlayerCharacter getPlayer(String id);
}
