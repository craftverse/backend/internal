package net.venade.internal.api.entities.npc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.entities.event.IEntityClickEvent;
import net.venade.internal.api.entities.hologram.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 17:53 Copyright (c) 2021
 */
public class NonPlayerCharacterBuilder {

  private final List<IEntityClickEvent> events;
  private String id;
  private String displayName;
  private Location location;
  private NonPlayerCharacterSkin skin;
  private boolean lookAtPlayer;
  private Hologram hologram;

  private NonPlayerCharacterBuilder() {
    this.id = UUID.randomUUID().toString();
    this.displayName = "";
    this.location = Objects.requireNonNull(Bukkit.getWorld("world")).getSpawnLocation();
    this.skin =
        new NonPlayerCharacterSkin(
            "ewogICJ0aW1lc3RhbXAiIDogMTYwMTA2Mjg1MjE2OCwKICAicHJvZmlsZUlkIiA6ICI5YjBkNDI0OGI5NmQ0YTg0OWZlYjg0NzA4N2QxNmIxYiIsCiAgInByb2ZpbGVOYW1lIiA6ICJCZWRXYXJzIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2U2Zjk3OTY4NGM4NjFiMDg3ZDc3ZTI4NTllMDg1ZTg5ZThjYWQyYWU2ZThjMzAxYTMzYmUzOGM1N2NlZmMzMWMiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ==",
            "Pf5MYsmonLQYkvqScSJiuchVVcc4Vz9eijbVuIEUab+ydU5VS/sj/XHjVLteyXLF1EVFJEWTef9k6hAud3SVfjss24XlMC5wiLKEdB6eQUWJ4AhgB9OMUTlj0+N89KB/Pzhs3Nb3a3Ed2ojk4HdcJGJ+UR8oTDO6oqSNGwJ/QPrGJ0upuc1X8jwk3gZ0SjZkPgVu5VyVnkshS22tQyCpnIIsQumtCTylPjIMiKJAKfnOWqimcf6pZfcWF7GEJanKANclDqxN//0JbUmilerbjRuA1KHq+yTHq4V3csahJTldlKnmN97QPujuMRXO8aCwZysuGq77xHDI5raz41c0zHpIelIcizr8spdPezQkrd0/fd4edFr+WDwG0/NxHnS7DzG1ZAy/nrfdFzH4H1HNp403b2BhF3Z42PEHvC9YaRUsvXDJ4Bg5zJjQIWHxLEdXLv/pnqUrhhYLzdrvbJqo4SGrL7+fGsfyTIgk+5qML577TYWATwo5SOFpypP1l+XZs9+Q1UkkSd5/A1tpt1Z7viQ2G70N2+6VimvqnzwD7NFPoOdO4GLpdpqHT3q707hT8KPGglIaQqlSqNUdsLrDPQYTrh6kjXmxOdZsU38hTm3kQTo6XIJ35fSr6tv3FtsTr+6eETDPuHz4uoCUbhwEJkSUfdnhTG8pfpVaai1Gr/8=");
    this.events = new ArrayList<>();
    this.lookAtPlayer = false;
    this.hologram =
        new Hologram(new String[]{"§7Unnamed npc"}, this.location.clone().add(0, 2, 0));
  }

  public static NonPlayerCharacterBuilder builder() {
    return new NonPlayerCharacterBuilder();
  }

  public NonPlayerCharacterBuilder id(String id) {
    this.id = id;
    return this;
  }

  public NonPlayerCharacterBuilder display(String displayName) {
    this.displayName = displayName;
    return this;
  }

  public NonPlayerCharacterBuilder location(Location location) {
    this.location = location;
    return this;
  }

  public NonPlayerCharacterBuilder skin(NonPlayerCharacterSkin skin) {
    this.skin = skin;
    return this;
  }

  public NonPlayerCharacterBuilder event(IEntityClickEvent event) {
    this.events.add(event);
    return this;
  }

  public NonPlayerCharacterBuilder lookAtPlayer(boolean lookAtPlayer) {
    this.lookAtPlayer = lookAtPlayer;
    return this;
  }

  public NonPlayerCharacterBuilder hologram(String... lines) {
    double height = 0.75D;
    for (String line : lines) {
      height += 0.25D;
    }
    this.hologram = new Hologram(lines, this.location.clone().add(0, height, 0));
    return this;
  }

  public void build(Consumer<INonPlayerCharacter> consumer) {
    VenadeAPI.getNonPlayerCharacterManager()
        .add(id, displayName, location, skin, lookAtPlayer, hologram, events);
    consumer.accept(VenadeAPI.getNonPlayerCharacterManager().getPlayer(id));
  }
}
