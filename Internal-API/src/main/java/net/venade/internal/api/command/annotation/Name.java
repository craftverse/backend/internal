package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Sets the name of the command. If not used, the parameter name is used
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 24/08/2021
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Name {

  String value();
}
