package net.venade.internal.api.command.provider;

import java.util.Collections;
import java.util.List;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.exc.CommandException;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 27/07/2021
 */
@FunctionalInterface
public interface Provider<T> {

  T provide(Context context) throws CommandException;

  default List<String> suggest(Context context) {
    return Collections.emptyList();
  }
}
