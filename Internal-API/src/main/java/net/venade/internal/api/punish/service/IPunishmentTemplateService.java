package net.venade.internal.api.punish.service;

import java.util.List;
import net.venade.internal.api.punish.template.PunishmentTemplate;

/**
 * @author JakeMT04
 * @since 27/10/2021
 */
public interface IPunishmentTemplateService {
  List<PunishmentTemplate> getPunishmentTemplates(String type);

  PunishmentTemplate getPunishmentTemplate(String type, String identifier);

  boolean punishmentTemplateExists(String type, String identifier);

  default void savePunishmentTemplate(PunishmentTemplate template) {
    savePunishmentTemplate(template, false);
  }

  void savePunishmentTemplate(PunishmentTemplate template, boolean async);

  void deletePunishmentTemplate(PunishmentTemplate template);
}
