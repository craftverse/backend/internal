package net.venade.internal.api.entities.event;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 23.02.2021, 17:32 Copyright (c) 2021
 */
public interface IEntityClickEvent {

  default void onRightClicked(EntityCharacterClickEvent event) {
  }

  default void onLeftClicked(EntityCharacterClickEvent event) {
  }
}
