package net.venade.internal.api.game;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
public class GamePrefix {

  public static final String SERVER_PREFIX = " §b§lSERVER §7";
  public static final String ERROR_PREFIX = " §4§lERROR §c";
  public static final String SYSTEM_PREFIX = " §c§lSYSTEM §7";

  public static final String REPORT_PREFIX = " §4§lREPORT §f";
  public static final String PUNISH_PREFIX = " §f§lPUNISH §f";
  public static final String STAFF_PREFIX = " §c§lSTAFF §f";
  public static final String STAFF_CHAT_PREFIX = " §c§lSTAFF CHAT §f";

  public static final String GAME_PREFIX = " §3§lGAME §7";
  public static final String KILL_PREFIX = " §a§lKILL §7";
  public static final String DEATH_PREFIX = " §c§lDEATH §7";

  public static final String PARTY_PREFIX = " §d§lPARTY §7";
  public static final String FRIEND_PREFIX = " §a§lFRIEND §7";
  public static final String GUILD_PREFIX = " §e§lGUILD §7";

  public static final String LOOT_RIFT_PREFIX = " §5§lLOOT RIFT §7";
}
