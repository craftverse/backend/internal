package net.venade.internal.api.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.opener.ChestInventoryOpener;
import net.venade.internal.api.inventory.opener.InventoryOpener;
import net.venade.internal.api.inventory.opener.SpecialInventoryOpener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class InventoryManager {

  private final JavaPlugin plugin;
  private final PluginManager pluginManager;

  private final Map<Player, SmartInventory> inventories;
  private final Map<Player, InventoryContents> contents;

  private final List<InventoryOpener> defaultOpeners;
  private final List<InventoryOpener> openers;

  public InventoryManager(JavaPlugin plugin) {
    this.plugin = plugin;
    this.pluginManager = Bukkit.getPluginManager();

    this.inventories = new HashMap<>();
    this.contents = new HashMap<>();

    this.defaultOpeners = Arrays.asList(new ChestInventoryOpener(), new SpecialInventoryOpener());

    this.openers = new ArrayList<>();
  }

  public void init(int period) {
    pluginManager.registerEvents(new InvListener(), plugin);

    new InvTask().runTaskTimer(plugin, 1, period);
  }

  public Optional<InventoryOpener> findOpener(InventoryType type) {
    Optional<InventoryOpener> opInv =
        this.openers.stream().filter(opener -> opener.supports(type)).findAny();

    if (opInv.isEmpty()) {
      opInv = this.defaultOpeners.stream().filter(opener -> opener.supports(type)).findAny();
    }

    return opInv;
  }

  public void registerOpeners(InventoryOpener... openers) {
    this.openers.addAll(Arrays.asList(openers));
  }

  public List<Player> getOpenedPlayers(SmartInventory inv) {
    List<Player> list = new ArrayList<>();

    this.inventories.forEach(
        (player, playerInv) -> {
          if (inv.equals(playerInv)) {
            list.add(player);
          }
        });

    return list;
  }

  public Optional<SmartInventory> getInventory(Player player) {
    return Optional.ofNullable(this.inventories.get(player));
  }

  protected void setInventory(Player p, SmartInventory inv) {
    if (inv == null) {
      this.inventories.remove(p);
    } else {
      this.inventories.put(p, inv);
    }
  }

  public Optional<InventoryContents> getContents(Player player) {
    return Optional.ofNullable(this.contents.get(player));
  }

  protected void setContents(Player player, InventoryContents contents) {
    if (contents == null) {
      this.contents.remove(player);
    } else {
      this.contents.put(player, contents);
    }
  }

  @SuppressWarnings("unchecked")
  class InvListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryClick(InventoryClickEvent event) {
      Player player = (Player) event.getWhoClicked();

      if (!inventories.containsKey(player)) {
        return;
      }

      if (event.getAction() == InventoryAction.COLLECT_TO_CURSOR
          || (event.getClickedInventory() == player.getOpenInventory().getBottomInventory() && event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
        event.setCancelled(true);
        return;
      }

      if (event.getAction() == InventoryAction.NOTHING && event.getClick() != ClickType.MIDDLE) {
        event.setCancelled(true);
        return;
      }

      if (event.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
        event.setCancelled(true);

        int row = event.getSlot() / 9;
        int column = event.getSlot() % 9;

        if (row < 0 || column < 0) {
          return;
        }

        SmartInventory inv = inventories.get(player);

        if (row >= inv.getRows() || column >= inv.getColumns()) {
          return;
        }

        inv.getListeners().stream()
            .filter(listener -> listener.getType() == InventoryClickEvent.class)
            .forEach(listener -> ((InventoryListener<InventoryClickEvent>) listener).accept(event));

        contents.get(player).get(row, column).ifPresent(item -> item.run(event));

        player.updateInventory();
      }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryDrag(InventoryDragEvent event) {
      Player player = (Player) event.getWhoClicked();

      if (!inventories.containsKey(player)) {
        return;
      }

      SmartInventory inv = inventories.get(player);

      for (int slot : event.getRawSlots()) {
        if (slot >= player.getOpenInventory().getTopInventory().getSize()) {
          continue;
        }

        event.setCancelled(true);
        break;
      }

      inv.getListeners().stream()
          .filter(listener -> listener.getType() == InventoryDragEvent.class)
          .forEach(listener -> ((InventoryListener<InventoryDragEvent>) listener).accept(event));
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryOpen(InventoryOpenEvent event) {
      Player player = (Player) event.getPlayer();

      if (!inventories.containsKey(player)) {
        return;
      }

      SmartInventory inv = inventories.get(player);

      inv.getListeners().stream()
          .filter(listener -> listener.getType() == InventoryOpenEvent.class)
          .forEach(listener -> ((InventoryListener<InventoryOpenEvent>) listener).accept(event));
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryClose(InventoryCloseEvent event) {
      Player player = (Player) event.getPlayer();

      if (!inventories.containsKey(player)) {
        return;
      }

      SmartInventory inv = inventories.get(player);

      inv.getListeners().stream()
          .filter(listener -> listener.getType() == InventoryCloseEvent.class)
          .forEach(listener -> ((InventoryListener<InventoryCloseEvent>) listener).accept(event));

      if (inv.isCloseable()) {
        event.getInventory().clear();

        inventories.remove(player);
        contents.remove(player);
      } else {
        Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(event.getInventory()));
      }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerQuit(PlayerQuitEvent event) {
      Player player = event.getPlayer();

      if (!inventories.containsKey(player)) {
        return;
      }

      SmartInventory inv = inventories.get(player);

      inv.getListeners().stream()
          .filter(listener -> listener.getType() == PlayerQuitEvent.class)
          .forEach(listener -> ((InventoryListener<PlayerQuitEvent>) listener).accept(event));

      inventories.remove(player);
      contents.remove(player);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPluginDisable(PluginDisableEvent event) {
      new HashMap<>(inventories)
          .forEach(
              (player, inv) -> {
                inv.getListeners().stream()
                    .filter(listener -> listener.getType() == PluginDisableEvent.class)
                    .forEach(
                        listener ->
                            ((InventoryListener<PluginDisableEvent>) listener).accept(event));

                inv.close(player);
              });

      inventories.clear();
      contents.clear();
    }
  }

  class InvTask extends BukkitRunnable {

    @Override
    public void run() {
      new HashMap<>(inventories)
          .forEach((player, inv) -> inv.getProvider().update(player, contents.get(player)));
    }
  }
}
