package net.venade.internal.api.report;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.ILanguageAPI;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import org.bson.types.ObjectId;
import org.bukkit.Material;

/**
 * @author JakeMT04
 * @since 26/10/2021
 */
@Getter
@Setter
public class Report {

  public static final LinkedHashMap<String, ReportReason> REPORT_REASONS = new LinkedHashMap<>();

  static {
    REPORT_REASONS.put(
        "CHEATING",
        new ReportReason(
            "Cheating",
            "Blacklisted game modifications",
            "core.report.reason.cheating",
            Material.IRON_SWORD,
            false
        )
    );
    REPORT_REASONS.put(
        "CHAT",
        new ReportReason(
            "Chat messages",
            "Breaking chat rules",
            "core.report.reason.chat",
            Material.BOOK,
            true
        )
    );
    REPORT_REASONS.put(
        "SKIN",
        new ReportReason(
            "Inappropriate Skin",
            "Inappropriate Skin",
            "core.report.reason.skin",
            Material.PLAYER_HEAD,
            false
        )
    );
    REPORT_REASONS.put(
        "NAME",
        new ReportReason(
            "Inappropriate Username",
            "Inappropriate Username",
            "core.report.reason.username",
            Material.NAME_TAG,
            false
        )
    );
    REPORT_REASONS.put(
        "STATUS",
        new ReportReason(
            "Status",
            "Inappropriate status",
            "core.report.reason.status",
            Material.OAK_SIGN,
            true
        )
    );
    REPORT_REASONS.put(
        "TROLLING",
        new ReportReason(
            "Trolling",
            "Trolling",
            "core.report.reason.trolling",
            Material.TNT,
            false
        )
    );
    REPORT_REASONS.put(
        "BOOSTING",
        new ReportReason(
            "Boosting",
            "Boosting",
            "core.report.reason.boosting",
            Material.FIREWORK_ROCKET,
            false
        )
    );
  }

  private final long reportedAt;
  private final UUID reporter;
  private final UUID reported;
  private final String server;
  private final String reason;
  private final boolean closedByMute;
  protected ObjectId _id;
  private UUID assignee;
  private boolean closed;
  private long closedAt;
  private String closedReason;

  Report(UUID reporter, UUID reported, long reportedAt, String server, String reason,
      boolean closedByMute, UUID assignee, boolean closed, long closedAt, String closedReason) {
    this.reportedAt = reportedAt;
    this.reporter = reporter;
    this.reported = reported;
    this.server = server;
    this.reason = reason;
    this.assignee = assignee;
    this.closed = closed;
    this.closedByMute = closedByMute;
    this.closedAt = closedAt;
    this.closedReason = closedReason;
  }

  public static Builder builder() {
    return new Builder();
  }

  public IOfflineCubidPlayer getReporterObject() {
    if (this.reporter.equals(Sender.CONSOLE_UUID)) {
      throw new IllegalStateException("Cannot get a CubidPlayer from a console object.");
    }
    IOfflineCubidPlayer executor = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.reporter).orElse(null);
    if (executor == null) {
      executor = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.reporter)
          .orElseThrow();
    }
    return executor;
  }

  public IOfflineCubidPlayer getReportedObject() {
    IOfflineCubidPlayer target = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.reported).orElse(null);
    if (target == null) {
      target = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.reported)
          .orElseThrow();
    }
    return target;
  }

  public Map<Language, JsonDocument> getBroadcastMessage() {
    String reporterName;
    if (this.getReporter().equals(Sender.CONSOLE_UUID)) {
      reporterName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer reporter = this.getReporterObject();
      reporterName = reporter.getName();
    }
    IOfflineCubidPlayer reportedObject = this.getReportedObject();
    String reportedName = reportedObject.getName();
    Map<Language, JsonDocument> map = new HashMap<>();
    Object[] parameters = new Object[]{
        reporterName, reportedName, this.getReason(), this.getServer()
    };
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    for (Language lang : Language.values()) {
      TextComponent nonHover = HexColor.translateToCompoment(
          translator.getLanguageString("core.report.broadcast.1", lang, parameters));
      TextComponent hover = HexColor.translateToCompoment(
          translator.getLanguageString("core.report.broadcast.2", lang, parameters));
      TextComponent button = HexColor.translateToCompoment(
          translator.getLanguageString("core.report.broadcast.button", lang, parameters));
      TextComponent hoverMessage = HexColor.translateToCompoment(
          translator.getLanguageString("core.report.broadcast.hover", lang, parameters));
      hover.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
          new Text(hoverMessage.getExtra().toArray(new BaseComponent[0]))));
      button.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
          "/acceptreport " + this.get_id().toString()));

      map.put(lang, new JsonDocument().append("message",
          ComponentSerializer.toString(nonHover, hover, button)));
    }
    return map;
  }

  public static class Builder {

    private UUID reporter;
    private UUID reported;
    private String server;
    private String reason;
    private UUID assignee = null;
    private boolean closed = false;
    private boolean closedByMute;
    private long reportedAt;
    private long closedAt;
    private String closedReason;

    Builder() {
    }

    public Builder closedByMute(boolean closedByMute) {
      this.closedByMute = closedByMute;
      return this;
    }

    public Builder closedReason(String closedReason) {
      this.closedReason = closedReason;
      return this;
    }

    public Builder reporter(UUID reporter) {
      this.reporter = reporter;
      return this;
    }

    public Builder reported(UUID reported) {
      this.reported = reported;
      return this;
    }

    public Builder server(String server) {
      this.server = server;
      return this;
    }

    public Builder reason(String reason) {
      this.reason = reason;
      return this;
    }

    public Builder assignee(UUID assignee) {
      this.assignee = assignee;
      return this;
    }

    public Builder closed(boolean closed) {
      this.closed = closed;
      return this;
    }

    public Builder reportedAt(long reportedAt) {
      this.reportedAt = reportedAt;
      return this;
    }

    public Builder closedAt(long closedAt) {
      this.closedAt = closedAt;
      return this;
    }

    public Report build() {
      return new Report(reporter, reported, reportedAt, server, reason, closedByMute, assignee,
          closed, closedAt, closedReason);
    }
  }

  @Getter
  @AllArgsConstructor
  public static final class ReportReason {

    private final String displayReason;
    private final String reportReason;
    private final String description;
    private final Material item;
    private final boolean closedByMute;
  }
}
