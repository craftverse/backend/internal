package net.venade.internal.api.command.registration;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.language.Language;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 18/12/2021
 */
public class RegisteredCommand extends Command {

  public static final Pattern COMMAND_SEPARATOR_PATTERN = Pattern.compile(
      " (?=([^\"]*\"[^\"]*\")*[^\"]*$)");
  public static final Splitter TAB_COMPLETE_SPLITTER = Splitter.on(COMMAND_SEPARATOR_PATTERN);
  private static final Splitter ARGUMENT_SPLITTER = Splitter.on(COMMAND_SEPARATOR_PATTERN)
      .omitEmptyStrings();
  private static final Joiner ARGUMENT_JOINER = Joiner.on(' ');
  private final VenadeCommand command;
  private final JavaPlugin plugin;

  public RegisteredCommand(VenadeCommand command, JavaPlugin plugin) {
    super(command.getName(), CubidCloud.getBridge().getLanguageAPI()
            .getLanguageString(command.getDescription(), Language.ENGLISH), command.getUsageMessage(),
        command.getAliases());
    this.command = command;
    this.plugin = plugin;
  }

  public static List<String> stripQuotes(List<String> input) {
    input = new ArrayList<>(input);
    ListIterator<String> iterator = input.listIterator();
    while (iterator.hasNext()) {
      String value = iterator.next();
      if (value.length() < 3) {
        continue;
      }

      if (value.charAt(0) == '"' && value.charAt(value.length() - 1) == '"') {
        iterator.set(value.substring(1, value.length() - 1));
      }
    }
    return input;
  }

  @Override
  public boolean execute(CommandSender sender, String s, String[] args) {
    //noinspection UnstableApiUsage
    List<String> arg = stripQuotes(ARGUMENT_SPLITTER.splitToList(ARGUMENT_JOINER.join(args)));
    Sender platformSender = new Sender(sender);
    if (this.command.isAsync()) {
      this.plugin.getServer().getScheduler()
          .runTaskAsynchronously(this.plugin, () -> this.command.doExecute(platformSender, s, arg));
    } else {
      this.command.doExecute(platformSender, s, arg);
    }
    return true;
  }

  @Override
  public List<String> tabComplete(CommandSender sender, String s, String[] args) {
    Sender platformSender = new Sender(sender);
    //noinspection UnstableApiUsage
    return this.command.getTabCompleteOptions(platformSender, s,
        stripQuotes(TAB_COMPLETE_SPLITTER.splitToList(ARGUMENT_JOINER.join(args))));
  }

  @Override
  public boolean testPermissionSilent(CommandSender sender) {
    Sender platformSender = new Sender(sender);
    return this.command.hasPermission(platformSender);
  }


}
