package net.venade.internal.api.entities.npc;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 19:37 Copyright (c) 2021
 */
public class NonPlayerCharacterSkin {

  private String value;
  private String signature;

  public NonPlayerCharacterSkin(String value, String signature) {
    this.value = value;
    this.signature = signature;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }
}
