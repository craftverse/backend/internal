package net.venade.internal.api.command;

import java.lang.ref.WeakReference;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 27/07/2021
 */
@Getter
@AllArgsConstructor
public class Context {

  private final VenadeCommand command;
  private final WeakReference<Sender> sender;
  private final List<String> args;
  private final String argument;

  public Sender getSender() {
    return this.sender.get();
  }
}
