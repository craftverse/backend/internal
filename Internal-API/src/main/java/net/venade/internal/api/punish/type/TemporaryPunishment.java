package net.venade.internal.api.punish.type;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.ILanguageAPI;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public interface TemporaryPunishment {

  long getExpiry();

  void setExpiry(long expiry);

  long getOriginalExpiry();

  UUID getReducedBy();

  void setReducedBy(UUID uuid);

  String getReducedReason();

  void setReducedReason(String reason);

  String getReducedServer();

  void setReducedServer(String server);

  long getReducedOn();

  void setReducedOn(long when);

  Object[] getReducedParameters();

  String getType();

  int getPoints();

  default Map<Language, JsonDocument> getReducedMessage() {
    Object[] parameters = this.getReducedParameters();
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    HashMap<Language, JsonDocument> map = new HashMap<>();
    for (Language lang : Language.values()) {
      TextComponent firstLine = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.getType() + ".broadcast.reduce.1",
              lang,
              parameters));
      TextComponent secondLine = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.getType() + ".broadcast.reduce.2",
              lang,
              parameters));
      TextComponent hoverMessage = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.getType() + ".broadcast.reduce.hover",
              lang,
              parameters));
      secondLine.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
          new Text(hoverMessage.getExtra().toArray(new BaseComponent[0]))));
      map.put(lang, new JsonDocument().append("message",
          ComponentSerializer.toString(firstLine, secondLine)));
    }
    return map;
  }

  default IOfflineCubidPlayer getReducedByObject() {
    if (this.getReducedBy() == null) {
      return null;
    }
    if (this.getReducedBy().equals(Sender.CONSOLE_UUID)) {
      throw new IllegalStateException("Cannot get a CubidPlayer from a console object.");
    }
    IOfflineCubidPlayer executor = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.getReducedBy()).orElse(null);
    if (executor == null) {
      executor = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.getReducedBy())
          .orElseThrow();
    }
    return executor;
  }

  default void setReduced(UUID by, String reason, String server, long when, long newExpiry) {
    this.setReducedBy(by);
    this.setReducedReason(reason);
    this.setReducedServer(server);
    this.setReducedOn(when);
    this.setExpiry(newExpiry);
  }

  default boolean isPermanent() {
    return this.getExpiry() == 0;
  }

  default boolean hasExpired() {
    if (this.getExpiry() == 0) {
      return false;
    }
    return this.getExpiry() < System.currentTimeMillis();
  }

  default boolean wasReduced() {
    return this.getExpiry() != this.getOriginalExpiry();
  }

  boolean isActive();
}
