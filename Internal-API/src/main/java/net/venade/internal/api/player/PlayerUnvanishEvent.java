package net.venade.internal.api.player;

import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author JakeMT04
 * @since 19/12/2021
 */
public class PlayerUnvanishEvent extends Event {

  private static final HandlerList handlers = new HandlerList();

  private final ICorePlayer corePlayer;
  private final Player bukkitPlayer;

  public PlayerUnvanishEvent(ICorePlayer corePlayer) {
    super(true);
    this.corePlayer = corePlayer;
    this.bukkitPlayer = corePlayer.getPlayer();
  }


  @NonNull
  public static HandlerList getHandlerList() {
    return handlers;
  }

  @NonNull
  public HandlerList getHandlers() {
    return handlers;
  }

  public ICorePlayer getCorePlayer() {
    return corePlayer;
  }

  public Player getBukkitPlayer() {
    return bukkitPlayer;
  }
}
