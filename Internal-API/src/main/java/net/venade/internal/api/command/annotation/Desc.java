package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Sets the description of the argument in the help menu (uses cubid language)
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 24/08/2021
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Desc {

  String value();
}
