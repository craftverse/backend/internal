package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Makes this argument consume all the rest of the arguments given to the command. Only works on
 * Strings and the last argument
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConsumeRest {

}
