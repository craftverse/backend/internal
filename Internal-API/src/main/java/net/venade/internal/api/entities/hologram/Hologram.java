package net.venade.internal.api.entities.hologram;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.network.protocol.game.PacketPlayOutEntityDestroy;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.protocol.game.PacketPlayOutSpawnEntityLiving;
import net.minecraft.world.entity.decoration.EntityArmorStand;
import net.venade.internal.api.color.HexColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_17_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author : CubePixels | Nikolas Rummel
 * @created : 17.02.21, 16:26
 * <p>Copyright (c) 2021
 */
public class Hologram {

  private final List<EntityArmorStand> entitylist = new ArrayList<>();

  private final String[] text;
  private final Location location;
  private final double DISTANCE = 0.25D;
  private int count;

  public Hologram(String[] text, Location location) {
    this.text = text;
    this.location = location;
    create();
  }

  public void update(Player player) {
    this.hidePlayer(player);
    this.showPlayer(player);
  }

  public void showPlayerTemp(final Player player, int time, JavaPlugin plugin) {
    showPlayer(player);
    Bukkit.getScheduler()
        .runTaskLater(
            plugin,
            () -> hidePlayer(player),
            time);
  }

  public void showAllTemp(int time, JavaPlugin plugin) {
    showAll();
    Bukkit.getScheduler()
        .runTaskLater(
            plugin,
            () -> hideAll(),
            time);
  }

  public void setLocation(Location location) {
    entitylist.forEach(
        entityArmorStand -> entityArmorStand.setLocation(location.getX(), location.getY(), location.getZ(), 0, 0));
  }

//  public void teleportAll(Location location) {
//    setLocation(location);
//    for (Player player : Bukkit.getOnlinePlayers()) {
//      PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();
//      ((CraftPlayer) player).getHandle().b.sendPacket(packet);
//      showPlayer(player);
//    }
//  }

  public void showPlayer(Player player) {
    for (EntityArmorStand armor : entitylist) {
      PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(armor);
      PacketPlayOutEntityMetadata packet2 =
          new PacketPlayOutEntityMetadata(armor.getId(), armor.getDataWatcher(), true);
      ((CraftPlayer) player).getHandle().b.sendPacket(packet);
      ((CraftPlayer) player).getHandle().b.sendPacket(packet2);
    }
  }

  public void hidePlayer(Player player) {
    for (EntityArmorStand armor : entitylist) {
      PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(armor.getId());
      ((CraftPlayer) player).getHandle().b.sendPacket(packet);
    }
  }

  public void showAll() {
    for (Player player : Bukkit.getOnlinePlayers()) {
      for (EntityArmorStand armor : entitylist) {
        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(armor);
        PacketPlayOutEntityMetadata packet2 =
            new PacketPlayOutEntityMetadata(armor.getId(), armor.getDataWatcher(), true);
        ((CraftPlayer) player).getHandle().b.sendPacket(packet);
        ((CraftPlayer) player).getHandle().b.sendPacket(packet2);
      }
    }
  }

  public void hideAll() {
    for (Player player : Bukkit.getOnlinePlayers()) {
      for (EntityArmorStand armor : entitylist) {
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(armor.getId());
        ((CraftPlayer) player).getHandle().b.sendPacket(packet);
      }
    }
  }

  public void create() {
    for (String text : this.text) {
      EntityArmorStand entity =
          new EntityArmorStand(
              ((CraftWorld) this.location.getWorld()).getHandle(),
              this.location.getX(),
              this.location.getY(),
              this.location.getZ());
      entity.setCustomName(
          CraftChatMessage.fromJSON(
              ComponentSerializer.toString(
                  TextComponent.fromLegacyText(HexColor.translate(text)))));
      entity.setCustomNameVisible(true);
      entity.setInvisible(true);
      entity.setNoGravity(false);
      entity.setSmall(true);
      entitylist.add(entity);
      this.location.subtract(0, this.DISTANCE, 0);
      count++;
    }

    for (int i = 0; i < count; i++) {
      this.location.add(0, this.DISTANCE, 0);
    }
    this.count = 0;
  }

  public EntityArmorStand getFirstEntity() {
    return entitylist.get(0);
  }

  public List<EntityArmorStand> getEntitylist() {
    return entitylist;
  }
}
