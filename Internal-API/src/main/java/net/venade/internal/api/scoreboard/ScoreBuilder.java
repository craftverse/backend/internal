package net.venade.internal.api.scoreboard;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import net.venade.internal.api.color.HexColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 11.02.21, 12:37 Copyright (c) 2021
 */
public class ScoreBuilder {

  private static final HashMap<UUID, ScoreBuilder> players = new HashMap<>();
  private final Scoreboard scoreboard;
  private final Objective sidebar;

  public ScoreBuilder(Player player) {
    scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    sidebar = scoreboard.registerNewObjective("board", "dummy");
    sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);

    for (int i = 1; i <= 15; i++) {
      Team team = scoreboard.registerNewTeam("SLOT_" + i);
      team.addEntry(generateEntry(i));
    }

    player.setScoreboard(scoreboard);
    players.put(player.getUniqueId(), this);
  }

  public static boolean hasScore(Player player) {
    return players.containsKey(player.getUniqueId());
  }

  public static ScoreBuilder createScore(Player player) {
    return new ScoreBuilder(player);
  }

  public static ScoreBuilder getByPlayer(Player player) {
    return players.get(player.getUniqueId());
  }

  public static ScoreBuilder removeScore(Player player) {
    return players.remove(player.getUniqueId());
  }

  public void setTitle(String title) {
    title = HexColor.translate(title);
    sidebar.setDisplayName(title.length() > 32 ? title.substring(0, 32) : title);
  }

  public void setSlot(int slot, String text) {
    Team team = scoreboard.getTeam("SLOT_" + slot);
    String entry = generateEntry(slot);

    if (!scoreboard.getEntries().contains(entry)) {
      sidebar.getScore(entry).setScore(slot);
    }

    text = HexColor.translate(text);
    String prefix = getFirstSplit(text);
    String suffix =
        getFirstSplit(HexColor.translate(ChatColor.getLastColors(prefix)) + getSecondSplit(text));
    assert team != null;
    team.setPrefix(prefix);
    team.setSuffix(suffix);
  }

  public void removeSlot(int slot) {
    String entry = generateEntry(slot);
    if (scoreboard.getEntries().contains(entry)) {
      scoreboard.resetScores(entry);
    }
  }

  public void setSlotsFromList(List<String> list) {
    while (list.size() > 15) {
      list.remove(list.size() - 1);
    }

    int slot = list.size();

    if (slot < 15) {
      for (int i = (slot + 1); i <= 15; i++) {
        removeSlot(i);
      }
    }

    for (String line : list) {
      setSlot(slot, line);
      slot--;
    }
  }

  private String generateEntry(int slot) {
    return ChatColor.values()[slot].toString();
  }

  private String getFirstSplit(String s) {
    return s.length() > 16 ? s.substring(0, 16) : s;
  }

  private String getSecondSplit(String s) {
    if (s.length() > 32) {
      s = s.substring(0, 32);
    }
    return s.length() > 16 ? s.substring(16) : "";
  }
}
