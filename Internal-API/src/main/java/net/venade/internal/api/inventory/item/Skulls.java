package net.venade.internal.api.inventory.item;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 01.03.2021, 23:54 Copyright (c) 2021
 */
public class Skulls {

  public static final String PROFILE_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzU2Yzk2MzVkNzM0ODg1ZWM1YzNmMjAxMDg2ODYzMWFhY2FmNDVlNGI0N2Q3MTI4YTczMmEwODFmNThmZjMifX19";
  public static final String TWITTER_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2M3NDVhMDZmNTM3YWVhODA1MDU1NTkxNDllYTE2YmQ0YTg0ZDQ0OTFmMTIyMjY4MThjMzg4MWMwOGU4NjBmYyJ9fX0=";
  public static final String FRIENDS_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTQ0MTNiZGIyYTdlYTI3Y2YzMDNkNzk5ZGRjNjI3ZDZmZWVkNzFiMDE4NzZiOWIxYjBlMDEzYThlMWM2MjQifX19";
  public static final String PARTY_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTBkNGQ3ZDM5NDllMWFjZTM3MjAxZTM0NzM0M2NiNWFmNTc0MzMzMWViYmJlMWE5MTE0MGI1NmE0MDU2MjQzOSJ9fX0=";
  public static final String GUILD_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTNjZjUzYzg0N2UyYzQ4NWQ0MjFjYTNiMmY1YmJiYTQzOWRhMTkwNjQyYTE4OGMyMjhiMjc5YjNmYWM1MmQifX19";
  public static final String GLOBE_SKULL =
      "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjFkZDRmZTRhNDI5YWJkNjY1ZGZkYjNlMjEzMjFkNmVmYTZhNmI1ZTdiOTU2ZGI5YzVkNTljOWVmYWIyNSJ9fX0=";
}
