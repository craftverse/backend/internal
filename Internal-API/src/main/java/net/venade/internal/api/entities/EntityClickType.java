package net.venade.internal.api.entities;

public enum EntityClickType {
  LEFT,
  RIGHT,
  ANY,
  NONE
}
