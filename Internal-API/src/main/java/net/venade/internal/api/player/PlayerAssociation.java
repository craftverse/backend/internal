package net.venade.internal.api.player;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author JakeMT04
 * @since 14/12/2021
 */
@AllArgsConstructor
@Getter
public final class PlayerAssociation {
  private final UUID source;
  private final UUID target;
  private final List<PlayerIPEntry> matching;
}
