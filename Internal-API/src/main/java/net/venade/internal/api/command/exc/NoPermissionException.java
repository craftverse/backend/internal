package net.venade.internal.api.command.exc;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
public class NoPermissionException extends CommandException {

  public NoPermissionException() {
    super("core.command.noperms");
  }
}
