package net.venade.internal.api.punish.type;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.ILanguageAPI;
import cloud.cubid.module.language.Language;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import org.bson.types.ObjectId;

/**
 * @author JakeMT04
 * @since 13/10/2021
 */
@Getter
public abstract class Punishment {

  protected final UUID target;
  protected final UUID executor;
  protected final long issued;
  protected final String server;
  protected final String reason;
  protected final int originalPoints;
  protected final String type;
  protected String punishmentId;
  @Setter
  protected int points;
  @Setter
  protected String internalNotes;
  protected boolean active = true;
  protected ObjectId _id;

  protected Punishment(UUID target, UUID executor, long issued, String server, String reason,
      int points, String internalNotes) {
    this.target = target;
    this.executor = executor;
    this.issued = issued;
    this.server = server;
    this.reason = reason;
    this.points = points;
    this.originalPoints = points;
    this.internalNotes = internalNotes;
    this.type = this.getClass().getSimpleName().toLowerCase();
    this.punishmentId = UUID.randomUUID().toString().split("-")[0].toUpperCase();
  }

  public void setId(ObjectId id) {
    if (this._id != null) {
      throw new IllegalStateException("The id of this punishment is already set");
    }
    this._id = id;
  }

  public void regeneratePunishmentId() {
    if (this._id != null) {
      throw new IllegalStateException("This punishment has been saved");
    }
    this.punishmentId = UUID.randomUUID().toString().split("-")[0].toUpperCase();
  }

  public boolean isActive() {
    return this.active;
  }

  public Map<Language, JsonDocument> getBroadcastMessage() {
    Object[] parameters = this.getBroadcastMessageParameters();
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    HashMap<Language, JsonDocument> map = new HashMap<>();
    for (Language lang : Language.values()) {
      TextComponent firstLine = HexColor.translateToCompoment(
          translator.getLanguageString(this.getBroadcastMessageKey() + ".1", lang,
              parameters));
      TextComponent secondLine = HexColor.translateToCompoment(
          translator.getLanguageString(this.getBroadcastMessageKey() + ".2", lang,
              parameters));
      TextComponent hoverMessage = HexColor.translateToCompoment(
          translator.getLanguageString(this.getBroadcastMessageKey() + ".hover", lang,
              parameters));
      secondLine.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
          new Text(hoverMessage.getExtra().toArray(new BaseComponent[0]))));
      map.put(lang, new JsonDocument().append("message", ComponentSerializer.toString(firstLine, secondLine)));
    }
    return map;
  }

  public String getBroadcastMessageKey() {
    return "core.punish." + this.type + ".broadcast.add";
  }

  abstract public Object[] getBroadcastMessageParameters();

  abstract public String getDisplayScreen(Language language);

  public String getApplyScreen(Language language) {
    return getDisplayScreen(language);
  }


  public IOfflineCubidPlayer getExecutorObject() {
    if (this.executor.equals(Sender.CONSOLE_UUID)) {
      throw new IllegalStateException("Cannot get a CubidPlayer from a console object.");
    }
    IOfflineCubidPlayer executor = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.executor).orElse(null);
    if (executor == null) {
      executor = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.executor)
          .orElseThrow();
    }
    return executor;
  }

  public IOfflineCubidPlayer getTargetObject() {
    IOfflineCubidPlayer target = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.target).orElse(null);
    if (target == null) {
      target = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.target)
          .orElseThrow();
    }
    return target;
  }

  public abstract static class Builder<T extends Punishment, B extends Builder<T, B>> {

    protected UUID target;
    protected UUID executor;
    protected long issued;
    protected String server;
    protected String reason;
    protected ObjectId id;
    protected int points;
    protected String internalNotes = "[No internal notes were provided]";

    protected Builder() {
    }

    protected abstract B getThis();

    public B target(UUID target) {
      this.target = target;
      return getThis();
    }

    public B executor(UUID executor) {
      this.executor = executor;
      return getThis();
    }

    public B issued(long issued) {
      this.issued = issued;
      return getThis();
    }

    public B server(String server) {
      this.server = server;
      return getThis();
    }

    public B reason(String reason) {
      this.reason = reason;
      return getThis();
    }

    public B id(ObjectId id) {
      this.id = id;
      return getThis();
    }

    public B points(int points) {
      this.points = points;
      return getThis();
    }

    public B internalNotes(String internalNotes) {
      this.internalNotes = internalNotes;
      return getThis();
    }

    abstract public T build();

  }

  public static final class PunishmentDeserializer implements JsonDeserializer<Punishment> {

    @Override
    public Punishment deserialize(JsonElement json, Type clazz,
        JsonDeserializationContext ctx) throws JsonParseException {
      JsonObject object = json.getAsJsonObject();
      String type = object.get("type").getAsString();
      switch (type) {
        case "ban":
          return ctx.deserialize(object, Ban.class);
        case "mute":
          return ctx.deserialize(object, Mute.class);
        case "kick":
          return ctx.deserialize(object, Kick.class);
      }
      throw new JsonParseException("Cannot resolve punishment type");
    }
  }
}
