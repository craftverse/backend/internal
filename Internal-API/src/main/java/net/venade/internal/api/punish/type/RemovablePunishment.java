package net.venade.internal.api.punish.type;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.ILanguageAPI;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;

/**
 * @author JakeMT04
 * @since 13/10/2021
 */
@Getter
public abstract class RemovablePunishment extends Punishment {

  private UUID removedBy;
  private String removedReason;
  private String removedServer;
  private long removedOn;

  protected RemovablePunishment(UUID target, UUID executor, long issued, String server,
      String reason, int points, String internalNotes, UUID removedBy, String removedReason,
      String removedServer, long removedOn) {
    super(target, executor, issued, server, reason, points, internalNotes);
    this.removedBy = removedBy;
    this.removedReason = removedReason;
    this.removedServer = removedServer;
    this.removedOn = removedOn;
  }

  public boolean isActive() {
    if (!super.isActive()) {
      return false;
    }
    this.active = super.isActive() && removedOn == 0;
    if (!this.active) {
      VenadeAPI.getPunishmentService().savePunishment(this, true);
    }
    return this.active;
  }

  public boolean wasRemoved() {
    return this.removedOn != 0;
  }

  public void setRemoved(UUID by, String reason, String server, long when) {
    this.removedBy = by;
    this.removedReason = reason;
    this.removedServer = server;
    this.removedOn = when;
  }


  public Object[] getRemoveMessageParameters() {
    // first parameters are the same
    Object[] mainParams = this.getBroadcastMessageParameters();
    String removedByName;
    if (this.getRemovedBy().equals(Sender.CONSOLE_UUID)) {
      removedByName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getRemovedByObject();
      removedByName = executor.getRank().getColor() + executor.getName();
    }
    String removedOnServer = this.getRemovedServer();
    String removedReason = this.getRemovedReason();

    List<Object> p = new ArrayList<>(List.of(mainParams));
    p.add(removedByName);
    p.add(removedOnServer);
    p.add(removedReason);
    p.add(this.points);

    return p.toArray();
  }

  public IOfflineCubidPlayer getRemovedByObject() {
    if (this.getRemovedBy() == null) {
      return null;
    }
    if (this.getRemovedBy().equals(Sender.CONSOLE_UUID)) {
      throw new IllegalStateException("Cannot get a CubidPlayer from a console object.");
    }
    IOfflineCubidPlayer executor = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(this.removedBy).orElse(null);
    if (executor == null) {
      executor = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(this.removedBy)
          .orElseThrow();
    }
    return executor;
  }

  public Map<Language, JsonDocument> getRemoveMessage() {
    Object[] parameters = this.getRemoveMessageParameters();
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    HashMap<Language, JsonDocument> map = new HashMap<>();
    for (Language lang : Language.values()) {
      TextComponent firstLine = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.type + ".broadcast.remove.1", lang,
              parameters));
      TextComponent secondLine = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.type + ".broadcast.remove.2", lang,
              parameters));
      TextComponent hoverMessage = HexColor.translateToCompoment(
          translator.getLanguageString("core.punish." + this.type + ".broadcast.remove.hover", lang,
              parameters));
      secondLine.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
          new Text(hoverMessage.getExtra().toArray(new BaseComponent[0]))));
      map.put(lang, new JsonDocument().append("message",
          ComponentSerializer.toString(firstLine, secondLine)));
    }
    return map;
  }

  public abstract static class Builder<T extends RemovablePunishment, B extends RemovablePunishment.Builder<T, B>> extends
      Punishment.Builder<T, B> {

    protected UUID removedBy = null;
    protected String removedReason = null;
    protected String removedServer = null;
    protected long removedOn = 0;

    public B removedBy(UUID removedBy) {
      this.removedBy = removedBy;
      return getThis();
    }

    public B removedReason(String removedReason) {
      this.removedReason = removedReason;
      return getThis();
    }

    public B removedServer(String removedServer) {
      this.removedServer = removedServer;
      return getThis();

    }

    public B removedOn(long removedOn) {
      this.removedOn = removedOn;
      return getThis();
    }
  }
}
