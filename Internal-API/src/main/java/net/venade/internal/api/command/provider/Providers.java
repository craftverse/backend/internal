package net.venade.internal.api.command.provider;

import java.util.HashMap;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 27/07/2021
 */
@SuppressWarnings("unchecked")
public class Providers {

  private static final HashMap<Class<?>, Provider<?>> providers = new HashMap<>();
  private static final HashMap<Class<?>, HashMap<String, Provider<?>>> alternate = new HashMap<>();

  public static <T> void bind(Class<T> clazz, Provider<T> provider) {
    if (providers.containsKey(clazz)) {
      throw new IllegalArgumentException(
          "Provider is already registered for class \"" + clazz.getSimpleName()
              + "\", use bindAlternate instead.");
    }
    providers.put(clazz, provider);
  }

  public static <T> void bind(Class<T> clazz, String key, Provider<T> provider) {
    if (alternate.containsKey(clazz)) {
      if (alternate.get(clazz).containsKey(key)) {
        throw new IllegalArgumentException(
            "Provider with key " + key + " is already registered for class \""
                + clazz.getSimpleName() + "\".");
      }
    } else {
      alternate.put(clazz, new HashMap<>());
    }
    alternate.get(clazz).put(key, provider);
  }

  public static <T> Provider<T> alternateProvider(Class<T> clazz, String providerName) {
    if (providerName.equalsIgnoreCase("default")) {
      return provider(clazz);
    }
    if (alternate.containsKey(clazz)) {
      HashMap<String, Provider<?>> providers = alternate.get(clazz);
      if (providers.containsKey(providerName)) {
        return (Provider<T>) providers.get(providerName);
      }
    }
    throw new IllegalArgumentException(
        clazz.getSimpleName() + " has no alternative provider named \"" + providerName + "\"");

  }

  public static <T> Provider<T> provider(Class<T> clazz) {
    Provider<?> provider = providers.get(clazz);
    if (provider == null) {
      throw new IllegalArgumentException(clazz.getSimpleName() + " has no provider registered.");
    }
    return (Provider<T>) provider;
  }
}
