package net.venade.internal.api.player;

import cubid.cloud.modules.player.ICubidPlayer;
import java.util.UUID;
import org.bukkit.entity.Player;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
public interface ICorePlayer {

  UUID getUniqueId();

  int getAmethysts();

  int getGlowstoneDust();

  void addAmethysts(int number);

  void removeAmethysts(int number);

  void addGlowstoneDust(int number);

  void removeGlowstoneDust(int number);

  int getGlobalLevel();

  int getGlobalXp();

  void incrementGlobalLevel();

  void addGlobalXp(int number);

  long getOnlineTime();

  long getPlayTime();

  void addOnlineTime(long onlineTime);

  void addPlayTime(long playTime);

  boolean isSubscriber();

  RecvMode getStaffMessagesMode();

  void setStaffMessagesMode(RecvMode mode);

  RecvMode getFilterMessagesMode();

  void setFilterMessagesMode(RecvMode mode);

  String getLanguageString(String path);

  String getLanguageString(String path, Object... replacements);

  String[] getLanguageArray(String path, Object... replacements);

  void sendMessage(String path, Object... replacements);

  ICubidPlayer getCubidPlayer();

  Player getPlayer();

  String getOnlineTimeFormatted();

  void sendServerBanner();

  boolean isVanished();

  void setVanished(boolean vanished);

  void setVanishedActionBar(boolean shouldDisplay);

  enum RecvMode {
    ON, SERVER, OFF;


    public static String[] tabComplete() {
      return new String[]{"on", "server", "off", "next"};
    }

    public String keyName() {
      return this.name().toLowerCase().replace("_", "-");
    }

    public RecvMode next() {
      if (this == ON) {
        return SERVER;
      } else if (this == SERVER) {
        return OFF;
      } else {
        return ON;
      }
    }
  }
}
