package net.venade.internal.api.chat;

import java.util.HashMap;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Requests a chat message from a {@link Player} and returns the output
 */
public class GetChatMessage {

  public static final HashMap<UUID, GetChatMessage> users = new HashMap<>();
  private final BiConsumer<GetChatMessage, String> callback;
  private final Player owner;

  public GetChatMessage(Player owner, String prompt, Consumer<String> callback) {
    this(owner, prompt, (m, s) -> callback.accept(s));
  }

  /**
   * Creates a new request
   *
   * @param owner    The player to request from
   * @param prompt   The message to show
   * @param callback The action to perform on the message
   */
  public GetChatMessage(Player owner, String prompt, BiConsumer<GetChatMessage, String> callback) {
    this.callback = callback;
    this.owner = owner;
    owner.sendMessage(prompt);
    users.put(owner.getUniqueId(), this);
  }

  public void reissue() {
    users.put(owner.getUniqueId(), this);
  }

  public void handle(Player p, AsyncPlayerChatEvent event) {
    users.remove(p.getUniqueId());
    callback.accept(this, event.getMessage());
  }

  public void handle(Player p, PlayerCommandPreprocessEvent event) {
    users.remove(p.getUniqueId());
    callback.accept(this, event.getMessage().replaceFirst("/", ""));
  }
}
