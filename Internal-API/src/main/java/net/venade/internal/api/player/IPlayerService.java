package net.venade.internal.api.player;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import net.venade.internal.api.punish.service.IPunishmentService;
import net.venade.internal.api.punish.service.IPunishmentTemplateService;
import net.venade.internal.api.punish.service.IReportService;

/**
 * @author Nikolas Rummel
 * @since 14.08.2021
 */
public interface IPlayerService {

  Optional<ICorePlayer> getOnlinePlayer(UUID uuid);

  void updateInRedis(ICorePlayer player);

  void savePlayer(ICorePlayer player);

  void updateIPHistory(UUID uuid, String ip);

  List<PlayerAssociation> getMatchingPlayers(UUID uuid);

  List<String> getIPHistory(UUID uuid);

  void sendStaffChatMessage(UUID source, String message);

  IPunishmentService getPunishmentService();

  IPunishmentTemplateService getPunishmentTemplateService();

  IReportService getReportService();
}
