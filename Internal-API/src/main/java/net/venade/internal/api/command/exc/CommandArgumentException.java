package net.venade.internal.api.command.exc;

import cloud.cubid.module.language.Language;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
public class CommandArgumentException extends CommandException {

  private final boolean raw;

  public CommandArgumentException(String message, Object... args) {
    super(message, args);
    this.raw = false;
  }

  public CommandArgumentException(boolean raw, String message) {
    super(message);
    this.raw = raw;
  }

  @Override
  public String getMessage(Language language) {
    if (this.raw) {
      return this.getKey();
    } else {
      return super.getMessage(language);
    }
  }
}
