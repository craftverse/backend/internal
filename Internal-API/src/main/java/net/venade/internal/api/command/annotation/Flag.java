package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Makes the argument a flag
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 02/09/2021
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Flag {

  char value();

  boolean hiddenFromHelp() default false;
}
