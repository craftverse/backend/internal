package net.venade.internal.api.entities;

import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 10.03.2021, 13:48 Copyright (c) 2021
 */
public abstract class EntityCharacterManager {

  public abstract void handleLeftClick(Player player, int entityId);

  public abstract void handleRightClick(Player player, int entityId);
}
