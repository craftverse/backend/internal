package net.venade.internal.api.command;

import com.google.common.base.Joiner;
import cubid.cloud.modules.player.permission.CubidRank;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.exc.NoPermissionException;
import net.venade.internal.api.command.permission.Permission;
import net.venade.internal.api.command.provider.Provider;
import org.bukkit.util.StringUtil;

/**
 * Based on GLib
 *
 * @author JakeMT04
 */
public class VenadeCommand {

  @Getter
  private final String name;
  @Getter
  private final String description;
  @Getter
  private final Permission permission;
  @Getter
  private final List<VenadeCommand> subCommands = new ArrayList<>();
  private final List<CommandArgument> argsWithoutFlags;
  private final List<CommandArgument.FlagArgument> flagsOnly;
  @Getter
  private final boolean container;
  @Getter
  private final List<String> aliases;
  private final Method executeMethod;
  private final List<CommandArgument> args;
  @Getter
  protected VenadeCommand parent;
  private boolean includeLabel = false;
  private boolean rawArgs;
  @Setter
  @Getter
  private boolean async = false;

  public VenadeCommand(String name, String description, Permission permission, boolean container,
      String... aliases) {
    this.name = name;
    this.description = description;
    this.permission = permission;
    this.container = container;
    this.aliases = Arrays.asList(aliases);
    if (container) {
      this.executeMethod = null;
      this.args = Collections.emptyList();
      this.argsWithoutFlags = Collections.emptyList();
      this.flagsOnly = Collections.emptyList();
      return;
    }
    for (Method method : this.getClass().getMethods()) {
      if (method.isAnnotationPresent(CommandMethod.class)) {
        this.executeMethod = method;
        Parameter[] p = this.executeMethod.getParameters();
        if (p.length < 1 || p[0].getType() != Sender.class) {
          throw new IllegalArgumentException(
              "Command method requires Sender to be first argument.");
        }
        if (p.length >= 2 && p[1].getName().equalsIgnoreCase("label")
            && p[1].getType() == String.class) {
          this.includeLabel = true;
          this.args = CommandArgument.of(Arrays.copyOfRange(executeMethod.getParameters(), 2,
              executeMethod.getParameterCount()));
          this.argsWithoutFlags = this.args.stream()
              .filter(s -> !(s instanceof CommandArgument.FlagArgument))
              .collect(Collectors.toList());
          this.flagsOnly = this.args.stream()
              .filter(s -> (s instanceof CommandArgument.FlagArgument))
              .map(s -> (CommandArgument.FlagArgument) s)
              .collect(Collectors.toList());
          if (p.length == 3) {
            if (p[2].getType() == List.class) {
              this.rawArgs = true;
            }
          }
        } else {
          this.args = CommandArgument.of(Arrays.copyOfRange(executeMethod.getParameters(), 1,
              executeMethod.getParameterCount()));
          this.argsWithoutFlags = this.args.stream()
              .filter(s -> !(s instanceof CommandArgument.FlagArgument))
              .collect(Collectors.toList());
          this.flagsOnly = this.args.stream()
              .filter(s -> (s instanceof CommandArgument.FlagArgument))
              .map(s -> (CommandArgument.FlagArgument) s)
              .collect(Collectors.toList());
          if (p.length == 2) {
            if (p[1].getType() == List.class) {
              this.rawArgs = true;
            }
          }
        }
        return;
      }
    }
    throw new IllegalArgumentException("No command method could be found.");

  }

  public VenadeCommand(String name, String description, Permission permission, String... aliases) {
    this(name, description, permission, false, aliases);
  }

  public void registerSubCommand(VenadeCommand command) {
    this.subCommands.add(command);
    command.parent = this;
  }

  public void doExecute(Sender sender, String label, List<String> args) {
    try {
      WeakReference<Sender> weakSender = new WeakReference<>(sender);
      if (!this.hasPermission(sender)) {
        throw new NoPermissionException();
      }
      if (args.size() != 0) {
        for (VenadeCommand command : this.getSubCommands()) {
          if (command.getName().equalsIgnoreCase(args.get(0))) {
            if (command.hasPermission(sender)) {
              command.doExecute(sender, label, args.subList(1, args.size()));
            } else {
              throw new NoPermissionException();
            }
            return;
          }
          for (String alias : command.getAliases()) {
            if (alias.equalsIgnoreCase(args.get(0))) {
              if (command.hasPermission(sender)) {
                command.doExecute(sender, label, args.subList(1, args.size()));
              } else {
                throw new NoPermissionException();
              }
              return;
            }
          }
        }
      }
      if (this.container) {
        sender.sendRawMessage(this.buildHelpMessage(sender));
        return;
      }
      Map<Character, Boolean> flags = CommandArgument.parseFlags(this.flagsOnly, args);
      if (this.rawArgs) {
        if (this.includeLabel) {
          this.executeMethod.invoke(this, sender, label, args);
        } else {
          this.executeMethod.invoke(this, sender, args);
        }
      } else {
        List<Object> parsedArgs = new ArrayList<>();
        parsedArgs.add(sender);
        if (this.includeLabel) {
          parsedArgs.add(label);
        }
        if (!this.args.isEmpty()) {
          int index = 0;
          CommandArgument last = null;
          for (CommandArgument argument : this.args) {
            if (argument instanceof CommandArgument.FlagArgument) {
              char flag = ((CommandArgument.FlagArgument) argument).getFlag();
              parsedArgs.add(flags.getOrDefault(flag, false));
              continue;
            }
            String arg;
            try {
              arg = args.get(index);
            } catch (IndexOutOfBoundsException e) {
              if (argument.getDefaultArgument() == null && !argument.isOptional()) {
                sender.sendRawMessage(this.buildHelpMessage(sender));
                return;
              }
              if (argument.isOptional()) {
                parsedArgs.add(null);
                continue;
              }
              arg = argument.getDefaultArgument();
            }
            Provider<?> provider = argument.getProvider();
            parsedArgs.add(provider.provide(new Context(this, weakSender, args, arg)));
            last = argument;
            index++;
          }
          if (index < args.size()) {
            // try and consume the rest
            if (last != null) {
              if (last.isConsumeRest() && last.getType() == String.class) {
                String existing = (String) parsedArgs.remove(parsedArgs.size() - 1);
                String rest = Joiner.on(' ').join(args.subList(index, args.size()));
                parsedArgs.add(existing + " " + rest);
              }
            }
          }
        }
        this.executeMethod.invoke(this, parsedArgs.toArray(new Object[0]));
      }

    } catch (CommandException ex) {
      sender.sendRawMessage(ex.getMessage(sender));
    } catch (Throwable e) {
      Throwable throwable = e;
      if (throwable instanceof InvocationTargetException) {
        throwable = throwable.getCause();
      }
      throwable.printStackTrace();
      sender.sendMessage("core.command.error");
      if (sender.hasRank(CubidRank.DEV) || sender.hasRank(CubidRank.SR_DEV) || sender.hasRank(
          CubidRank.ADMIN)) {
        sender.sendRawMessage("§cError: §f" + throwable);
        sender.sendRawMessage("§cCheck console for the full error.");
      }
    }
  }

  public List<String> getTabCompleteOptions(Sender sender, String label, List<String> args) {
    return this.getTabCompleteOptions(sender, args);
  }

  public List<String> getTabCompleteOptions(Sender sender, List<String> args) {
    if (!this.hasPermission(sender)) {
      return Collections.emptyList();
    }
    if (args.size() == 0) {
      return Collections.emptyList();
    }
    if (this.container) {
      if (args.size() == 1) {
        List<String> poss = new ArrayList<>();
        for (VenadeCommand command : this.subCommands) {
          if (command.hasPermission(sender)) {
            poss.add(command.getName());
          }
        }
        return StringUtil.copyPartialMatches(args.get(0), poss, new ArrayList<>());
      } else {
        for (VenadeCommand command : this.subCommands) {
          if (command.hasPermission(sender)) {
            if (args.get(0).equalsIgnoreCase(command.getName())) {
              return command.getTabCompleteOptions(sender, args.subList(1, args.size()));
            }
            for (String alias : command.getAliases()) {
              if (args.get(0).equalsIgnoreCase(alias)) {
                return command.getTabCompleteOptions(sender, args.subList(1, args.size()));
              }
            }
          }
        }
      }
    } else {
      for (VenadeCommand command : this.subCommands) {
        if (command.hasPermission(sender)) {
          if (args.get(0).equalsIgnoreCase(command.getName())) {
            return command.getTabCompleteOptions(sender, args.subList(1, args.size()));
          }
          for (String alias : command.getAliases()) {
            if (args.get(0).equalsIgnoreCase(alias)) {
              return command.getTabCompleteOptions(sender, args.subList(1, args.size()));
            }
          }
        }
      }
      CommandArgument.stripFlags(this.flagsOnly, args);
      if (args.size() == 0) {
        return Collections.emptyList();
      }
      String arg = args.get(args.size() - 1);
      CommandArgument p;
      try {
        p = this.argsWithoutFlags.get(args.size() - 1);
      } catch (IndexOutOfBoundsException ignored) {
        return Collections.emptyList();
      }
      WeakReference<Sender> weakSender = new WeakReference<>(sender);
      Context context = new Context(this, weakSender, args, arg);
      Provider<?> provider = p.getProvider();
      List<String> a = provider.suggest(context);
      return StringUtil.copyPartialMatches(arg, a, new ArrayList<>());

    }
    return Collections.emptyList();
  }

  public String getUsageMessage() {
    StringBuilder builder = new StringBuilder("/" + getQualifiedName() + " ");
    for (CommandArgument parameter : this.args) {
      if (parameter instanceof CommandArgument.FlagArgument
          && ((CommandArgument.FlagArgument) parameter).isHiddenFromHelp()) {
        continue;
      }
      boolean optional = parameter.isShowOptionalInHelp() && (parameter.isOptional()
          || parameter.getDefaultArgument() != null);
      String name = parameter.getName();
      builder.append(optional ? "[" : "<").append(name)
          .append(parameter.isConsumeRest() ? "..." : "").append(optional ? "]" : ">").append(" ");
    }
    return builder.toString().trim();
  }

  public String buildHelpMessage(Sender sender) {
    StringBuilder builder = new StringBuilder(
        sender.getLanguageString("core.command.help.header", "/" + this.getQualifiedName()));
    List<VenadeCommand> commands = this.getSubCommands().stream()
        .filter(c -> c.hasPermission(sender)).toList();
    if (!isContainer()) {
      builder.append("\n").append("§7 » §r")
          .append(sender.getLanguageString("core.command.help.usage", this.getUsageMessage()));
      builder.append("\n").append("§7 » §r").append(
          sender.getLanguageString("core.command.help.description",
              sender.getLanguageString(this.getDescription())));
      StringBuilder argumentBuilder = new StringBuilder();
      argumentBuilder.append("\n").append("§7 » §r")
          .append(sender.getLanguageString("core.command.help.arguments"));
      boolean added = false;
      for (CommandArgument parameter : this.args) {
        if (parameter instanceof CommandArgument.FlagArgument
            && ((CommandArgument.FlagArgument) parameter).isHiddenFromHelp()) {
          continue;
        }
        if (parameter.getDescription() != null) {
          boolean optional = parameter.isShowOptionalInHelp() && (parameter.isOptional()
              || parameter.getDefaultArgument() != null);
          String name = parameter.getName();
          String description = sender.getLanguageString(parameter.getDescription());

          argumentBuilder.append("\n").append("§7 »   ").append(ChatColor.of("#55eefd"))
              .append(optional ? "[" : "<").append(name)
              .append(parameter.isConsumeRest() ? "..." : "").append(optional ? "]" : ">")
              .append(" §7- ").append(ChatColor.of("#d8d8d8")).append(description);
          added = true;
        }
      }
      if (added) {
        builder.append(argumentBuilder);
      }

      if (!commands.isEmpty()) {
        builder.append("\n").append("§7 » §r")
            .append(sender.getLanguageString("core.command.help.subcommands"));
        for (
            VenadeCommand subCommand : commands
            .stream()
            .sorted(Comparator.comparing(VenadeCommand::getName)).toList()
        ) {
          String description = sender.getLanguageString(subCommand.getDescription());
          builder.append("\n").append("§7 »   ").append(ChatColor.of("#55eefd"))
              .append(subCommand.getUsageMessage())
              .append(" §7- §r").append(description);
        }
      }

    } else {
      for (
          VenadeCommand subCommand : commands
          .stream()
          .sorted(Comparator.comparing(VenadeCommand::getName)).toList()
      ) {
        String description = sender.getLanguageString(subCommand.getDescription());
        builder.append("\n").append("§7 » ").append(ChatColor.of("#55eefd"))
            .append(subCommand.getUsageMessage())
            .append(" §7- §r").append(description);
      }
    }
    return builder.toString();
  }

  public String getQualifiedName() {
    if (this.getParent() == null) {
      return this.getName();
    }
    return this.getParent().getQualifiedName() + " " + this.getName();
  }

  public boolean hasPermission(Sender sender) {
    if (this.getPermission() == null) {
      return true;
    }
    return this.getPermission().hasPermission(sender);
  }
}
