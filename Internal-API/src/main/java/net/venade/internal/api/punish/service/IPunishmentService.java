package net.venade.internal.api.punish.service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import net.venade.internal.api.punish.BlockedName;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.punish.type.RemovablePunishment;

/**
 * @author JakeMT04
 * @since 18/10/2021
 */
public interface IPunishmentService {

  // --- Punishments --- //

  List<Punishment> getPunishments(UUID uuid);

  <T extends Punishment> List<T> getPunishments(Class<T> type, UUID uuid, boolean reversed);

  <T extends Punishment> T getActivePunishment(Class<T> type, UUID uuid);

  void broadcastBlockedName(BlockedName blockedName);

  void broadcastUnblockedName(BlockedName blockedName, UUID unblockedBy);

  int getPunishmentPoints(String type, UUID uuid);

  default void savePunishment(Punishment punishment) {
    savePunishment(punishment, false);
  }

  void savePunishment(Punishment punishment, boolean async);

  void applyPunishment(Punishment punishment);

  void broadcastPunishment(Punishment punishment);

  void broadcastRemovedPunishment(RemovablePunishment punishment);

  void broadcastReducedPunishment(Punishment punishment);

  <T extends Punishment> T getPunishmentById(String id, Class<T> clazz);

  boolean isBlockedName(String name);

  void saveBlockedName(BlockedName blockedName);

  void applyBlockedName(BlockedName blockedName);

  BlockedName getBlockedName(String name);

  CompletableFuture<List<BlockedName>> getBlockedNames();

  void removeBlockedName(String name);
}
