package net.venade.internal.api.command.exc;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.language.Language;
import lombok.Getter;
import net.venade.internal.api.command.Sender;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
@Getter
public class CommandException extends Exception {

  private final String key;
  private final Object[] args;

  public CommandException(String key, Object... args) {
    this.key = key;
    this.args = args;
  }

  public String getMessage() {
    return this.getMessage(Language.ENGLISH);
  }

  public String getMessage(Language language) {
    return CubidCloud.getBridge().getLanguageAPI().getLanguageString(this.key, language, this.args);
  }

  public String getMessage(Sender sender) {
    return this.getMessage(sender.getLanguage());
  }
}
