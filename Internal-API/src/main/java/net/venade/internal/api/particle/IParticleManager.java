package net.venade.internal.api.particle;

import org.bukkit.Location;
import org.bukkit.Particle;

/**
 * @author Nikolas Rummel
 * @since 23.08.2021
 */
public interface IParticleManager {

  void spawnParticle(Particle effect, Location location);

  void spawnCircleParticle(Particle effect, Location location);
}
