package net.venade.internal.api.inventory.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemCreator {

  private final ItemStack itemStack;

  public ItemCreator(Material material) {
    this.itemStack = new ItemStack(material);
  }

  public ItemCreator(ItemStack itemStack) {
    this.itemStack = itemStack;
  }

  public ItemCreator(Material material, int amount) {
    this.itemStack = new ItemStack(material, amount);
  }

  public String getName() {
    return itemStack.getItemMeta().getDisplayName();
  }

  public ItemCreator setName(String name) {
    ItemMeta im = itemStack.getItemMeta();
    im.setDisplayName(name);
    itemStack.setItemMeta(im);
    return this;
  }

  public Material getType() {
    return itemStack.getType();
  }

  @Override
  public ItemCreator clone() {
    return new ItemCreator(itemStack);
  }

  public ItemCreator setDurability(short durability) {
    itemStack.setDurability(durability);
    return this;
  }

  public ItemCreator addUnsafeEnchantment(Enchantment enchantment, int level) {
    itemStack.addUnsafeEnchantment(enchantment, level);
    return this;
  }

  public ItemCreator removeEnchantment(Enchantment enchantment) {
    itemStack.removeEnchantment(enchantment);
    return this;
  }

  public ItemCreator setSkullOwner(String owner) {
    try {
      SkullMeta im = (SkullMeta) itemStack.getItemMeta();
      im.setOwner(owner);
      itemStack.setItemMeta(im);
    } catch (ClassCastException e) {
    }
    return this;
  }

  public ItemCreator addEnchant(Enchantment enchantment, int level) {
    ItemMeta im = itemStack.getItemMeta();
    im.addEnchant(enchantment, level, true);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator addEnchantments(Map<Enchantment, Integer> enchantments) {
    itemStack.addEnchantments(enchantments);
    return this;
  }

  public ItemCreator setInfinityDurability() {
    itemStack.setDurability(Short.MAX_VALUE);
    return this;
  }

  public ItemCreator setLore(String... lore) {
    ItemMeta im = itemStack.getItemMeta();
    im.setLore(Arrays.asList(lore));
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator setLore(List<String> lore) {
    ItemMeta im = itemStack.getItemMeta();
    im.setLore(lore);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator removeLoreLine(String line) {
    ItemMeta im = itemStack.getItemMeta();
    List<String> lore = new ArrayList<>(im.getLore());
    if (!lore.contains(line)) {
      return this;
    }
    lore.remove(line);
    im.setLore(lore);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator removeLoreLine(int index) {
    ItemMeta im = itemStack.getItemMeta();
    List<String> lore = new ArrayList<>(im.getLore());
    if (index < 0 || index > lore.size()) {
      return this;
    }
    lore.remove(index);
    im.setLore(lore);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator addLoreLine(String line) {
    ItemMeta im = itemStack.getItemMeta();
    List<String> lore = new ArrayList<>();
    if (im.hasLore()) {
      lore = new ArrayList<>(im.getLore());
    }
    lore.add(line);
    im.setLore(lore);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator addLoreLine(String line, int pos) {
    ItemMeta im = itemStack.getItemMeta();
    List<String> lore = new ArrayList<>(im.getLore());
    lore.set(pos, line);
    im.setLore(lore);
    itemStack.setItemMeta(im);
    return this;
  }

  public ItemCreator setLeatherArmorColor(Color color) {
    try {
      LeatherArmorMeta im = (LeatherArmorMeta) itemStack.getItemMeta();
      im.setColor(color);
      itemStack.setItemMeta(im);
    } catch (ClassCastException e) {
    }
    return this;
  }

  public ItemStack build() {
    return itemStack.clone();
  }
}
