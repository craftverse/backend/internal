package net.venade.internal.api.player;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author JakeMT04
 * @since 14/12/2021
 */
@Getter
@Setter
@AllArgsConstructor
public final class PlayerIPEntry {
  private final UUID uuid;
  private final String ip;
  private final long firstSeen;
  private long lastSeen;
  private int loginCount;
}
