package net.venade.internal.api.command.registration;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.language.Language;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.venade.internal.api.command.VenadeCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 25/08/2021
 */
public class CommandManager {

  private static final Field COMMAND_MAP_FIELD;
  private static final Field KNOWN_COMMANDS_FIELD;

  static {
    try {
      COMMAND_MAP_FIELD = CraftBukkitUtil.obcClass("CraftServer").getDeclaredField("commandMap");
      COMMAND_MAP_FIELD.setAccessible(true);
      KNOWN_COMMANDS_FIELD = SimpleCommandMap.class.getDeclaredField("knownCommands");
      KNOWN_COMMANDS_FIELD.setAccessible(true);
    } catch (NoSuchFieldException | ClassNotFoundException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  @SuppressWarnings("unchecked")
  public static void registerCommand(VenadeCommand command, JavaPlugin plugin,
      String fallbackPrefix) {
    CommandMap map;
    try {
      map = (CommandMap) COMMAND_MAP_FIELD.get(plugin.getServer());
      PluginCommand pluginCommand = plugin.getCommand(command.getName());
      // Attempt to remove the command if its registered
      if (pluginCommand != null) {
        Map<String, Command> commands = (Map<String, Command>) KNOWN_COMMANDS_FIELD.get(map);
        for (Iterator<Entry<String, Command>> it = commands.entrySet().iterator(); it.hasNext(); ) {
          Map.Entry<String, Command> entry = it.next();
          if (entry.getValue() instanceof PluginCommand c) {
            if (c.getName().equalsIgnoreCase(pluginCommand.getName()) && c.getPlugin() == plugin) {
              c.unregister(map);
              it.remove();
              break;
            }
          }
        }

      }
      RegisteredCommand registeredCommand = new RegisteredCommand(command, plugin);
      map.register(fallbackPrefix, registeredCommand);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }


  public static void registerCommand(VenadeCommand venadeCommand, JavaPlugin plugin) {
    registerCommand(venadeCommand, plugin, plugin.getName());
  }

  public static void oldRegisterCommand(VenadeCommand venadeCommand, JavaPlugin plugin,
      String fallbackPrefix) {
    PluginCommand command = plugin.getCommand(venadeCommand.getName());
    CommandMap map;
    try {
      map = (CommandMap) COMMAND_MAP_FIELD.get(plugin.getServer());
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
    if (command == null) {
      // So the command doesn't exist in the plugin's plugin.yml, thats fine, lets make a new command and dynamically register it
      try {
        // For some reason the constructor is protected so i need to use reflection to access it
        Constructor<PluginCommand> constr = PluginCommand.class.getDeclaredConstructor(String.class,
            Plugin.class);
        // Make it public, thanks bukkit devs
        constr.setAccessible(true);
        command = constr.newInstance(venadeCommand.getName(), plugin);
        command.setAliases(venadeCommand.getAliases());
        // register the command into the server, there's no point setting extra flags as they are defined later anyway
        map.register(fallbackPrefix, command);
      } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
        throw new RuntimeException(e);
      }
    } else {
      // aliases
      if (!venadeCommand.getAliases().containsAll(command.getAliases())) {
        List<String> newAliases = new ArrayList<>(command.getAliases());
        for (String alias : venadeCommand.getAliases()) {
          if (!newAliases.contains(alias)) {
            newAliases.add(alias);
          }
        }
        command.unregister(map);
        command.setAliases(newAliases);
        command.register(map);
      }
    }
    BukkitExecutor executor = new BukkitExecutor(plugin, venadeCommand);
    command.setExecutor(executor);
    command.setTabCompleter(executor);
    command.setDescription(CubidCloud.getBridge().getLanguageAPI()
        .getLanguageString(venadeCommand.getDescription(), Language.ENGLISH));
    command.setUsage(venadeCommand.getUsageMessage());
  }
}
