package net.venade.internal.api;

import java.util.UUID;
import net.venade.internal.api.entities.npc.AbstractNonPlayerCharacterManager;
import net.venade.internal.api.inventory.InventoryManager;
import net.venade.internal.api.particle.IParticleManager;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.player.IPlayerService;
import net.venade.internal.api.punish.service.IPunishmentService;
import net.venade.internal.api.punish.service.IPunishmentTemplateService;
import net.venade.internal.api.punish.service.IReportService;

/**
 * @author Nikolas Rummel
 * @since 08.08.2021
 */
public class VenadeAPI {

  private static AbstractNonPlayerCharacterManager nonPlayerCharacterManager;
  private static InventoryManager inventoryManager;
  private static IPlayerService playerService;
  private static IParticleManager particleManager;

  private VenadeAPI() {
  }

  public static ICorePlayer getPlayer(UUID uuid) {
    return playerService.getOnlinePlayer(uuid).orElse(null);
  }

  public static IPlayerService getPlayerService() {
    return playerService;
  }

  public static void setPlayerService(IPlayerService playerService) {
    VenadeAPI.playerService = playerService;
  }

  public static IPunishmentService getPunishmentService() {
    return playerService.getPunishmentService();
  }

  public static AbstractNonPlayerCharacterManager getNonPlayerCharacterManager() {
    return nonPlayerCharacterManager;
  }

  public static void setNonPlayerCharacterManager(
      AbstractNonPlayerCharacterManager nonPlayerCharacterManager) {
    VenadeAPI.nonPlayerCharacterManager = nonPlayerCharacterManager;
  }

  public static InventoryManager getInventoryManager() {
    return inventoryManager;
  }

  public static void setInventoryManager(InventoryManager inventoryManager) {
    VenadeAPI.inventoryManager = inventoryManager;
  }

  public static IParticleManager getParticleManager() {
    return particleManager;
  }

  public static void setParticleManager(IParticleManager particleManager) {
    VenadeAPI.particleManager = particleManager;
  }

  public static IReportService getReportService() {
    return playerService.getReportService();
  }

  public static IPunishmentTemplateService getPunishmentTemplateService() {
    return playerService.getPunishmentTemplateService();
  }
}
