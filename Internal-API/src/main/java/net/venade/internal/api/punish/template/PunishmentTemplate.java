package net.venade.internal.api.punish.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

/**
 * @author JakeMT04
 * @since 17/10/2021
 */
@AllArgsConstructor
@Getter
@Setter
public class PunishmentTemplate {

  private ObjectId _id;
  private String name;
  private String identifier;
  private String reason;
  private String type; // ban or mute
  private int pointsToIssue;

  private static long getTime(long days) {
    return (days * 86400000);
  }

  public long totalDuration(int currentUserPoints) {
    long days = totalDays(currentUserPoints);
    if (days == 0) {
      return 0;
    } else {
      return getTime(days);
    }
  }

  public long totalDays(int currentUserPoints) {
    int points = currentUserPoints + this.pointsToIssue;
    if (points >= 100) {
      return 0; // permanent
    } else if (points >= 75) {
      return 14;
    } else if (points >= 50) {
      return 7;
    } else if (points >= 25) {
      return 3;
    } else {
      return 1;
    }
  }


}
