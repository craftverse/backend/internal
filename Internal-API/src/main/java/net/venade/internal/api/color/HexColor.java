package net.venade.internal.api.color;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * @author Nikolas Rummel
 * @since 30.08.2021
 */
public class HexColor {

  public static final String WITH_DELIMITER = "((?<=%1$s)|(?=%1$s))";

  /**
   * @param text The string of text to apply color/effects to
   * @return Returns a string of text with color/effects applied
   */
  public static String translate(String text) {

    String[] texts = text.split(String.format(WITH_DELIMITER, "&"));

    StringBuilder finalText = new StringBuilder();

    for (int i = 0; i < texts.length; i++) {
      if (texts[i].equalsIgnoreCase("&")) {
        // get the next string
        i++;
        if (texts[i].charAt(0) == '#') {
          finalText.append(ChatColor.of(texts[i].substring(0, 7))).append(texts[i].substring(7));
        } else {
          finalText.append(ChatColor.translateAlternateColorCodes('&', "&" + texts[i]));
        }
      } else {
        finalText.append(texts[i]);
      }
    }

    return finalText.toString();
  }

  public static TextComponent translateToCompoment(String text) {

    String[] texts = text.split(String.format(WITH_DELIMITER, "&"));

    ComponentBuilder builder = new ComponentBuilder();

    for (int i = 0; i < texts.length; i++) {
      TextComponent subComponent = new TextComponent();
      if (texts[i].equalsIgnoreCase("&")) {
        // get the next string
        i++;
        if (texts[i].charAt(0) == '#') {
          subComponent.setText(texts[i].substring(7));
          subComponent.setColor(ChatColor.of(texts[i].substring(0, 7)));
          builder.append(subComponent);
        } else {
          if (texts[i].length() > 1) {
            subComponent.setText(texts[i].substring(1));
          } else {
            subComponent.setText(" ");
          }

          switch (texts[i].charAt(0)) {
            case '0' -> subComponent.setColor(ChatColor.BLACK);
            case '1' -> subComponent.setColor(ChatColor.DARK_BLUE);
            case '2' -> subComponent.setColor(ChatColor.DARK_GREEN);
            case '3' -> subComponent.setColor(ChatColor.DARK_AQUA);
            case '4' -> subComponent.setColor(ChatColor.DARK_RED);
            case '5' -> subComponent.setColor(ChatColor.DARK_PURPLE);
            case '6' -> subComponent.setColor(ChatColor.GOLD);
            case '7' -> subComponent.setColor(ChatColor.GRAY);
            case '8' -> subComponent.setColor(ChatColor.DARK_GRAY);
            case '9' -> subComponent.setColor(ChatColor.BLUE);
            case 'a' -> subComponent.setColor(ChatColor.GREEN);
            case 'b' -> subComponent.setColor(ChatColor.AQUA);
            case 'c' -> subComponent.setColor(ChatColor.RED);
            case 'd' -> subComponent.setColor(ChatColor.LIGHT_PURPLE);
            case 'e' -> subComponent.setColor(ChatColor.YELLOW);
            case 'f' -> subComponent.setColor(ChatColor.WHITE);
            case 'k' -> subComponent.setObfuscated(true);
            case 'l' -> subComponent.setBold(true);
            case 'm' -> subComponent.setStrikethrough(true);
            case 'n' -> subComponent.setUnderlined(true);
            case 'o' -> subComponent.setItalic(true);
            case 'r' -> subComponent.setColor(ChatColor.RESET);
          }

          builder.append(subComponent);
        }
      } else {
        builder.append(texts[i]);
      }
    }

    return new TextComponent(builder.create());
  }
}
