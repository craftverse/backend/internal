package net.venade.internal.api.punish;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.common.document.impl.JsonDocument;
import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.ILanguageAPI;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.chat.ComponentSerializer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.date.Dates;
import org.bson.types.ObjectId;

/**
 * @author JakeMT04
 * @since 23/12/2021
 */
@RequiredArgsConstructor
@Getter
public class BlockedName {

  private final String name;
  private final UUID blockedBy;
  private final long blockedAt;
  private final String blockedReason;
  private ObjectId _id;


  public void set_id(ObjectId id) {
    if (this._id != null) {
      throw new IllegalStateException("The id of this punishment is already set");
    }
    this._id = id;
  }

  private IOfflineCubidPlayer getObject(UUID uuid) {
    if (uuid.equals(Sender.CONSOLE_UUID)) {
      throw new IllegalStateException("Cannot get a CubidPlayer from a console object.");
    }
    IOfflineCubidPlayer executor = CubidCloud.getBridge().getPlayerManager()
        .getOnlinePlayer(uuid).orElse(null);
    if (executor == null) {
      executor = CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(uuid)
          .orElseThrow();
    }
    return executor;
  }

  public Map<Language, JsonDocument> getBroadcastMessage() {
    String executorName;
    if (this.getBlockedBy().equals(Sender.CONSOLE_UUID)) {
      executorName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getObject(this.getBlockedBy());
      executorName = executor.getRank().getColor() + executor.getName();
    }
    String targetName = this.getName();
    String issuedDate = Dates.FORMATTER.format(this.getBlockedAt());
    Object[] parameters = new Object[]{
        executorName, targetName, this.getBlockedReason(), issuedDate
    };
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    HashMap<Language, JsonDocument> map = new HashMap<>();
    for (Language lang : Language.values()) {
      String firstLine = HexColor.translate(
          translator.getLanguageString("core.blockedname.broadcast", lang,
              parameters));
      map.put(lang, new JsonDocument().append("message", firstLine));
    }
    return map;
  }


  public Map<Language, JsonDocument> getRemoveMessage(UUID unblockedBy) {
    String executorName;
    if (this.getBlockedBy().equals(Sender.CONSOLE_UUID)) {
      executorName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getObject(this.getBlockedBy());
      executorName = executor.getRank().getColor() + executor.getName();
    }
    String removedByName;

    if (unblockedBy.equals(Sender.CONSOLE_UUID)) {
      removedByName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getObject(unblockedBy);
      removedByName = executor.getRank().getColor() + executor.getName();
    }
    String targetName = this.getName();
    String issuedDate = Dates.FORMATTER.format(this.getBlockedAt());
    Object[] parameters = new Object[]{
        removedByName, targetName, executorName, this.getBlockedReason(), issuedDate,
    };
    ILanguageAPI translator = CubidLanguage.getLanguageAPI();
    HashMap<Language, JsonDocument> map = new HashMap<>();
    for (Language lang : Language.values()) {
      TextComponent firstLine = HexColor.translateToCompoment(
          translator.getLanguageString("core.blockedname.removal", lang,
              parameters));
      TextComponent hoverMessage = HexColor.translateToCompoment(
          translator.getLanguageString("core.blockedname.removal.hover", lang,
              parameters));
      firstLine.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
          new Text(hoverMessage.getExtra().toArray(new BaseComponent[0]))));
      map.put(lang, new JsonDocument().append("message", ComponentSerializer.toString(firstLine)));
    }
    return map;
  }
}
