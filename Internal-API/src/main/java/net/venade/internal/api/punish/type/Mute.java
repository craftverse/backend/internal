package net.venade.internal.api.punish.type;

import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.date.Dates;

/**
 * @author JakeMT04
 * @since 13/10/2021
 */
@Getter
@Setter
public class Mute extends RemovablePunishment implements TemporaryPunishment {

  private final long originalExpiry;
  private long expiry;
  private UUID reducedBy;
  private String reducedServer;
  private String reducedReason;
  private long reducedOn;

  private Mute(UUID target, UUID executor, long issued, String server, String reason, int points,
      String internalNotes, long expiry, UUID removedBy, String removedReason, String removedServer,
      long removedOn) {
    super(target, executor, issued, server, reason, points, internalNotes, removedBy, removedReason,
        removedServer, removedOn);
    this.expiry = expiry;
    this.originalExpiry = expiry;
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public Object[] getBroadcastMessageParameters() {
    String executorName;
    if (this.getExecutor().equals(Sender.CONSOLE_UUID)) {
      executorName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getExecutorObject();
      executorName = executor.getRank().getColor() + executor.getName();
    }
    IOfflineCubidPlayer target = this.getTargetObject();
    String targetName = target.getRank().getColor() + target.getName();
    String issuedDate = Dates.FORMATTER.format(this.issued);
    String expiryDate =
        originalExpiry == 0 ? "Never" : Dates.FORMATTER.format(this.getOriginalExpiry());
    String duration =
        this.originalExpiry == 0 ? "Permanent"
            : Dates.formatDateDiff(this.issued, this.getOriginalExpiry(), 2, false);
    return new Object[]{
        executorName, targetName, this.reason, this.server, this.originalPoints, this.internalNotes,
        issuedDate, expiryDate, duration
    };
  }

  @Override
  public String getBroadcastMessageKey() {
    return super.getBroadcastMessageKey() + (this.isPermanent() ? ".perm" : ".temp");
  }

  @Override
  public String getDisplayScreen(Language language) {
    String duration =
        this.getExpiry() == 0 ? "Permanent"
            : Dates.formatDateDiff(System.currentTimeMillis(), this.getExpiry(), 0, true);
    return CubidLanguage.getLanguageAPI()
        .getLanguageString("core.punish.mute.screen." + (expiry == 0 ? "perm" : "temp"), language,
            this.reason, this.punishmentId, duration);
  }

  @Override
  public String getApplyScreen(Language language) {
    String duration =
        this.getOriginalExpiry() == 0 ? "Never"
            : Dates.formatDateDiff(this.issued, this.getOriginalExpiry(), 0, true);
    return CubidLanguage.getLanguageAPI()
        .getLanguageString("core.punish.mute.apply." + (expiry == 0 ? "perm" : "temp"), language,
            this.reason, this.punishmentId, duration);
  }


  public Object[] getReducedParameters() {
    // first parameters are the same
    Object[] mainParams = this.getBroadcastMessageParameters();
    String removedByName;
    if (this.getReducedBy().equals(Sender.CONSOLE_UUID)) {
      removedByName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getReducedByObject();
      removedByName = executor.getRank().getColor() + executor.getName();
    }
    String removedOnServer = this.getReducedServer();
    String removedReason = this.getReducedReason();
    String newDuration = Dates.formatDateDiff(this.getReducedOn(), this.getExpiry(), 2, false);
    List<Object> p = new ArrayList<>(List.of(mainParams));
    p.add(removedByName);
    p.add(removedOnServer);
    p.add(removedReason);
    p.add(newDuration);
    p.add(this.points);

    return p.toArray();
  }

  @Override
  public boolean isActive() {
    if (!super.isActive()) {
      return false;
    }
    this.active = expiry == 0 || expiry > System.currentTimeMillis();
    if (!this.active) {
      VenadeAPI.getPunishmentService().savePunishment(this, true);
    }
    return this.active;
  }

  public static class Builder extends RemovablePunishment.Builder<Mute, Builder> {

    private long expiry = 0;

    public Builder expiry(long expiry) {
      this.expiry = expiry;
      return this;
    }

    @Override
    protected Builder getThis() {
      return this;
    }

    @Override
    public Mute build() {
      return new Mute(this.target, this.executor, this.issued, this.server, this.reason,
          this.points, this.internalNotes, this.expiry, this.removedBy, this.removedReason,
          this.removedServer, this.removedOn);
    }
  }
}
