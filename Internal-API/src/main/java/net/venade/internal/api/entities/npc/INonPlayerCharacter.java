package net.venade.internal.api.entities.npc;

import net.minecraft.server.level.EntityPlayer;
import net.venade.internal.api.entities.EntityCharacter;
import net.venade.internal.api.entities.hologram.Hologram;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 09.03.2021, 18:20 Copyright (c) 2021
 */
public interface INonPlayerCharacter extends EntityCharacter {

  String getId();

  String getDisplayName();

  Location getLocation();

  EntityPlayer getEntityPlayer();

  NonPlayerCharacterSkin getSkin();

  Hologram getHologram();

  void sendPacket(Player player);
}
