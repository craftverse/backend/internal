package net.venade.internal.api.entities.event;

import java.util.function.Consumer;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 13.03.2021, 22:58 Copyright (c) 2021
 */
public class EntityLeftClickEvent implements IEntityClickEvent {

  private final Consumer<EntityCharacterClickEvent> consumer;

  public EntityLeftClickEvent(Consumer<EntityCharacterClickEvent> consumer) {
    this.consumer = consumer;
  }

  @Override
  public void onLeftClicked(EntityCharacterClickEvent event) {
    this.consumer.accept(event);
  }
}
