package net.venade.internal.api.document;

import java.util.function.Supplier;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 20.05.2021, 18:13 Copyright (c) 2021
 */
public interface IConfiguration<T> {

  IConfiguration<T> createIfNotExists(final Supplier<T> supplier);

  IConfiguration<T> load();

  IConfiguration<T> write(T type);

  IConfiguration<T> save();

  T get();
}
