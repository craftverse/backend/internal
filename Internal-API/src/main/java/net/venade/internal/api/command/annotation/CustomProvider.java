package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Makes this argument use a different provider rather than the default
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 24/08/2021
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomProvider {

  String value();
}
