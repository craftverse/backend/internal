package net.venade.internal.api.chat;

/**
 * @author Nikolas Rummel
 * @since 20.12.21
 */

public class ChatLogMessage {
    private long timestamp;
    private String message;
    private String server;

    public ChatLogMessage(long timestamp, String message, String server) {
        this.timestamp = timestamp;
        this.message = message;
        this.server = server;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
}
