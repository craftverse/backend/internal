package net.venade.internal.api.punish.service;

import java.util.List;
import java.util.UUID;
import net.venade.internal.api.punish.type.Punishment;
import net.venade.internal.api.report.Report;
import org.bson.types.ObjectId;

/**
 * @author JakeMT04
 * @since 27/10/2021
 */
public interface IReportService {

  List<Report> getReportsForPlayer(UUID uuid, boolean activeOnly);

  List<Report> getAllOpenReports(boolean includeAssigned);

  boolean hasOpenReport(UUID reporter, UUID reported);

  default void saveReport(Report report) {
    saveReport(report, false);
  }

  void saveReport(Report report, boolean async);

  void broadcastReport(Report report);

  Report getReportById(ObjectId id);

  boolean isAlreadyAssignedToReport(UUID uuid);

  void sendClosedMessages(Report report);

  void closeApplicableReports(Punishment punishment);

  Report getAssignedReport(UUID uuid);
}
