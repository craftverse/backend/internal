package net.venade.internal.api.command.permission;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.internal.api.command.Sender;

/**
 * @author JakeMT04
 * @since 30/08/2021
 */
public abstract class Permission {

  public static final Permission STAFF = new Permission() {
    @Override
    public boolean hasPermission(Sender sender) {
      return sender.getRank().isStaff();
    }
  };

  public static Permission has(CubidRank... ranks) {
    return new Has(ranks);
  }

  public static Permission hasOrHigher(CubidRank... ranks) {
    return new HasOrHigher(ranks);
  }

  abstract public boolean hasPermission(Sender sender);

  private static final class Has extends Permission {

    private final CubidRank[] ranks;

    private Has(CubidRank... ranks) {
      this.ranks = ranks;
    }

    @Override
    public boolean hasPermission(Sender sender) {
      for (CubidRank rank : ranks) {
        if (sender.hasRank(rank)) {
          return true;
        }
      }
      return false;
    }
  }

  public static final class HasOrHigher extends Permission {

    private final CubidRank[] ranks;

    private HasOrHigher(CubidRank... ranks) {
      this.ranks = ranks;
    }

    @Override
    public boolean hasPermission(Sender sender) {
      for (CubidRank rank : ranks) {
        if (sender.hasRankOrHigher(rank)) {
          return true;
        }
      }
      return false;
    }
  }


}
