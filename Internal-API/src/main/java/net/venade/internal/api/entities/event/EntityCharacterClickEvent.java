package net.venade.internal.api.entities.event;

import net.venade.internal.api.entities.EntityCharacter;
import net.venade.internal.api.entities.EntityClickType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 10.03.2021, 13:38 Copyright (c) 2021
 */
public class EntityCharacterClickEvent extends Event {

  public static final HandlerList HANDLER_LIST = new HandlerList();
  private final Player player;
  private final EntityClickType clickType;
  private final EntityCharacter entityCharacter;

  public EntityCharacterClickEvent(
      Player player, EntityClickType clickType, EntityCharacter entityCharacter) {
    this.player = player;
    this.clickType = clickType;
    this.entityCharacter = entityCharacter;
  }

  public static HandlerList getHandlerList() {
    return EntityCharacterClickEvent.HANDLER_LIST;
  }

  public Player getPlayer() {
    return player;
  }

  public EntityClickType getClickType() {
    return clickType;
  }

  public EntityCharacter getEntityCharacter() {
    return entityCharacter;
  }

  public HandlerList getHandlers() {
    return EntityCharacterClickEvent.HANDLER_LIST;
  }
}
