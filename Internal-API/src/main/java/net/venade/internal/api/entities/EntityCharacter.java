package net.venade.internal.api.entities;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.venade.internal.api.entities.event.IEntityClickEvent;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 10.03.2021, 13:41 Copyright (c) 2021
 */
public interface EntityCharacter {

  UUID getUniqueId();

  List<IEntityClickEvent> getListOfEvents();

  ConcurrentHashMap<String, String> getMetadata();
}
