package net.venade.internal.api.command;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.CubidPlayer;
import cubid.cloud.modules.player.permission.CubidRank;
import java.lang.ref.WeakReference;
import java.util.UUID;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.player.ICorePlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Based on GLib
 *
 * @author JakeMT04
 * @since 01/08/2021
 */
public class Sender {

  public static final UUID CONSOLE_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");
  public static final String CONSOLE_NAME = "&#920000§lCONSOLE";
  private final UUID senderUuid;
  private WeakReference<CommandSender> sender;

  public Sender(CommandSender sender) {
    this.sender = new WeakReference<>(sender);
    this.senderUuid = this.getUniqueId();
  }

  public boolean isOp() {
    return sender.get().isOp();
  }

  public UUID getUniqueId() {
    return this.isConsole() ? CONSOLE_UUID : this.getAsPlayer().getUniqueId();
  }

  public ICorePlayer getAsCorePlayer() {
    return VenadeAPI.getPlayer(this.getAsPlayer().getUniqueId());
  }

  public Player getAsPlayer() {
    if (this.isConsole()) {
      throw new IllegalStateException("Sender is not a player");
    }
    return (Player) this.getAsCommandSender();
  }

  public CommandSender getAsCommandSender() {
    if (this.sender.refersTo(null)) {
      if (this.senderUuid.equals(CONSOLE_UUID)) {
        this.sender = new WeakReference<>(Bukkit.getConsoleSender());
      } else {
        this.sender = new WeakReference<>(Bukkit.getPlayer(this.senderUuid));
      }
    }
    if (this.sender.refersTo(null)) {
      throw new IllegalStateException("No sender to retrieve, has been GC'd");
    }
    return this.sender.get();
  }

  public void sendMessage(String key, Object... args) {
    if (this.isConsole()) {
      String message = CubidCloud.getBridge().getLanguageAPI()
          .getLanguageString(key, Language.ENGLISH, args);
      this.getAsCommandSender().sendMessage(HexColor.translate(message));
    } else {
      this.getAsCorePlayer().sendMessage(key, args);
    }
  }

  public Language getLanguage() {
    return this.isConsole() ? Language.ENGLISH : this.getAsCorePlayer().getCubidPlayer()
        .getLanguage();
  }

  public String getLanguageString(String key, Object... args) {
    if (this.isConsole()) {
      return HexColor.translate(
          CubidCloud.getBridge().getLanguageAPI().getLanguageString(key, Language.ENGLISH, args));
    } else {
      return this.getAsCorePlayer().getLanguageString(key, args);
    }
  }

  public String getNonTranslatedLanguageString(String key, Object... args) {
    if (this.isConsole()) {
      return CubidCloud.getBridge().getLanguageAPI().getLanguageString(key, Language.ENGLISH, args);
    } else {
      return this.getAsCorePlayer().getCubidPlayer().getLanguageString(key, args);
    }
  }

  public String getLanguageString(String key) {
    if (this.isConsole()) {
      return HexColor.translate(
          CubidCloud.getBridge().getLanguageAPI().getLanguageString(key, Language.ENGLISH));
    } else {
      return this.getAsCorePlayer().getLanguageString(key);
    }
  }

  public void sendRawMessage(String message) {
    this.getAsCommandSender().sendMessage(message);
  }

  public boolean isConsole() {
    return !(this.getAsCommandSender() instanceof Player);
  }

  public boolean hasRank(CubidRank rank) {
    if (this.isConsole()) {
      return true;
    }
    if (this.getAsCorePlayer() == null) {
      CubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(this.getUniqueId()).orElse(null);
      if (cubidPlayer == null) {
        return false;
      }
      return cubidPlayer.hasRank(rank);
    }
    return this.getAsCorePlayer().getCubidPlayer().hasRank(rank);
  }

  public boolean hasRankOrHigher(CubidRank rank) {
    if (this.isConsole()) {
      return true;
    }
    if (this.getAsCorePlayer() == null) {
      CubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(this.getUniqueId()).orElse(null);
      if (cubidPlayer == null) {
        return false;
      }
      return cubidPlayer.hasRankOrHigher(rank);
    }
    return this.getAsCorePlayer().getCubidPlayer().hasRankOrHigher(rank);
  }

  public CubidRank getRank() {
    if (this.isConsole()) {
      return CubidRank.ADMIN;
    }

    if (this.getAsCorePlayer() == null) {
      CubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(this.getUniqueId()).orElse(null);
      if (cubidPlayer == null) {
        return CubidRank.DEFAULT;
      }
      return cubidPlayer.getRank();
    }
    return this.getAsCorePlayer().getCubidPlayer().getRank();
  }

  public boolean hasRankOrLower(CubidRank rank) {
    if (this.isConsole()) {
      return true;
    }
    if (this.getAsCorePlayer() == null) {
      CubidPlayer cubidPlayer = CubidCloud.getBridge().getPlayerManager()
          .getOnlinePlayer(this.getUniqueId()).orElse(null);
      if (cubidPlayer == null) {
        return false;
      }
      return cubidPlayer.hasRankOrLower(rank);
    }
    return this.getAsCorePlayer().getCubidPlayer().hasRankOrLower(rank);
  }
}
