package net.venade.internal.api.entities;

import io.netty.channel.Channel;
import java.util.Map;
import java.util.UUID;
import net.minecraft.network.protocol.Packet;
import org.bukkit.entity.Player;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 10.03.2021, 14:01 Copyright (c) 2021
 */
public interface IPacketReader {

  Channel getChannel();

  Map<UUID, Channel> getChannelMap();

  void inject(Player player);

  void uninject(Player player);

  void readPacket(Player player, Packet<?> packet);

  Object getValue(Object packet, String name);
}
