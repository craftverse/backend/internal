package net.venade.internal.api.punish.type;

import cloud.cubid.module.language.CubidLanguage;
import cloud.cubid.module.language.Language;
import cubid.cloud.modules.player.IOfflineCubidPlayer;
import java.util.UUID;
import lombok.Getter;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.date.Dates;

/**
 * @author JakeMT04
 * @since 13/10/2021
 */
@Getter
public class Kick extends Punishment {

  private Kick(UUID target, UUID executor, long issued, String server, String reason,
      String internalNotes) {
    super(target, executor, issued, server, reason, 0, internalNotes);
    this.active = false;
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public Object[] getBroadcastMessageParameters() {
    String executorName;
    if (this.getExecutor().equals(Sender.CONSOLE_UUID)) {
      executorName = Sender.CONSOLE_NAME;
    } else {
      IOfflineCubidPlayer executor = this.getExecutorObject();
      executorName = executor.getRank().getColor() + executor.getName();
    }
    IOfflineCubidPlayer target = this.getTargetObject();
    String targetName = target.getRank().getColor() + target.getName();
    String issued = Dates.FORMATTER.format(this.issued);
    return new Object[]{
        executorName, targetName, this.reason, this.server, this.originalPoints, this.internalNotes, issued
    };

  }

  @Override
  public String getDisplayScreen(Language language) {
    return CubidLanguage.getLanguageAPI()
        .getLanguageString("core.punish.kick.screen", language, this.reason);
  }

  @Override
  public boolean isActive() {
    return false;
  }

  public static class Builder extends Punishment.Builder<Kick, Builder> {

    @Override
    protected Builder getThis() {
      return this;
    }

    @Override
    public Kick build() {
      return new Kick(this.target, this.executor, this.issued, this.server, this.reason,
          this.internalNotes);
    }
  }
}
