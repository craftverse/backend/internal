package net.venade.internal.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a default argument if one is not passed during command execution
 * <p>
 * Based on GLib
 *
 * @author JakeMT04
 * @since 31/07/2021
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Default {

  String value();
}
