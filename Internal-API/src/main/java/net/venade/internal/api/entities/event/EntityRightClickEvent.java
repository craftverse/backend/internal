package net.venade.internal.api.entities.event;

import java.util.function.Consumer;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 13.03.2021, 22:58 Copyright (c) 2021
 */
public class EntityRightClickEvent implements IEntityClickEvent {

  private final Consumer<EntityCharacterClickEvent> consumer;

  public EntityRightClickEvent(Consumer<EntityCharacterClickEvent> consumer) {
    this.consumer = consumer;
  }

  @Override
  public void onRightClicked(EntityCharacterClickEvent event) {
    this.consumer.accept(event);
  }
}
