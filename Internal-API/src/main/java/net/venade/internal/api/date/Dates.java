package net.venade.internal.api.date;


import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * General date utilities
 */
public class Dates {

  public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/y h:mm:ss a",
      Locale.ENGLISH);
  private static final int maxYears = 10000;
  private static final Pattern timePattern = Pattern.compile(
      "(?:([0-9]+)(y[a-z]*|\\s*year[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(mo[a-z]*|\\smonth[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(w[a-z]*|\\s*week[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(d[a-z]*|\\s*day[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(h[a-z]*|\\s*hour[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(m[a-z]*|\\s*minute[s]?)[,\\s]*)?" +
          "(?:([0-9]+)(s[a-z]*|\\s*second[s]?)[,\\s]*)?",
      Pattern.CASE_INSENSITIVE);

  static {
    FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    DateFormatSymbols symbols = DateFormatSymbols.getInstance(Locale.UK);
    symbols.setAmPmStrings(new String[]{"AM", "PM"});
    FORMATTER.setDateFormatSymbols(symbols);
  }

  /**
   * Strips time strings (i.e. 7d or 7 days or something similar) out of an input string
   *
   * @param input The string to strip input from
   * @return The string without time patterns
   */
  public static String removeTimePattern(String input) {
    return timePattern.matcher(input).replaceAll("").trim();
  }

  /**
   * Parses timestamps contained in a string (i.e. 7d or 7 days or something similar)
   *
   * @param time   The string to test
   * @param future Whether the time will be in the future
   * @return The time in a unix timestamp
   */
  public static long parseDateDiff(String time, boolean future) {
    Matcher m = timePattern.matcher(time);
    int years = 0;
    int months = 0;
    int weeks = 0;
    int days = 0;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    boolean found = false;
    while (m.find()) {
      if (m.group() == null || m.group().isEmpty()) {
        continue;
      }
      for (int i = 0; i < m.groupCount(); i++) {
        if (m.group(i) != null && !m.group(i).isEmpty()) {
          found = true;
          break;
        }
      }
      if (found) {
        if (m.group(1) != null && !m.group(1).isEmpty()) {
          years = years + Integer.parseInt(m.group(1));
        }
        if (m.group(3) != null && !m.group(3).isEmpty()) {
          months = months + Integer.parseInt(m.group(3));
        }
        if (m.group(5) != null && !m.group(5).isEmpty()) {
          weeks = weeks + Integer.parseInt(m.group(5));
        }
        if (m.group(7) != null && !m.group(7).isEmpty()) {
          days = days + Integer.parseInt(m.group(7));
        }
        if (m.group(9) != null && !m.group(9).isEmpty()) {
          hours = hours + Integer.parseInt(m.group(9));
        }
        if (m.group(11) != null && !m.group(11).isEmpty()) {
          minutes = minutes + Integer.parseInt(m.group(11));
        }
        if (m.group(13) != null && !m.group(13).isEmpty()) {
          seconds = seconds + Integer.parseInt(m.group(13));
        }
        //break;
      }
    }
    if (!found) {
      return 0;
    }
    Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    if (years > 0) {
      if (years > maxYears) {
        years = maxYears;
      }
      c.add(Calendar.YEAR, years * (future ? 1 : -1));
    }
    if (months > 0) {
      c.add(Calendar.MONTH, months * (future ? 1 : -1));
    }
    if (weeks > 0) {
      c.add(Calendar.WEEK_OF_YEAR, weeks * (future ? 1 : -1));
    }
    if (days > 0) {
      c.add(Calendar.DAY_OF_MONTH, days * (future ? 1 : -1));
    }
    if (hours > 0) {
      c.add(Calendar.HOUR_OF_DAY, hours * (future ? 1 : -1));
    }
    if (minutes > 0) {
      c.add(Calendar.MINUTE, minutes * (future ? 1 : -1));
    }
    if (seconds > 0) {
      c.add(Calendar.SECOND, seconds * (future ? 1 : -1));
    }
    Calendar max = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    max.add(Calendar.YEAR, 10);
    if (c.after(max)) {
      return 0;
    }
    return c.getTimeInMillis();
  }

  private static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future) {
    int year = Calendar.YEAR;

    int fromYear = fromDate.get(year);
    int toYear = toDate.get(year);
    if (Math.abs(fromYear - toYear) > maxYears) {
      toDate.set(year, fromYear +
          (future ? maxYears : -maxYears));
    }

    int diff = 0;
    long savedDate = fromDate.getTimeInMillis();
    while ((future && !fromDate.after(toDate)) || (!future && !fromDate.before(toDate))) {
      savedDate = fromDate.getTimeInMillis();
      fromDate.add(type, future ? 1 : -1);
      diff++;
    }
    diff--;
    fromDate.setTimeInMillis(savedDate);
    return diff;
  }

  /**
   * Formats a unix timestamp into a future (i.e. 7 days 3 hours 40 minutes)
   *
   * @param date     The unix timestamp to use
   * @param accuracy The amount of units to see, set to 0 to see all
   * @return The formatted string
   */
  public static String formatDateDiff(long date, int accuracy, boolean useLetters) {
    Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    c.setTimeInMillis(date);
    Calendar now = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    return Dates.formatDateDiff(now, c, accuracy, useLetters);
  }

  /**
   * Formats the difference between two unix timestamps into a future (i.e. 7 days 3 hours 40
   * minutes)
   *
   * @param fromDate The unix timestamp to start from
   * @param toDate   The unix timestamp to end at
   * @param accuracy The amount of units to see, set to 0 to see all
   * @return The formatted string
   */
  public static String formatDateDiff(long fromDate, long toDate, int accuracy,
      boolean useLetters) {
    Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    c.setTimeInMillis(toDate);
    Calendar now = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    now.setTimeInMillis(fromDate);
    return Dates.formatDateDiff(now, c, accuracy, useLetters);
  }

  /**
   * Formats the difference between two unix timestamps into a future (i.e. 7 days 3 hours 40
   * minutes)
   *
   * @param fromDate A {@link Calendar} to start from
   * @param toDate   A {@link Calendar} to end at
   * @param accuracy The amount of units to see, set to 0 to see all
   * @return The formatted string
   */
  public static String formatDateDiff(Calendar fromDate, Calendar toDate, int accuracy,
      boolean useLetters) {
    boolean future = false;
    if (toDate.equals(fromDate)) {
      return useLetters ? "< 1s" : "< 1 second";
    }
    if (toDate.after(fromDate)) {
      future = true;
    }
    StringBuilder sb = new StringBuilder();
    int[] types = new int[]{Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH,
        Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND};
    String[] names;
    if (useLetters) {
      names = new String[]{"y", "y", "mo", "mo", "d", "d", "h",
          "h", "m", "m", "s", "s"};
    } else {
      names = new String[]{"year", "years", "month", "months", "day", "days", "hour",
          "hours", "minute", "minutes", "second",
          "seconds"};
    }
    int ac = 0;
    for (int i = 0; i < types.length; i++) {
      if (accuracy != 0) {
        if (ac > accuracy) {
          break;
        }
      }
      int diff = dateDiff(types[i], fromDate, toDate, future);
      if (diff > 0) {
        ac++;
        sb.append(" ").append(diff).append(useLetters ? "" : " ")
            .append(names[i * 2 + (diff > 1 ? 1 : 0)]);
      }
    }
    if (sb.length() == 0) {
      return useLetters ? "< 1s" : "< 1 second";
    }
    return sb.toString().trim();
  }

  public static String millisToTimer(long millis) {
    long seconds = millis / 1000L;
    String HOUR_FORMAT = "%02d:%02d:%02d";
    String MINUTE_FORMAT = "%02d:%02d";
    if (seconds > 3600L) {
      return String.format(HOUR_FORMAT, seconds / 3600L, seconds % 3600L / 60L, seconds % 60L);
    } else {
      return String.format(MINUTE_FORMAT, seconds / 60L, seconds % 60L);
    }
  }
}

