package net.venade.internal.api.inventory.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.banner.Pattern;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class ItemBuilder {

  public static final int NORMAL = 1;
  public static final int LEATHER = 2;
  public static final int BANNER = 3;
  public static final int SKULL = 4;
  public static final int POTION = 5;
  private final int mode;
  private int amount;
  private short data;
  private String displayName;
  private ItemStack itemStack;
  private Color leatherColor;
  private String owner;
  private OfflinePlayer ownerPlayer;
  private DyeColor baseColor;
  private PotionData potionData;
  private LeatherArmorMeta leatherArmorMeta;
  private ItemMeta itemMeta;
  private BannerMeta bannerMeta;
  private SkullMeta skullMeta;
  private PotionMeta potionMeta;
  private Map<Enchantment, Integer> enchantments;
  private List<String> lore;
  private List<Pattern> patterns;

  private ItemBuilder(Material material, int mode) {
    this.itemStack = new ItemStack(material);
    this.mode = mode;
    switch (mode) {
      case NORMAL -> this.itemMeta = this.itemStack.getItemMeta();
      case LEATHER -> this.leatherArmorMeta = (LeatherArmorMeta) this.itemStack.getItemMeta();
      case BANNER -> {
        this.bannerMeta = (BannerMeta) this.itemStack.getItemMeta();
        this.patterns = new ArrayList<>();
      }
      case SKULL -> {
        this.itemStack = new ItemStack(material);
        this.skullMeta = (SkullMeta) this.itemStack.getItemMeta();
      }
      case POTION -> this.potionMeta = (PotionMeta) this.itemStack.getItemMeta();
    }
    this.enchantments = new HashMap<>();
    this.amount = 1;
  }

  public static ItemBuilder builder(Material material) {
    return new ItemBuilder(material, NORMAL);
  }

  public static ItemBuilder builder(Material material, int mode) {
    return new ItemBuilder(material, mode);
  }

  public ItemBuilder parent(ItemStack stack) {
    this.itemStack = stack;
    return this;
  }

  public ItemBuilder amount(int amount) {
    this.amount = amount;
    return this;
  }

  public ItemBuilder unbreakable() {
    Objects.requireNonNull(this.itemStack.getItemMeta()).setUnbreakable(true);
    return this;
  }

  public ItemBuilder display(String displayName) {
    this.displayName = displayName;
    return this;
  }

  public ItemBuilder lore(String... lore) {
    this.lore = Arrays.asList(lore);
    return this;
  }

  public ItemBuilder setLore(List<String> lore) {
    this.lore = lore;
    return this;
  }

  public ItemBuilder data(short data) {
    this.data = data;
    return this;
  }

  public ItemBuilder leatherColor(Color color) {
    this.leatherColor = color;
    return this;
  }

  public ItemBuilder enchantments(Map<Enchantment, Integer> enchantments) {
    this.enchantments = enchantments;
    return this;
  }

  public ItemBuilder appendEnchantment(Enchantment enchantment, int lvl) {
    this.enchantments.put(enchantment, lvl);
    return this;
  }

  public ItemBuilder owner(String owner) {
    this.owner = owner;
    return this;
  }
  public ItemBuilder owner(OfflinePlayer owner) {
    this.ownerPlayer = owner;
    return this;
  }

  public ItemBuilder appendPattern(Pattern pattern) {
    this.patterns.add(pattern);
    return this;
  }

  public ItemBuilder pattern(List<Pattern> patterns) {
    this.patterns = patterns;
    return this;
  }

  public ItemBuilder baseColor(DyeColor color) {
    this.baseColor = color;
    return this;
  }

  public ItemBuilder potionData(PotionType type) {
    this.potionData = new PotionData(type);
    return this;
  }

  public ItemStack build() {
    this.itemStack.setAmount(this.amount);
    this.itemStack.setDurability(this.data);

    this.enchantments.forEach(
        (enchantment, level) -> this.itemMeta.addEnchant(enchantment, level, true));

    switch (this.mode) {
      case NORMAL -> {
        this.defaultMeta(this.itemMeta);
        this.itemStack.setItemMeta(this.itemMeta);
      }
      case LEATHER -> {
        this.defaultMeta(this.leatherArmorMeta);
        this.leatherArmorMeta.setColor(this.leatherColor);
        this.itemStack.setItemMeta(this.leatherArmorMeta);
      }
      case BANNER -> {
        this.defaultMeta(this.bannerMeta);
        this.bannerMeta.setPatterns(this.patterns);
        this.bannerMeta.setBaseColor(this.baseColor);
        this.itemStack.setItemMeta(this.bannerMeta);
      }
      case SKULL -> {
        this.defaultMeta(this.skullMeta);
        if (this.ownerPlayer != null) {
          this.skullMeta.setOwningPlayer(this.ownerPlayer);
        } else {
          this.skullMeta.setOwner(this.owner);
        }
        this.itemStack.setItemMeta(this.skullMeta);
      }
      case POTION -> {
        this.defaultMeta(this.potionMeta);
        this.potionMeta.setBasePotionData(this.potionData);
      }
    }
    return this.itemStack.clone();
  }

  private void defaultMeta(ItemMeta itemMeta) {
    if (displayName != null) {
      itemMeta.setDisplayName(displayName);
    }
    if (lore != null && !lore.isEmpty()) {
      itemMeta.setLore(lore);
    }
    if (enchantments != null && enchantments.isEmpty()) {
      itemStack.addEnchantments(enchantments);
    }

    itemMeta.setUnbreakable(true);
    itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
  }
}
