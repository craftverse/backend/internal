package net.venade.internal.api.document.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.TypeAdapters;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import lombok.SneakyThrows;

/**
 * All rights of this code are reserved to
 *
 * @author CubePixels | Nikolas Rummel
 * @since 26.07.2021 Copyright (c) 2021
 */
public class QuickJsonConfigUtil {

  public static final Gson GSON =
      new GsonBuilder()
          .registerTypeAdapter(UUID.class, TypeAdapters.UUID)
          .disableHtmlEscaping()
          .create();

  public static final Gson PRETTY_GSON =
      new GsonBuilder()
          .registerTypeAdapter(UUID.class, TypeAdapters.UUID)
          .disableHtmlEscaping()
          .setPrettyPrinting()
          .create();

  @SneakyThrows
  public static <T> T getFromFile(final File file, Class<T> type) {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      return GSON.fromJson(reader, type);
    }
  }

  @SneakyThrows
  public static <T> T getPrettyFromFile(final File file, Class<T> type) {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      return PRETTY_GSON.fromJson(reader, type);
    }
  }

  @SneakyThrows
  public static <T> void writeToFile(final File file, T t) {
    final Writer writer =
        new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);

    GSON.toJson(t, writer);

    writer.flush();
    writer.close();
  }

  @SneakyThrows
  public static <T> void writeToFilePretty(final File file, T t) {
    final Writer writer =
        new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);

    PRETTY_GSON.toJson(t, writer);

    writer.flush();
    writer.close();
  }
}
